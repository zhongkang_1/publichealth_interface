-- auto Generated on 2020-08-07
DROP TABLE IF EXISTS HEALTH_FAMILY;
CREATE TABLE HEALTH_FAMILY(
	id VARCHAR (50) NOT NULL COMMENT 'id',
	creater VARCHAR (50) COMMENT 'creater',
	createdate DATETIME COMMENT 'createdate',
	modifyer VARCHAR (50) COMMENT 'modifyer',
	modifydate DATETIME COMMENT 'modifydate',
	intime DATETIME COMMENT 'intime',
	outtime DATETIME COMMENT 'outtime',
	cause VARCHAR (50) COMMENT 'cause',
	hospital VARCHAR (50) COMMENT 'hospital',
	idnum VARCHAR (50) COMMENT 'idnum',
	healthid VARCHAR (50) COMMENT 'healthid',
	PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT 'HEALTH_FAMILY';
