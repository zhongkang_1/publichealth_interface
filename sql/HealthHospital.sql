-- auto Generated on 2020-08-07
DROP TABLE IF EXISTS HEALTH_HOSPITAL;
CREATE TABLE HEALTH_HOSPITAL(
	id VARCHAR (50) NOT NULL COMMENT 'id',
	creater VARCHAR (50) COMMENT 'creater',
	createdate DATETIME COMMENT 'createdate',
	modifyer VARCHAR (50) COMMENT 'modifyer',
	modifydate DATETIME COMMENT 'modifydate',
	hospital VARCHAR (50) COMMENT 'hospital',
	intime DATETIME COMMENT 'intime',
	outtime DATETIME COMMENT 'outtime',
	cause VARCHAR (50) COMMENT 'cause',
	idnum VARCHAR (50) COMMENT 'idnum',
	healthid VARCHAR (50) COMMENT 'healthid',
	PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT 'HEALTH_HOSPITAL';
