-- auto Generated on 2020-08-07
DROP TABLE IF EXISTS health_medicine;
CREATE TABLE health_medicine(
	id VARCHAR (50) NOT NULL COMMENT 'id',
	createdate DATETIME COMMENT 'createdate',
	modifydate DATETIME COMMENT 'modifydate',
	`name` VARCHAR (50) COMMENT 'name',
	class_id VARCHAR (50) COMMENT 'classId',
	creater VARCHAR (50) COMMENT 'creater',
	modifyer VARCHAR (50) COMMENT 'modifyer',
	using1 VARCHAR (50) COMMENT 'using1',
	usingquantity VARCHAR (50) COMMENT 'usingquantity',
	usingtime VARCHAR (50) COMMENT 'usingtime',
	comply VARCHAR (50) COMMENT 'comply',
	healthid VARCHAR (50) COMMENT 'healthid',
	PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT 'health_medicine';
