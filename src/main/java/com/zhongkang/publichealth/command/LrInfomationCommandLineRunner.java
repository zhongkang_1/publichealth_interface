package com.zhongkang.publichealth.command;

import ai.yue.library.base.view.Result;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.zhongkang.publichealth.dao.PeispatientDao;
import com.zhongkang.publichealth.pojo.*;
import com.zhongkang.publichealth.service.*;
import com.zhongkang.publichealth.utils.PublicHealthInterfaceConfig;
import com.zhongkang.publichealth.utils.ToolUtil;
import com.zhongkang.publichealth.utils.WebService;
import com.zhongkang.publichealth.utils.WebServicePortType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * program
 *
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 13:51
 */
@Profile("mysql")
@Component
@Slf4j
public class LrInfomationCommandLineRunner implements CommandLineRunner {
    private final String configFile = "publicHealth.xml";
    @Autowired
    private LrInformationService lrInformationService;
    @Autowired
    private HealthExaminationService healthExaminationService;
    @Autowired
    private HealthDefendService healthDefendService;
    @Autowired
    private HealthFamilyService healthFamilyService;
    @Autowired
    private HealthHospitalService healthHospitalService;
    @Autowired
    private HealthMedicineService healthMedicineService;
    @Autowired
    private PeispatientDao peispatientDao;

    public static void main(String[] args) {
        Date date = new Date(1592202291000L);
        String format = DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        System.out.println("format = " + format);
    }

    @Override
    public void run(String... args) throws Exception {
        String arg = args[0];
        switch (arg) {
            case "getInfo":
                getInfo(args[1]);
                break;
            case "saveInfo":
                saveInfo("");
                break;
            case "getExam":
                getExam(args[1]);
                break;
            case "sendExam":
                sendExam(args[1]);
                break;
            case "createInfo":
                createInfo(args[1]);
                break;
        }
        System.exit(0);
    }

    @SneakyThrows
    private void createInfo(String path) {
        File file = new File(path);
        if (file.exists()) {
            log.info("读取文件" + file.getName());
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> idnums = new ArrayList<>();
        String string = null;
        while ((string = bufferedReader.readLine()) != null) {//BufferedReader有readLine()，可以实现按行读取
            idnums.add(string);
        }
        log.info("读取到的身份证号为" + idnums.toString());
        ArrayList<LrInfomation> lrInfomations = new ArrayList<>(idnums.size());
        String key = ToolUtil.getXmlProperties(configFile, "key");
        String getInformation = ToolUtil.getXmlProperties(configFile, "getInformation");
        String username = ToolUtil.getXmlProperties(configFile, "username");
        String prgid = ToolUtil.getXmlProperties(configFile, "prgid");
        String jzkh = ToolUtil.getXmlProperties(configFile, "jzkh");
        for (String idcardno : idnums) {
            log.info("获取身份证号" + idcardno + "信息");
            Document document = DocumentHelper.createDocument();
            Element root = document.addElement(PublicHealthInterfaceConfig.XMLTOPERSONS);
            root.addAttribute("biaoshi", "2");
            root.addAttribute("value", "0");
            root.addAttribute("return", PublicHealthInterfaceConfig.TRUE);
            root.addAttribute("username", username);
            root.addAttribute("prgid", prgid);
            Element row = root.addElement(PublicHealthInterfaceConfig.ROW);
            row.addAttribute("name", PublicHealthInterfaceConfig.T_DA_JKDA_RKXZL);
            row.addAttribute("jzkh", jzkh);
            Element field = row.addElement(PublicHealthInterfaceConfig.FIELD);
            field.addAttribute("name", PublicHealthInterfaceConfig.DSFZH);
            field.addCDATA(idcardno);
            LrInfomation lrInfomation = lrInformationService.findByIdnum("370211060121065802");
            String xml = PublicHealthInterfaceConfig.informationToXml(lrInfomation);
            // 调用webservice接口
            log.info(xml);
            WebService service = new WebService();
            WebServicePortType port = service.getPort(WebServicePortType.class);
            String result = port.dataCenterWebservice(key// 接口密钥
                    , getInformation// 调用业务功能(也是 一串加密的字符串,由对方提供)
                    , xml);
            log.info(result);

        }
    }

    @SneakyThrows
    private void sendExam(String path) {
        File file = new File(path);
        if (file.exists()) {
            log.info("读取文件" + file.getName());
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> idnums = new ArrayList<>();
        String string = null;
        while ((string = bufferedReader.readLine()) != null) {//BufferedReader有readLine()，可以实现按行读取
            idnums.add(string);
        }
        log.info("读取到的身份证号为" + idnums.toString());
        for (String idnum : idnums) {
            log.info("读取" + idnum + "的体检信息");
            Peispatient peispatient = peispatientDao.findFirstByIdcardnoOrderByCreatedateDesc(idnum);
            HealthExamination healthExamination = healthExaminationService.findByPatientcode(peispatient.getPatientcode());
            List<HealthHospital> healthHospitals = healthHospitalService.findByHealthid(healthExamination.getId());
            List<HealthFamily> healthFamilies = healthFamilyService.findByHealthid(healthExamination.getId());
            List<HealthMedicineMySQL> healthMedicineOracles = healthMedicineService.findByHealthid(healthExamination.getId());
            List<HealthDefend> healthDefends = healthDefendService.findByHealthid(healthExamination.getId());
            String xml = PublicHealthInterfaceConfig.healthExaminationToXml(peispatient, healthExamination, healthHospitals,
                    healthFamilies, healthMedicineOracles, healthDefends);
            log.info(xml);
            WebService service = new WebService();
            WebServicePortType port = service.getPort(WebServicePortType.class);
            String result = port.dataCenterWebservice(ToolUtil.getXmlProperties(configFile, "key")// 接口密钥
                    , ToolUtil.getXmlProperties(configFile, "uploadHealthData")// 调用业务功能(也是 一串加密的字符串,由对方提供)
                    , xml);
            log.info("用身份证号查询基本信息结果：" + result);
        }
    }
//    }

    @SneakyThrows
    private void getExam(String path) {
        File file = new File(path);
        if (file.exists()) {
            log.info("读取文件" + file.getName());
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> idnums = new ArrayList<>();
        String string = null;
        while ((string = bufferedReader.readLine()) != null) {//BufferedReader有readLine()，可以实现按行读取
            idnums.add(string);
        }
        String uri = "http://localhost:8081/sendExam";
        Duration duration = Duration.ofDays(1);
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(duration)
                .readTimeout(duration)
                .callTimeout(duration)
                .writeTimeout(duration).build();
        for (String idnum : idnums) {
            RequestBody requestBody = RequestBody.create(MediaType.get("application/json; charset=utf-8"), JSONUtil.toJsonStr(Arrays.asList(idnum)));
            Request request = new Request.Builder().post(requestBody).url(uri).build();
            Response response = client.newCall(request).execute();
            @SuppressWarnings("ConstantConditions") String s = response.body().string();
            log.info(s);
            @SuppressWarnings("rawtypes") Result result = JSONUtil.toBean(s, Result.class);
            JSONArray data = (JSONArray) result.getData();
            List<ExamDTO> examDTOS = data.toList(ExamDTO.class);
            for (ExamDTO examDTO : examDTOS) {
                log.info("获取体检信息" + JSONUtil.toJsonPrettyStr(examDTO));
                Peispatient peispatient = examDTO.getPeispatient();
                HealthExamination examination = examDTO.getExamination();
                List<HealthHospital> hospitals = examDTO.getHospitals();
                List<HealthFamily> families = examDTO.getFamilies();
                List<HealthMedicineOracle> medicines = examDTO.getMedicines();
                List<HealthDefend> defends = examDTO.getDefends();
                if (peispatient != null) {
                    peispatientDao.save(peispatient);
                }
                if (examination != null) {
                    healthExaminationService.save(examination);
                }
                if (hospitals != null) {
                    healthHospitalService.saveAll(hospitals);
                }
                if (families != null) {
                    healthFamilyService.saveAll(families);
                }
                if (medicines != null) {
                    healthMedicineService.saveAll(medicines);
                }
                if (defends != null) {
                    healthDefendService.saveAll(defends);
                }
                log.info(idnum + "获取成功");
            }
        }
        log.info("保存成功");
    }

    @SneakyThrows
    private void saveInfo(String path) {
        String uri = "http://localhost:8081/saveInfo";
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        List<LrInfomation> lrInfomations = lrInformationService.findAll();
        for (LrInfomation lrInfomation : lrInfomations) {
            RequestBody requestBody = RequestBody.create(MediaType.get("application/json; charset=utf-8"), JSONUtil.toJsonStr(Arrays.asList(lrInfomation)));
            Request request = new Request.Builder().post(requestBody).url(uri).build();
            Response response = client.newCall(request).execute();
            String string = response.body().string();
            log.info(string);
        }

    }

    @SneakyThrows
    private void getInfo(String path) {
        File file = new File(path);
        if (file.exists()) {
            log.info("读取文件" + file.getName());
        }
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> idnums = new ArrayList<>();
        String string = null;
        while ((string = bufferedReader.readLine()) != null) {//BufferedReader有readLine()，可以实现按行读取
            idnums.add(string);
        }
        log.info("读取到的身份证号为" + idnums.toString());
        ArrayList<LrInfomation> lrInfomations = new ArrayList<>(idnums.size());
        String key = ToolUtil.getXmlProperties(configFile, "key");
        String getInformation = ToolUtil.getXmlProperties(configFile, "getInformation");
        String username = ToolUtil.getXmlProperties(configFile, "username");
        String prgid = ToolUtil.getXmlProperties(configFile, "prgid");
        String jzkh = ToolUtil.getXmlProperties(configFile, "jzkh");
        for (String idcardno : idnums) {
            log.info("获取身份证号" + idcardno + "信息");
            Document document = DocumentHelper.createDocument();
            Element root = document.addElement(PublicHealthInterfaceConfig.XMLTOPERSONS);
            root.addAttribute("biaoshi", "2");
            root.addAttribute("value", "0");
            root.addAttribute("return", PublicHealthInterfaceConfig.TRUE);
            root.addAttribute("username", username);
            root.addAttribute("prgid", prgid);
            Element row = root.addElement(PublicHealthInterfaceConfig.ROW);
            row.addAttribute("name", PublicHealthInterfaceConfig.T_DA_JKDA_RKXZL);
            row.addAttribute("jzkh", jzkh);
            Element field = row.addElement(PublicHealthInterfaceConfig.FIELD);
            field.addAttribute("name", PublicHealthInterfaceConfig.DSFZH);
            field.addCDATA(idcardno);
            Peispatient peispatient = peispatientDao.findByIdcardno(idcardno);
            String xml = null;
            // 调用webservice接口
            log.info("发送内容" + document.asXML());
            WebService service = new WebService();
            WebServicePortType port = service.getPort(WebServicePortType.class);
            String result = port.dataCenterWebservice(key// 接口密钥
                    , getInformation// 调用业务功能(也是 一串加密的字符串,由对方提供)
                    , document.asXML());
            log.info(result);
            if (result.equalsIgnoreCase("本机构没有操作权限，请注册后再使用！")) {
                log.info(result);
            } else if ((result == null) || result.contains("没有此人档案") || result.contains("无此人档案") || result.contains("无数据")) {
                log.info(idcardno + "老人信息不存在");
            } else {
                LrInfomation lrInfomation = PublicHealthInterfaceConfig.xmlToInformation(result);
                log.info("获取信息" + JSONUtil.toJsonPrettyStr(lrInfomation));
                lrInfomations.add(lrInfomation);
//            }
            }
        }
        lrInformationService.saveOrUpdateAll(lrInfomations);
        log.info("保存完成");
    }
}