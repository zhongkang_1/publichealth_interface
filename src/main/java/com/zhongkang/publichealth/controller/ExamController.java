package com.zhongkang.publichealth.controller;

import ai.yue.library.base.view.Result;
import ai.yue.library.base.view.ResultInfo;
import cn.hutool.json.JSONUtil;
import com.zhongkang.publichealth.pojo.ExamDTO;
import com.zhongkang.publichealth.service.ExamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 09:56
 */
//@Profile("oracle")
@RestController
@RequestMapping
@Slf4j
public class ExamController {
    @Autowired
    private ExamService examService;

    @PostMapping("sendExam")
    public Result<List<ExamDTO>> sendExam(@RequestBody List<String> idNums) {
        log.info(JSONUtil.toJsonPrettyStr(idNums));
        return ResultInfo.success(examService.getExamInfoByIdnums(idNums));
    }
}