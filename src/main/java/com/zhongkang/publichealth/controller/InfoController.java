package com.zhongkang.publichealth.controller;

import ai.yue.library.base.view.Result;
import ai.yue.library.base.view.ResultInfo;
import cn.hutool.json.JSONUtil;
import com.zhongkang.publichealth.pojo.LrInfomation;
import com.zhongkang.publichealth.service.LrInformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 16:29
 */
@Profile("oracle")
@RestController
@RequestMapping
@Slf4j
public class InfoController {
    @Autowired
    private LrInformationService lrInformationService;
    @PostMapping("saveInfo")
    private Result<?> saveInfo(@RequestBody List<LrInfomation> lrInfomation) {
        log.info(JSONUtil.toJsonStr(lrInfomation));
        lrInformationService.saveOrUpdateAll(lrInfomation);
        return ResultInfo.success();
    }
}