/**
 * 项目名：
 * 文件名：ToolUtil.java
 * 创建日期：2015-12-23 上午10:25:19
 * 创建人：Administrator
 * cvs版本：$Reversion$ $Date: 2016/11/14 03:39:34 $
 * 修改记录：无
 * [参考格式：修改人：Administrator 修改时间：2015-12-23 上午10:25:19 修改内容：X]
 */
package com.zhongkang.publichealth.utils;

import lombok.SneakyThrows;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

/**
 * 类名称：[中文类名] 作者： Administrator
 */

@Service("toolUtil")
public class ToolUtil {


    /**
     * 获取配置文件里的配置 String property 配置文件名称 String name 需要获取的配置参数名称 return String
     * 配置参数
     **/
    public static String getPropert(String property, String name) {
        String url = "";
        try {
            ClassLoader cls = Thread.currentThread().getContextClassLoader();
            InputStream in = cls.getResourceAsStream(property);
            Properties p = new Properties();

            p.load(in);
            if (p.containsKey(name)) {
                url = p.getProperty(name);
            }
        } catch (Exception e) {
        }

        return url;
    }


    public static HashMap<String, String> getProperties(String path) throws Exception {
        HashMap<String, String> result = new HashMap<String, String>();
        InputStream io = null;
        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            io = cld.getResourceAsStream(path);
            Properties pro = new Properties();
            pro.load(io);
            for (Object key : pro.keySet()) {
                result.put(key.toString(), pro.getProperty(key.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (io != null) {
                io.close();
            }
        }
        return result;
    }

    /**
     * 读取所有属性
     *
     * @param property
     * @return Map<Object, Object>
     * @Title: getAllProperts
     * @author xuhp
     * @since 2017-4-6 V 1.0
     */
    public static Map<Object, Object> getAllProperts(String property) {
        Map<Object, Object> map = new HashMap<Object, Object>();
        try {
            ClassLoader cls = Thread.currentThread().getContextClassLoader();
            InputStream in = cls.getResourceAsStream(property);
            Properties p = new Properties();
            p.load(in);
            for (Entry<Object, Object> entry : p.entrySet()) {
                map.put(entry.getKey(), entry.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 读取xml文件，找不到就返回null
     */
    @SuppressWarnings("rawtypes")
    public static String getXmlPro(String xmlName, String key) {
        try {
            SAXReader saxReader = new SAXReader();
            String abPath = Thread.currentThread()
                    .getContextClassLoader().getResource("").toURI().getPath()
                    + File.separator
                    + xmlName
                    + ".xml";
            File xml = new File(abPath);
            if (!xml.exists()) return null;
            Document document = saxReader.read(abPath);
            Element root = document.getRootElement();
            List list = root.elements(key);
            if (list == null || list.size() == 0) return null;
            Element target = (Element) (list.get(0));
            return target.getText();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @SneakyThrows
    public static String getXmlProperties(String fileName, String properties) {
        File file = new File(fileName);
        if (!file.exists()) {
            throw new RuntimeException("文件不存在");
        }
        Document document = new SAXReader().read(file);
        Element rootElement = document.getRootElement();
        Node node = rootElement.selectSingleNode(properties);
        return node.getText();
    }

}
