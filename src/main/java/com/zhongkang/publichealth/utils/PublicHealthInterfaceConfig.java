package com.zhongkang.publichealth.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.zhongkang.publichealth.pojo.*;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.List;

public class PublicHealthInterfaceConfig {
    // elements
    public final static String XMLTOPERSONS = "XMLTOPERSONS";
    public final static String ROW = "row";
    public final static String FIELD = "field";
    // attributes
    public final static String T_DA_JKDA_RKXZL = "T_DA_JKDA_RKXZL";
    public final static String TRUE = "TRUE";
    public final static String DSFZH = "DSfzh";

    @SuppressWarnings("rawtypes")
    public static LrInfomation xmlToInformation(String xml) throws Exception {
        Document doc = DocumentHelper.parseText(xml);
        Element root = doc.getRootElement();
        Element row = root.element("row");
        List fields = row.elements("field");
        LrInfomation lr = new LrInfomation();
        for (int i = 0; i < fields.size(); i++) {
            Element field = (Element) fields.get(i);
            String value = field.getStringValue();
            String name = field.attributeValue("name");
            if (StrUtil.isEmpty(value)) {
                continue;
            }
            switch (name) {
                case "DGrdabh":
//					业务档案编号
                    lr.setBusinessArchivesNo(value);
                    break;
                case "DGrdabhshow":
//					显示档案编号
                    lr.setArchivesno(value);
                    break;
                case "DXm":
//					姓名
                    lr.setName(value);
                    break;
                case "DSfzh":
//					身份证号
                    lr.setIdnum(value);
                    break;
                case "DPyjm":
//					拼音简码
                    lr.setNamecode(value);
                    break;
                case "DYhzgx":
//					与户主关系
                    lr.setHouseholdRelation(value);
                    break;
                case "DLxdh":
//					联系电话
                    lr.setPhonenum(value);
                    break;
                case "DEmail":
//					电子邮箱
                    break;
                case "DSheng":
//					省
                    lr.setSheng(value);
                    break;
                case "DShi":
//					市
                    lr.setShi(value);
                    break;
                case "DQu":
//					区
                    lr.setQu(value);
                    break;
                case "DJd":
//					街道
                    lr.setJiedao(value);
                    break;
                case "DJwh":
//					居委会
                    lr.setShequ(value);
                    break;
                case "DXxdz":
//					详细地址
                    lr.setAddress(value);
                    break;
                case "DSspq":
//					所属片区
                    lr.setSubArea(value);
                    break;
                case "DJzzk":
//					居住状态
                    lr.setDwellState(value);
                    break;
                case "DXb":
                    lr.setSex(value);
                    break;
                case "DCsrq":
                    lr.setBirth(value);
                    break;
                case "DMz":
                    lr.setNation(value);
                    break;
                case "DWhcd":
                    lr.setEducation(value);
                    break;
                case "DZy":
                    lr.setOccupation(value);
                    break;
                case "DHyzk":
                    lr.setMarriage(value);
                    break;
                case "DYlfzflx":
                    lr.setPayment(value);
                    break;
                case "DYlbxh":
//					医疗保险号
                    lr.setMedicareNo(value);
                    break;
                case "DXnhh":
//					新农合号
                    lr.setNewFarmingNo(value);
                    break;
                case "PRgid":
                    lr.setJigou(value);
                    break;
                case "DLxrxm":
                    lr.setContact(value);
                    break;
                case "DLxrdh":
                    lr.setContactphone(value);
                    break;
                case "DGzdw":
                    lr.setCompany(value);
                    break;
                case "Sftj":
//					是否体检
                    lr.setIsHealthExamination(value);
                    break;
                case "IsGxy":
//					高血压
                    lr.setIsHighBloodPressure(value);
                    break;
                case "IsTnb":
//					糖尿病
                    lr.setIsDiabetes(value);
                    break;
                case "IsGxb":
                    lr.setIsHeartCoronary(value);
//					冠心病
                    break;
                case "IsNzz":
//					脑卒中
                    lr.setIsApoplexy(value);
                    break;
                case "IsYcf":
//					孕产妇
                    lr.setIsMotherhood(value);
                    break;
                case "IsJsb":
//					精神病
                    lr.setIsMentalDisease(value);
                    break;
                case "IsCjr":
//					残疾人
                    lr.setIsBodyDisable(value);
                    break;
                case "DJtdabh":
//					家庭档案
                    lr.setHomeDocNo(value);
                    break;
                case "DXx":
                    lr.setXuexing(value);
                    break;
                case "DJwbs":
                    lr.setAnamnesis(value);
                    break;
//				既往病史
                case "IsJtys":
//					家庭医生
                    lr.setIsHomeDoctor(value);
                    break;
                case "createuser":
                    lr.setCreater(value);
                    break;
                case "dWzd":
//					完整度
                    break;
                case "dGms":
                    lr.setYwgms(value);
                    break;
                case "gxyglk":
//					高血压是否在管理
                    lr.setIsHighBloodPressureMange(value);
                    break;
                case "tnbglk":
//					糖尿病是否在管理
                    lr.setIsDiabetesMange(value);
                    break;
            }
        }
        return lr;
    }

    public static String lrInfoAndHealthExamToXml(LrInfomation lrInformation, HealthExamination examination) {
        Document document = DocumentHelper.createDocument();
        Element rootElement = document.addElement("");
        String xml = "";
        return xml;
    }

    public static String informationToXml(LrInfomation lrInformation) {
        Document document = DocumentHelper.createDocument();
        Element rootElement = document.addElement(PublicHealthInterfaceConfig.XMLTOPERSONS);
        rootElement.addAttribute("return", "TRUE");
        rootElement.addAttribute("value", "0");
        rootElement.addAttribute("username", ToolUtil.getXmlPro("publicHealth", "username"));
        rootElement.addAttribute("prgid", ToolUtil.getXmlPro("publicHealth", "prgid"));
        Element element = rootElement.addElement(PublicHealthInterfaceConfig.ROW);
        element.addAttribute("name", PublicHealthInterfaceConfig.T_DA_JKDA_RKXZL);
        element.addAttribute("jzkh", ToolUtil.getXmlPro("publicHealth", "jzkh"));
        addFieldElement(element, "DGrdabh", lrInformation.getBusinessArchivesNo());
        addFieldElement(element, "DGrdabhshow", lrInformation.getArchivesno());
        addFieldElement(element, "DXm", lrInformation.getName());
        addFieldElement(element, "DSfzh", lrInformation.getIdnum());
        addFieldElement(element, "DPyjm", lrInformation.getNamecode());
        addFieldElement(element, "DYhzgx", lrInformation.getHouseholdRelation());
        addFieldElement(element, "DLxdh", lrInformation.getPhonenum());
        addFieldElement(element, "DEmail", lrInformation.getEmail());
        addFieldElement(element, "DSheng", lrInformation.getSheng());
        addFieldElement(element, "DShi", lrInformation.getShi());
        addFieldElement(element, "DQu", lrInformation.getQu());
        addFieldElement(element, "DJd", lrInformation.getJiedao());
        addFieldElement(element, "DJwh", lrInformation.getShequ());
        addFieldElement(element, "DXxdz", lrInformation.getAddress());
        addFieldElement(element, "DSspq", lrInformation.getSubArea());
        addFieldElement(element, "DJzzk", lrInformation.getDwellState());
        addFieldElement(element, "DXb", lrInformation.getSex());
        addFieldElement(element, "DCsrq", lrInformation.getBirth());
        addFieldElement(element, "DMz", lrInformation.getNation());
        addFieldElement(element, "DWhcd", lrInformation.getEducation());
        addFieldElement(element, "DZy", lrInformation.getOccupation());
        addFieldElement(element, "DHyzk", lrInformation.getMarriage());
        addFieldElement(element, "DYlbxh", lrInformation.getMedicareNo());
        addFieldElement(element, "DYlbxh", lrInformation.getNewFarmingNo());
        addFieldElement(element, "PRgid", lrInformation.getJigou());
        addFieldElement(element, "DLxrxm", lrInformation.getContact());
        addFieldElement(element, "DLxrdh", lrInformation.getContactphone());
        addFieldElement(element, "DLxrdh", lrInformation.getCompany());
        addFieldElement(element, "Sftj", lrInformation.getIsHealthExamination());
        addFieldElement(element, "IsGxy", lrInformation.getIsHighBloodPressure());
        addFieldElement(element, "IsTnb", lrInformation.getIsDiabetes());
        addFieldElement(element, "IsGxb", lrInformation.getIsHeartCoronary());
        addFieldElement(element, "IsNzz", lrInformation.getIsApoplexy());
        addFieldElement(element, "IsYcf", lrInformation.getIsMotherhood());
        addFieldElement(element, "IsJsb", lrInformation.getIsMentalDisease());
        addFieldElement(element, "IsCjr", lrInformation.getIsBodyDisable());
        addFieldElement(element, "DJtdabh", lrInformation.getHomeDocNo());
        addFieldElement(element, "DXx", lrInformation.getXuexing());
        addFieldElement(element, "DJwbs", lrInformation.getAnamnesis());
        addFieldElement(element, "IsJtys", lrInformation.getIsHomeDoctor());
        addFieldElement(element, "dGms", lrInformation.getYwgms());
        addFieldElement(element, "gxyglk", lrInformation.getIsHighBloodPressureMange());
        addFieldElement(element, "tnbglk", lrInformation.getIsDiabetesMange());
        String xml = document.asXML();
        return xml;
    }

    public static String healthExaminationToXml(Peispatient peispatient, HealthExamination examination, List<HealthHospital> healthHospitals,
                                                List<HealthFamily> families, List<HealthMedicineMySQL> medicines, List<HealthDefend> defends) {
        Document document = DocumentHelper.createDocument();
        Element rootElement = document.addElement(PublicHealthInterfaceConfig.XMLTOPERSONS);
        rootElement.addAttribute("return", "TRUE");
        rootElement.addAttribute("biaoshi", "1");
        rootElement.addAttribute("value", "0");
        rootElement.addAttribute("username", ToolUtil.getXmlPro("publicHealth", "username"));
        rootElement.addAttribute("prgid", ToolUtil.getXmlPro("publicHealth", "prgid"));
        Element element = rootElement.addElement(PublicHealthInterfaceConfig.ROW);
        element.addAttribute("name", PublicHealthInterfaceConfig.T_DA_JKDA_RKXZL);
        element.addAttribute("jzkh", ToolUtil.getXmlPro("publicHealth", "jzkh"));
//        addFieldElement(element, "jkbs", "");
        addFieldElement(element, "DSfzh", peispatient.getIdcardno());
        Element subRowElement = addSubRowElement(element, "T_JK_JKTJ");
        addExaminationElement(subRowElement, examination);
        addHospitalAndFamilyElement(subRowElement, healthHospitals, families);
        addMedicineElement(subRowElement, medicines);
        addDefendsElememnt(subRowElement, defends);
        addOlderAblitityElement(element);
        return document.asXML();
    }

    private static void addZhongYiTiZhiElement(Element element, PhysiqueIdentification physiqueIdentification) {
//		if (physiqueIdentification!=null) {
//			Element addSubRowElement = addSubRowElement(element, "T_LNR_ZYYTZGL");
//			addFieldElement(addSubRowElement, "LJl", data);
//		}

    }

    private static void addOlderAblitityElement(Element element) {
//		Element subRowElement = addSubRowElement(element, "T_JG_LNRSF");
//		addFieldElement(subRowElement, "id", "");
//		addFieldElement(subRowElement, "jcpf", "");
//		addFieldElement(subRowElement, "sxpf", "");
//		addFieldElement(subRowElement, "rcpf", "");
//		addFieldElement(subRowElement, "cypf", "");
//		addFieldElement(subRowElement, "hdpf", "");
//		addFieldElement(subRowElement, "happentime", "");
//		addFieldElement(subRowElement, "GXcsfmb", "");		

    }

    private static void addExaminationElement(Element element, HealthExamination examination) {
        if (examination != null) {
            addFieldElement(element, "id", examination.getId());
            addFieldElement(element, "GZhzh", examination.getSymptom());
            addFieldElement(element, "GZzqt", examination.getSymptom());
            addFieldElement(element, "lnrzkpg", examination.getOlderZwpg().toString());
            addFieldElement(element, "lnrzlpg", examination.getOlderZlnl().toString());
            addFieldElement(element, "GLnrrz", examination.getOlderRzgn());
            addFieldElement(element, "GLnrrzfen", examination.getOlderRzgnFen());
            addFieldElement(element, "GLnrqg", examination.getOlderQgzt());
            addFieldElement(element, "GLnrqgfen", examination.getOlderQgztFen());
            addFieldElement(element, "GDlpl", examination.getSportrate());
            addFieldElement(element, "GMcdlsj", examination.getPertimesport());
            addFieldElement(element, "GJcdlsj", examination.getSporttime().toString());
            addFieldElement(element, "GDlfs", examination.getSporttype());
            addFieldElement(element, "GNxgjb", examination.getNxgjb());
            addFieldElement(element, "GNxgjbqt", examination.getNxgjbOther());
            addFieldElement(element, "GSzjb", examination.getSzjb());
            addFieldElement(element, "GSzjbqt", examination.getSzjbOther());
            addFieldElement(element, "GXzjb", examination.getXzjb());
            addFieldElement(element, "GXzjbqt", examination.getXzjbOther());
            addFieldElement(element, "GYbjb", examination.getYbjb());
            addFieldElement(element, "GYbjbqt", examination.getYbjbOther());
            addFieldElement(element, "GSjxtjb", examination.getSjxtjb());
            addFieldElement(element, "GSjxtjbqt", examination.getSjxtjbOther());
            addFieldElement(element, "GQtxtjb", examination.getQtxtjb());
            addFieldElement(element, "GQtxtjbqt", examination.getQtxtjbOther());
            addFieldElement(element, "GYsxg", examination.getEatstate());
            addFieldElement(element, "GXyzk", String.valueOf(examination.getSmokestate()));
            addFieldElement(element, "GRxyl", String.valueOf(examination.getPerdaysmoke()));
            addFieldElement(element, "GKsxynl", examination.getSmokestart().toString());
            addFieldElement(element, "GJynl", examination.getSmokeend().toString());
            addFieldElement(element, "GYjpl", examination.getDrinkstate().toString());
            addFieldElement(element, "GRyjl", String.valueOf(examination.getPerdaydrink()));
            addFieldElement(element, "GSfjj", String.valueOf(examination.getQuietdrink()));
            addFieldElement(element, "GJjnl", String.valueOf(examination.getQuietdrinkage()));
            addFieldElement(element, "GKsyjnl", String.valueOf(examination.getStartdrink()));
            addFieldElement(element, "GYnnsfyj", String.valueOf(examination.getDrunked()));
            addFieldElement(element, "GYjzl", examination.getDrinktype());
            addFieldElement(element, "GYjzlqt", examination.getDrinkTypeOther());
            addFieldElement(element, "GYwzybl", examination.getDanger());
            addFieldElement(element, "GJtzy", examination.getWorkerType());
            addFieldElement(element, "GCysj", examination.getWorkDate());
            addFieldElement(element, "fenchen", examination.getFenchen());
            addFieldElement(element, "fchcs", examination.getFchcs());
            addFieldElement(element, "fchy", examination.getFchy());
            addFieldElement(element, "GShexian", examination.getShexian());
            addFieldElement(element, "GSxfhcs", examination.getSxfhcs());
            addFieldElement(element, "GSxfhcsqt", examination.getSxfhcsqt());
            addFieldElement(element, "wuliyinsu", examination.getWuli());
            addFieldElement(element, "wlcs", examination.getWlcs());
            addFieldElement(element, "wly", examination.getWly());
            addFieldElement(element, "GHxp", examination.getHuaxue());
            addFieldElement(element, "GHxpfhcs", examination.getHxpfhcs());
            addFieldElement(element, "GHxpfhcsjt", examination.getHxpfhcsjt());
            addFieldElement(element, "blqita", examination.getBlqita());
            addFieldElement(element, "blqtcs", examination.getBlqtcs());
            addFieldElement(element, "qty", String.valueOf(examination.getQty()));
            addFieldElement(element, "GTw", String.valueOf(examination.getTemperature()));
            addFieldElement(element, "GMb", String.valueOf(examination.getPulserate()));
            addFieldElement(element, "GHx", String.valueOf(examination.getBreathrate()));
            addFieldElement(element, "GXyzc1", String.valueOf(examination.getLeftbldpressure()));
            addFieldElement(element, "GXyzc2", String.valueOf(examination.getLeftlowpressure()));
            addFieldElement(element, "GXyyc1", String.valueOf(examination.getRightbldpressure()));
            addFieldElement(element, "GXyyc2", String.valueOf(examination.getRightlowpressure()));
            addFieldElement(element, "GSg", String.valueOf(examination.getHeight()));
            addFieldElement(element, "GTzh", String.valueOf(examination.getWeight()));
            addFieldElement(element, "GYw", String.valueOf(examination.getWaistline()));
            addFieldElement(element, "GTzhzh", String.valueOf(examination.getBodyindex()));
            addFieldElement(element, "GKouchun", String.valueOf(examination.getKqKc()));
            addFieldElement(element, "GChilei", examination.getKqCl());
            addFieldElement(element, "GClyi", "");
            addFieldElement(element, "GYanbu", String.valueOf(examination.getKqYb()));
            addFieldElement(element, "GZysl", String.valueOf(examination.getLefteye()));
            addFieldElement(element, "GYysl", String.valueOf(examination.getRighteye()));
            addFieldElement(element, "GZyjz", String.valueOf(examination.getJzLefteye()));
            addFieldElement(element, "GYyjz", String.valueOf(examination.getJzRighteye()));
            addFieldElement(element, "GTl", String.valueOf(examination.getTl()));
            addFieldElement(element, "GYdgn", String.valueOf(examination.getSportability()));
            addFieldElement(element, "GPfgm", examination.getSkinOther());
            addFieldElement(element, "GPfqt", examination.getSkinOther());
            addFieldElement(element, "GGongmo", String.valueOf(examination.getGm()));
            addFieldElement(element, "GGmqt", examination.getGmqt());
            addFieldElement(element, "GLbj", String.valueOf(examination.getLbj()));
            addFieldElement(element, "GLbjqt", examination.getLbjqt());
            addFieldElement(element, "GTzx", String.valueOf(examination.getTzx()));
            addFieldElement(element, "GHxy", String.valueOf(examination.getHxy()));
            addFieldElement(element, "GHxyyc", examination.getHxyYc());
            addFieldElement(element, "GLy", String.valueOf(examination.getLy()));
            addFieldElement(element, "GLyyc", examination.getLyqt());
            addFieldElement(element, "GXinlv", String.valueOf(examination.getHeartrate()));
            addFieldElement(element, "GXinlvci", String.valueOf(examination.getHeartrhythm()));
            addFieldElement(element, "GZayin", String.valueOf(examination.getOthervoice()));
            addFieldElement(element, "GZayinyo", examination.getOtherVoiceDesc());
            addFieldElement(element, "GYato", String.valueOf(examination.getFbYt()));
            addFieldElement(element, "GYatoyo", examination.getFbYtDesc());
            addFieldElement(element, "GBk", String.valueOf(examination.getFbBk()));
            addFieldElement(element, "GBkyo", examination.getFbBkDesc());
            addFieldElement(element, "GGanda", String.valueOf(examination.getFbGd()));
            addFieldElement(element, "GGandayo", examination.getFbGdDesc());
            addFieldElement(element, "GPida", String.valueOf(examination.getFbPd()));
            addFieldElement(element, "GPidayo", examination.getFbPdDesc());
            addFieldElement(element, "GZhuoyin", String.valueOf(examination.getFbZy()));
            addFieldElement(element, "GZhuoyinyo", examination.getFbZyDesc());
            addFieldElement(element, "GXzsz", String.valueOf(examination.getXzsz()));
            addFieldElement(element, "hb", examination.getXhdb());
            addFieldElement(element, "wbc", examination.getBxb());
            addFieldElement(element, "plt", examination.getXxb());
            addFieldElement(element, "GXcgqt", examination.getXcgOther());
            addFieldElement(element, "GNdb", examination.getNdb());
            addFieldElement(element, "GNt", examination.getNt());
            addFieldElement(element, "GNtt", examination.getNtt());
            addFieldElement(element, "GNqx", examination.getNqx());
            addFieldElement(element, "GNcgqt", examination.getNcgOther());
            addFieldElement(element, "GKfxt", examination.getKfxt());
            addFieldElement(element, "GKfxtqt", 1);
            addFieldElement(element, "alt", String.valueOf(examination.getXqgbzam()));
            addFieldElement(element, "ast", String.valueOf(examination.getXqgczam()));
            addFieldElement(element, "alb", String.valueOf(examination.getBdb()));
            addFieldElement(element, "tbil", String.valueOf(examination.getZdhs()));
            addFieldElement(element, "dbil", String.valueOf(examination.getJhdhs()));
            addFieldElement(element, "scr", String.valueOf(examination.getXqjg()));
            addFieldElement(element, "bun", examination.getXns());
            addFieldElement(element, "GSgnxjnd", examination.getXjnd());
            addFieldElement(element, "GSgnxnnd", examination.getXnnd());
            addFieldElement(element, "GSgnxnnd", examination.getXnnd());
            addFieldElement(element, "cho", examination.getZdgc());
            addFieldElement(element, "tg", examination.getGysz());
            addFieldElement(element, "ldlc", examination.getXqdmdzdb());
            addFieldElement(element, "hdlc", examination.getXqgmdzdb());
            addFieldElement(element, "GQt", examination.getBcOther());
            addFieldElement(element, "GQtyc", examination.getBcOther());
            addFieldElement(element, "GFbbc", examination.getFbbc());
            addFieldElement(element, "GFbbcyc", examination.getFbbc());
            addFieldElement(element, "GAbo", "");
            addFieldElement(element, "GRh", "");
            addFieldElement(element, "happentime",
                    DateUtil.format(examination.getCreatedate(), "yyyy-MM-dd"));
            addFieldElement(element, "field2", examination.getDoctor());
            addFieldElement(element, "GJkpj", examination.getJkpj());
            addFieldElement(element, "GJkpjyc1", examination.getJkpjYc());
            addFieldElement(element, "GJkpjyc2", examination.getJkpjYc());
            addFieldElement(element, "GJkpjyc3", examination.getJkpjYc());
            addFieldElement(element, "GJkpjyc4", examination.getJkpjYc());
            addFieldElement(element, "GJkzd", examination.getJkzd());
            addFieldElement(element, "GWxyskz", examination.getWxkz());
            addFieldElement(element, "GWxystz", examination.getWxkz());
            addFieldElement(element, "GWsysym", examination.getWxkz());
            addFieldElement(element, "GWxysqt", examination.getWxkz());
        }
    }

    public static void addDefendsElememnt(Element element, List<HealthDefend> defends) {
        if (defends != null) {
            for (HealthDefend healthDefend : defends) {
                Element subRowElement = addSubRowElement(element, "T_TJ_FMYGHYFB");
                addFieldElement(subRowElement, "FId", healthDefend.getId());
                addFieldElement(subRowElement, "FJzmc", healthDefend.getName());
                addFieldElement(subRowElement, "FJzrq",
                        DateUtil.format(healthDefend.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
                addFieldElement(subRowElement, "FJzjg", healthDefend.getHospital());
                addFieldElement(subRowElement, "FHappentime",
                        DateUtil.format(healthDefend.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
            }
        }
    }

    public static void addMedicineElement(Element element, List<HealthMedicineMySQL> medicines) {
        if (medicines != null) {
            for (HealthMedicineMySQL healthMedicineOracle : medicines) {
                Element subRowElement = addSubRowElement(element, "T_TJ_YYQKB");
                addFieldElement(subRowElement, "YId", healthMedicineOracle.getId());
                addFieldElement(subRowElement, "YYwmc", healthMedicineOracle.getName());
                addFieldElement(subRowElement, "YYongfa", healthMedicineOracle.getUsing());
                addFieldElement(subRowElement, "YYongl", healthMedicineOracle.getUsingquantity());
                addFieldElement(subRowElement, "YYysj", healthMedicineOracle.getUsingtime());
                addFieldElement(subRowElement, "YFyycx", healthMedicineOracle.getUsingtime());
                addFieldElement(subRowElement, "YHappentime",
                        DateUtil.format(healthMedicineOracle.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
            }
        }
    }

    public static void addHospitalAndFamilyElement(Element element, List<HealthHospital> healthHospitals,
                                                   List<HealthFamily> families) {
        if (healthHospitals != null) {
            for (HealthHospital healthHospital : healthHospitals) {
                Element subRowElement = addSubRowElement(element, "T_TJ_ZYZLQKB");
                addFieldElement(subRowElement, "ZId", healthHospital.getId());
                addFieldElement(subRowElement, "ZType", "1");
                addFieldElement(subRowElement, "ZRyjcrq",
                        DateUtil.format(healthHospital.getIntime(), "yyyy-MM-dd HH:mm:ss"));
                addFieldElement(subRowElement, "ZCyccrq",
                        DateUtil.format(healthHospital.getOuttime(), "yyyy-MM-dd HH:mm:ss"));
                addFieldElement(subRowElement, "ZYuanyin", healthHospital.getCause());
                addFieldElement(subRowElement, "ZYljgmc", healthHospital.getHospital());
                addFieldElement(subRowElement, "ZBingah", healthHospital.getIdnum());
                addFieldElement(subRowElement, "ZHappentime",
                        DateUtil.format(healthHospital.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
            }
        }
        if (families != null) {
            for (HealthFamily healthFamily : families) {
                Element subRowElement = addSubRowElement(element, "T_TJ_ZYZLQKB");
                addFieldElement(subRowElement, "ZId", healthFamily.getId());
                addFieldElement(subRowElement, "ZType", "1");
                addFieldElement(subRowElement, "ZRyjcrq",
                        DateUtil.format(healthFamily.getIntime(), "yyyy-MM-dd HH:mm:ss"));
                addFieldElement(subRowElement, "ZCyccrq",
                        DateUtil.format(healthFamily.getOuttime(), "yyyy-MM-dd HH:mm:ss"));
                addFieldElement(subRowElement, "ZYuanyin", healthFamily.getCause());
                addFieldElement(subRowElement, "ZYljgmc", healthFamily.getHospital());
                addFieldElement(subRowElement, "ZBingah", healthFamily.getIdnum());
                addFieldElement(subRowElement, "ZHappentime",
                        DateUtil.format(healthFamily.getCreatedate(), "yyyy-MM-dd HH:mm:ss"));
            }
        }
    }

    private static Element addSubRowElement(Element element, String name) {
        Element addElement = element.addElement("subrow");
        addElement.addAttribute("name", name);
        return addElement;

    }

    public static void addFieldElement(Element element, String name, Object data) {
        Element addElement = element.addElement("field");
        addElement.addAttribute("name", name);
        addElement.addCDATA(String.valueOf(data).equals("null") ? "" : data.toString());
    }

}
