package com.zhongkang.publichealth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 09:51
 */
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}