package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 14:55
 */
@Repository
public interface HealthFamilyDao extends JpaRepository<HealthFamily, String> {
    List<HealthFamily> findByHealthid(String healthid);
}
