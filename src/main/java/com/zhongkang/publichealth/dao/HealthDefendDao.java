package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthDefend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 09:59
 */
@Repository
public interface HealthDefendDao extends JpaRepository<HealthDefend,String> {
    List<HealthDefend> findByHealthid(String healthid);
}
