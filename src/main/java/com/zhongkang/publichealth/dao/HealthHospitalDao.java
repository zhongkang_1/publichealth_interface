package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthHospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 14:56
 */
@Repository
public interface HealthHospitalDao extends JpaRepository<HealthHospital,String> {
    List<HealthHospital> findByHealthid(String healthid);
}
