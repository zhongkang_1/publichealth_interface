package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthMedicineOracle;
import com.zhongkang.publichealth.pojo.HealthMedicineMySQL;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 14:42
 */
@Repository
public interface HealthMedicineMySQLDao extends JpaRepository<HealthMedicineMySQL, String> {
    List<HealthMedicineMySQL> findByHealthid(String healthid);
}
