package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthMedicineOracle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 14:56
 */
@Repository
public interface HealthMedicineOracleDao extends JpaRepository<HealthMedicineOracle, String> {
    List<HealthMedicineOracle> findByHealthid(String healthid);
}
