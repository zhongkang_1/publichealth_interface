package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.LrInfomation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 14:55
 */
@Repository
public interface LrInfomationDao extends JpaRepository<LrInfomation, String> {
    LrInfomation findByIdnum(@Param("idnum") String idnum);
}
