package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.Peispatient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 13:57
 */
@Repository
public interface PeispatientDao extends JpaRepository<Peispatient,String> {
    Peispatient findByIdcardno(String idcardno);

    List<Peispatient> findAllByIdcardnoOrderByCreatedateDesc(String idcardno);

    Peispatient findFirstByIdcardnoOrderByCreatedateDesc(String idcardno);
}
