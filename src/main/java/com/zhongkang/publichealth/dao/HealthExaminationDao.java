package com.zhongkang.publichealth.dao;

import com.zhongkang.publichealth.pojo.HealthExamination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 14:55
 */
@Repository
public interface HealthExaminationDao extends JpaRepository<HealthExamination, String> {
    //    @Query(nativeQuery = true,value = "select * from health_examination where cast(PATIENTCODE as char) = cast(? as char )")
    HealthExamination findByPatientcodeLike(String patientcode);

    HealthExamination findFirstByPatientcode(String patientcode);

    HealthExamination findByIdnum(String idnum);

    List<HealthExamination> findAllByIdnumOrderByCreatedateDesc(String idnum);
}
