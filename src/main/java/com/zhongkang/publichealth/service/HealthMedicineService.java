package com.zhongkang.publichealth.service;

import cn.hutool.core.bean.BeanUtil;
import com.zhongkang.publichealth.dao.HealthMedicineOracleDao;
import com.zhongkang.publichealth.dao.HealthMedicineMySQLDao;
import com.zhongkang.publichealth.pojo.HealthMedicineOracle;
import com.zhongkang.publichealth.pojo.HealthMedicineMySQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 15:04
 */
@Service
public class HealthMedicineService {
    @Autowired
    private HealthMedicineOracleDao healthMedicineOracleDao;
    @Autowired
    private HealthMedicineMySQLDao healthMedicineMySQLDao;

    public List<HealthMedicineMySQL> findByHealthid(String id) {
        return healthMedicineMySQLDao.findByHealthid(id);
    }

    @Transactional
    public void saveAll(List<HealthMedicineOracle> medicines) {
        ArrayList<HealthMedicineMySQL> healthMedicineMySQLS = new ArrayList<>();
        for (HealthMedicineOracle medicine : medicines) {
            HealthMedicineMySQL healthMedicineMySQL = new HealthMedicineMySQL();
            BeanUtil.copyProperties(medicine, healthMedicineMySQL);
            healthMedicineMySQLS.add(healthMedicineMySQL);
        }
        healthMedicineMySQLDao.saveAll(healthMedicineMySQLS);
    }
}