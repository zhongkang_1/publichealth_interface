package com.zhongkang.publichealth.service;

import com.zhongkang.publichealth.dao.HealthDefendDao;
import com.zhongkang.publichealth.pojo.HealthDefend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 15:02
 */
@Service
public class HealthDefendService {
    @Autowired
    private HealthDefendDao healthDefendDao;

    public List<HealthDefend> findByHealthid(String healthId) {
        return healthDefendDao.findByHealthid(healthId);
    }

    @Transactional
    public void saveAll(List<HealthDefend> defends) {
        healthDefendDao.saveAll(defends);
    }
}