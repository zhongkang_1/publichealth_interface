package com.zhongkang.publichealth.service;

import com.zhongkang.publichealth.dao.HealthExaminationDao;
import com.zhongkang.publichealth.pojo.HealthExamination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 15:02
 */
@Service
public class HealthExaminationService {
    @Autowired
    private HealthExaminationDao healthExaminationDao;

    public HealthExamination findByPatientcode(String patientcode) {
        return healthExaminationDao.findFirstByPatientcode(patientcode);
    }

    public HealthExamination findByIdnum(String idnum) {
        return healthExaminationDao.findByIdnum(idnum);
    }

    public HealthExamination findNewestByIdNum(String idnum) {
        List<HealthExamination> healthExaminations = healthExaminationDao.findAllByIdnumOrderByCreatedateDesc(idnum);
        if (healthExaminations != null && healthExaminations.size() > 0) {
            return healthExaminations.get(0);
        } else {
            return null;
        }
    }

    @Transactional
    public void save(HealthExamination examination) {
        healthExaminationDao.save(examination);
    }
}