package com.zhongkang.publichealth.service;

import com.zhongkang.publichealth.dao.HealthFamilyDao;
import com.zhongkang.publichealth.pojo.HealthFamily;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 15:03
 */
@Service
public class HealthFamilyService {
    @Autowired
    private HealthFamilyDao healthFamilyDao;

    public List<HealthFamily> findByHealthid(String id) {
        return healthFamilyDao.findByHealthid(id);
    }

    @Transactional
    public void saveAll(List<HealthFamily> families) {
        healthFamilyDao.saveAll(families);
    }
}