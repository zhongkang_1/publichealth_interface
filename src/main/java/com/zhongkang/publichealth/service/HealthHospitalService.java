package com.zhongkang.publichealth.service;

import com.zhongkang.publichealth.dao.HealthHospitalDao;
import com.zhongkang.publichealth.pojo.HealthHospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 15:03
 */
@Service
public class HealthHospitalService {
    @Autowired
    private HealthHospitalDao healthHospitalDao;

    public List<HealthHospital> findByHealthid(String id) {
        return healthHospitalDao.findByHealthid(id);
    }

    @Transactional
    public void saveAll(List<HealthHospital> hospitals) {
        healthHospitalDao.saveAll(hospitals);
    }
}