package com.zhongkang.publichealth.service;

import cn.hutool.core.bean.BeanUtil;
import com.zhongkang.publichealth.dao.LrInfomationDao;
import com.zhongkang.publichealth.pojo.LrInfomation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-11 13:52
 */
@Service
public class LrInformationService {
    @Autowired
    private LrInfomationDao lrInfomationDao;

    @Transactional
    public void saveAll(ArrayList<LrInfomation> lrInfomations) {
        lrInfomationDao.saveAll(lrInfomations);
    }

    public List<LrInfomation> findAll() {
        return lrInfomationDao.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdateAll(List<LrInfomation> lrInfomation) {
        for (LrInfomation infomation : lrInfomation) {
            LrInfomation lrInfomationDb = lrInfomationDao.findByIdnum(infomation.getIdnum());
            if (lrInfomationDb != null) {
                BeanUtil.copyProperties(lrInfomation, lrInfomationDb, "id");
                lrInfomationDao.save(lrInfomationDb);
            } else {
                lrInfomationDao.save(infomation);
            }
        }
    }

    public LrInfomation findByIdnum(String idNum) {
        return lrInfomationDao.findByIdnum(idNum);
    }
}