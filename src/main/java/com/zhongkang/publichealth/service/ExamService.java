package com.zhongkang.publichealth.service;

import com.zhongkang.publichealth.dao.*;
import com.zhongkang.publichealth.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 10:12
 */
@Service
public class ExamService {
    @Autowired
    private HealthExaminationDao healthExaminationDao;
    @Autowired
    private HealthHospitalDao healthHospitalDao;
    @Autowired
    private HealthFamilyDao healthFamilyDao;
    @Autowired
    private HealthMedicineOracleDao healthMedicineOracleDao;
    @Autowired
    private HealthDefendDao healthDefendDao;
    @Autowired
    private LrInfomationDao lrInfomationDao;
    @Autowired
    private PeispatientDao peispatientDao;

    public List<ExamDTO> getExamInfoByIdnums(List<String> idNums) {

        ArrayList<ExamDTO> examDTOS = new ArrayList<>();
        for (String idNum : idNums) {
            boolean retry = true;
            while (retry) {
                try {
                    ExamDTO examDTO = new ExamDTO();
                    Peispatient peispatient = peispatientDao.findFirstByIdcardnoOrderByCreatedateDesc(idNum);
                    examDTO.setPeispatient(peispatient);
                    if (peispatient != null ) {
                        String patientcode = peispatient.getPatientcode();
                        HealthExamination examination = healthExaminationDao.findFirstByPatientcode(String.valueOf(patientcode));
                        examDTO.setExamination(examination);
                        if (examination != null) {
                            List<HealthHospital> hospitals = healthHospitalDao.findByHealthid(examination.getId());
                            examDTO.setHospitals(hospitals);
                            List<HealthFamily> families = healthFamilyDao.findByHealthid(examination.getId());
                            examDTO.setFamilies(families);
                            List<HealthMedicineOracle> medicines = healthMedicineOracleDao.findByHealthid(examination.getId());
                            examDTO.setMedicines(medicines);
                            List<HealthDefend> defends = healthDefendDao.findByHealthid(examination.getId());
                            examDTO.setDefends(defends);
                        }
                    }
                    examDTOS.add(examDTO);
                    retry = false;
                } catch (Exception e) {
                    retry = e.getMessage().contains("由于连接方在一段时间后没有正确答复或连接的主机没有反应");
                }
            }
        }
        return examDTOS;
    }
}

