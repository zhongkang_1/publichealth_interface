package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 08:32
 */
@Entity
@Table(name = "HEALTH_HOSPITAL")
public class HealthHospital {
    private String id;
    private String creater;
    private Date createdate;
    private String modifyer;
    private Date modifydate;
    private String hospital;
    private Date intime;
    private Date outtime;
    private String cause;
    private String idnum;
    private String healthid;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CREATER", nullable = true, length = 32)
    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYER", nullable = true, length = 32)
    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "HOSPITAL", nullable = true, length = 32)
    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    @Basic
    @Column(name = "INTIME", nullable = true)
    public Date getIntime() {
        return intime;
    }

    public void setIntime(Date intime) {
        this.intime = intime;
    }

    @Basic
    @Column(name = "OUTTIME", nullable = true)
    public Date getOuttime() {
        return outtime;
    }

    public void setOuttime(Date outtime) {
        this.outtime = outtime;
    }

    @Basic
    @Column(name = "CAUSE", nullable = true, length = 255)
    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Basic
    @Column(name = "IDNUM", nullable = true, length = 45)
    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    @Basic
    @Column(name = "HEALTHID", nullable = true, length = 32)
    public String getHealthid() {
        return healthid;
    }

    public void setHealthid(String healthid) {
        this.healthid = healthid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthHospital that = (HealthHospital) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creater, that.creater) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifyer, that.modifyer) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(hospital, that.hospital) &&
                Objects.equals(intime, that.intime) &&
                Objects.equals(outtime, that.outtime) &&
                Objects.equals(cause, that.cause) &&
                Objects.equals(idnum, that.idnum) &&
                Objects.equals(healthid, that.healthid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creater, createdate, modifyer, modifydate, hospital, intime, outtime, cause, idnum, healthid);
    }
}