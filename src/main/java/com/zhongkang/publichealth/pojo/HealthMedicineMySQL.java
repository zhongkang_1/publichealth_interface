package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 14:42
 */
@Entity
@Table(name = "health_medicine")
public class HealthMedicineMySQL {
    private String id;
    private Date createdate;
    private Date modifydate;
    private String name;
    private String classId;
    private String creater;
    private String modifyer;
    private String using;
    private String usingquantity;
    private String usingtime;
    private String comply;
    private String healthid;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid")
    @Column(name = "id", nullable = false, length = 50)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "createdate", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "modifydate", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "class_id", nullable = true, length = 50)
    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    @Basic
    @Column(name = "creater", nullable = true, length = 50)
    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    @Basic
    @Column(name = "modifyer", nullable = true, length = 50)
    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer;
    }

    @Basic
    @Column(name = "using1", nullable = true, length = 50)
    public String getUsing() {
        return using;
    }

    public void setUsing(String using) {
        this.using = using;
    }

    @Basic
    @Column(name = "usingquantity", nullable = true, length = 50)
    public String getUsingquantity() {
        return usingquantity;
    }

    public void setUsingquantity(String usingquantity) {
        this.usingquantity = usingquantity;
    }

    @Basic
    @Column(name = "usingtime", nullable = true, length = 50)
    public String getUsingtime() {
        return usingtime;
    }

    public void setUsingtime(String usingtime) {
        this.usingtime = usingtime;
    }

    @Basic
    @Column(name = "comply", nullable = true, length = 50)
    public String getComply() {
        return comply;
    }

    public void setComply(String comply) {
        this.comply = comply;
    }

    @Basic
    @Column(name = "healthid", nullable = true, length = 50)
    public String getHealthid() {
        return healthid;
    }

    public void setHealthid(String healthid) {
        this.healthid = healthid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthMedicineMySQL that = (HealthMedicineMySQL) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(name, that.name) &&
                Objects.equals(classId, that.classId) &&
                Objects.equals(creater, that.creater) &&
                Objects.equals(modifyer, that.modifyer) &&
                Objects.equals(using, that.using) &&
                Objects.equals(usingquantity, that.usingquantity) &&
                Objects.equals(usingtime, that.usingtime) &&
                Objects.equals(comply, that.comply) &&
                Objects.equals(healthid, that.healthid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdate, modifydate, name, classId, creater, modifyer, using, usingquantity, usingtime, comply, healthid);
    }
}