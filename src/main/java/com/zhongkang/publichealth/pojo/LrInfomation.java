package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 08:32
 */
@Entity
@Table(name = "LR_INFOMATION")
public class LrInfomation {
    private String id;
    private String creater;
    private Date createdate;
    private String modifyer;
    private Date modifydate;
    private String delFlag;
    private String remarks;
    private String name;
    private String sex;
    private String birth;
    private Long age;
    private String idnum;
    private String phonenum;
    private String company;
    private String nation;
    private String xuexing;
    private String contact;
    private String contactphone;
    private String education;
    private String occupation;
    private String marriage;
    private String payment;
    private String ywgms;
    private String bls;
    private String ycbs;
    private String cjqk;
    private String resident;
    private String cfpqss;
    private String rllx;
    private String drink;
    private String toilet;
    private String qcl;
    private String fqbs;
    private String mqbs;
    private String xdjmbs;
    private String znbs;
    private String rhlx;
    private String path;
    private String rwm;
    private String archivesno;
    private String address;
    private String jigou;
    private String relations;
    private String qtywgm;
    private String fqbsbc;
    private String mqbsbc;
    private String xdjmbsbc;
    private String znbsbc;
    private String blsdetail;
    private String ssmz;
    private String namecode;
    private String entrymode;
    private String specialcategory;
    private String shineng;
    private String shizhi;
    private String canji;
    private String zili;
    private String servicepurchase;
    private String liveconditions;
    private String economicsource;
    private String hobby;
    private String faith;
    private String others;
    private String phonenum2;
    private String sheng;
    private String shi;
    private String qu;
    private String jiedao;
    private String shequ;
    private Long serviceObj;
    private Long serviceClass;
    private String closeNote;
    private Boolean isClosed;
    private String jhsy;
    private String povertyAlleviation;
    private String govOlderId;
    private String specialFlags;
    private String isDead;
    private String patientcode;
    private String email;
    private String subArea;
    private String dwellState;
    private String medicareNo;
    private String newFarmingNo;
    private String isHealthExamination;
    private String isHighBloodPressure;
    private String isDiabetes;
    private String isHeartCoronary;
    private String isMentalDisease;
    private String homeDocNo;
    private String anamnesis;
    private String isHomeDoctor;
    private String isHighBloodPressureMange;
    private String isDiabetesMange;
    private String householdRelation;
    private String businessArchivesNo;
    private String isApoplexy;
    private String isMotherhood;
    private String isBodyDisable;
    private String isFromGw;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CREATER", nullable = true, length = 32)
    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYER", nullable = true, length = 32)
    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "DEL_FLAG", nullable = true, length = 1)
    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Basic
    @Column(name = "REMARKS", nullable = true, length = 255)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "NAME", nullable = true, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "SEX", nullable = true, length = 255)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "BIRTH", nullable = true, length = 255)
    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    @Basic
    @Column(name = "AGE", nullable = true, precision = 0)
    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    @Basic
    @Column(name = "IDNUM", nullable = true, length = 255)
    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    @Basic
    @Column(name = "PHONENUM", nullable = true, length = 255)
    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    @Basic
    @Column(name = "COMPANY", nullable = true, length = 255)
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Basic
    @Column(name = "NATION", nullable = true, length = 255)
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    @Basic
    @Column(name = "XUEXING", nullable = true, length = 255)
    public String getXuexing() {
        return xuexing;
    }

    public void setXuexing(String xuexing) {
        this.xuexing = xuexing;
    }

    @Basic
    @Column(name = "CONTACT", nullable = true, length = 255)
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Basic
    @Column(name = "CONTACTPHONE", nullable = true, length = 255)
    public String getContactphone() {
        return contactphone;
    }

    public void setContactphone(String contactphone) {
        this.contactphone = contactphone;
    }

    @Basic
    @Column(name = "EDUCATION", nullable = true, length = 255)
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Basic
    @Column(name = "OCCUPATION", nullable = true, length = 255)
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Basic
    @Column(name = "MARRIAGE", nullable = true, length = 255)
    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    @Basic
    @Column(name = "PAYMENT", nullable = true, length = 255)
    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    @Basic
    @Column(name = "YWGMS", nullable = true, length = 255)
    public String getYwgms() {
        return ywgms;
    }

    public void setYwgms(String ywgms) {
        this.ywgms = ywgms;
    }

    @Basic
    @Column(name = "BLS", nullable = true, length = 255)
    public String getBls() {
        return bls;
    }

    public void setBls(String bls) {
        this.bls = bls;
    }

    @Basic
    @Column(name = "YCBS", nullable = true, length = 255)
    public String getYcbs() {
        return ycbs;
    }

    public void setYcbs(String ycbs) {
        this.ycbs = ycbs;
    }

    @Basic
    @Column(name = "CJQK", nullable = true, length = 255)
    public String getCjqk() {
        return cjqk;
    }

    public void setCjqk(String cjqk) {
        this.cjqk = cjqk;
    }

    @Basic
    @Column(name = "RESIDENT", nullable = true, length = 255)
    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
    }

    @Basic
    @Column(name = "CFPQSS", nullable = true, length = 255)
    public String getCfpqss() {
        return cfpqss;
    }

    public void setCfpqss(String cfpqss) {
        this.cfpqss = cfpqss;
    }

    @Basic
    @Column(name = "RLLX", nullable = true, length = 255)
    public String getRllx() {
        return rllx;
    }

    public void setRllx(String rllx) {
        this.rllx = rllx;
    }

    @Basic
    @Column(name = "DRINK", nullable = true, length = 255)
    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Basic
    @Column(name = "TOILET", nullable = true, length = 255)
    public String getToilet() {
        return toilet;
    }

    public void setToilet(String toilet) {
        this.toilet = toilet;
    }

    @Basic
    @Column(name = "QCL", nullable = true, length = 255)
    public String getQcl() {
        return qcl;
    }

    public void setQcl(String qcl) {
        this.qcl = qcl;
    }

    @Basic
    @Column(name = "FQBS", nullable = true, length = 255)
    public String getFqbs() {
        return fqbs;
    }

    public void setFqbs(String fqbs) {
        this.fqbs = fqbs;
    }

    @Basic
    @Column(name = "MQBS", nullable = true, length = 255)
    public String getMqbs() {
        return mqbs;
    }

    public void setMqbs(String mqbs) {
        this.mqbs = mqbs;
    }

    @Basic
    @Column(name = "XDJMBS", nullable = true, length = 255)
    public String getXdjmbs() {
        return xdjmbs;
    }

    public void setXdjmbs(String xdjmbs) {
        this.xdjmbs = xdjmbs;
    }

    @Basic
    @Column(name = "ZNBS", nullable = true, length = 255)
    public String getZnbs() {
        return znbs;
    }

    public void setZnbs(String znbs) {
        this.znbs = znbs;
    }

    @Basic
    @Column(name = "RHLX", nullable = true, length = 255)
    public String getRhlx() {
        return rhlx;
    }

    public void setRhlx(String rhlx) {
        this.rhlx = rhlx;
    }

    @Basic
    @Column(name = "PATH", nullable = true, length = 255)
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Basic
    @Column(name = "RWM", nullable = true, length = 255)
    public String getRwm() {
        return rwm;
    }

    public void setRwm(String rwm) {
        this.rwm = rwm;
    }

    @Basic
    @Column(name = "ARCHIVESNO", nullable = true, length = 12)
    public String getArchivesno() {
        return archivesno;
    }

    public void setArchivesno(String archivesno) {
        this.archivesno = archivesno;
    }

    @Basic
    @Column(name = "ADDRESS", nullable = true, length = 300)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "JIGOU", nullable = true, length = 255)
    public String getJigou() {
        return jigou;
    }

    public void setJigou(String jigou) {
        this.jigou = jigou;
    }

    @Basic
    @Column(name = "RELATIONS", nullable = true, length = 255)
    public String getRelations() {
        return relations;
    }

    public void setRelations(String relations) {
        this.relations = relations;
    }

    @Basic
    @Column(name = "QTYWGM", nullable = true, length = 255)
    public String getQtywgm() {
        return qtywgm;
    }

    public void setQtywgm(String qtywgm) {
        this.qtywgm = qtywgm;
    }

    @Basic
    @Column(name = "FQBSBC", nullable = true, length = 255)
    public String getFqbsbc() {
        return fqbsbc;
    }

    public void setFqbsbc(String fqbsbc) {
        this.fqbsbc = fqbsbc;
    }

    @Basic
    @Column(name = "MQBSBC", nullable = true, length = 255)
    public String getMqbsbc() {
        return mqbsbc;
    }

    public void setMqbsbc(String mqbsbc) {
        this.mqbsbc = mqbsbc;
    }

    @Basic
    @Column(name = "XDJMBSBC", nullable = true, length = 255)
    public String getXdjmbsbc() {
        return xdjmbsbc;
    }

    public void setXdjmbsbc(String xdjmbsbc) {
        this.xdjmbsbc = xdjmbsbc;
    }

    @Basic
    @Column(name = "ZNBSBC", nullable = true, length = 255)
    public String getZnbsbc() {
        return znbsbc;
    }

    public void setZnbsbc(String znbsbc) {
        this.znbsbc = znbsbc;
    }

    @Basic
    @Column(name = "BLSDETAIL", nullable = true, length = 255)
    public String getBlsdetail() {
        return blsdetail;
    }

    public void setBlsdetail(String blsdetail) {
        this.blsdetail = blsdetail;
    }

    @Basic
    @Column(name = "SSMZ", nullable = true, length = 32)
    public String getSsmz() {
        return ssmz;
    }

    public void setSsmz(String ssmz) {
        this.ssmz = ssmz;
    }

    @Basic
    @Column(name = "NAMECODE", nullable = true, length = 20)
    public String getNamecode() {
        return namecode;
    }

    public void setNamecode(String namecode) {
        this.namecode = namecode;
    }

    @Basic
    @Column(name = "ENTRYMODE", nullable = true, length = 32)
    public String getEntrymode() {
        return entrymode;
    }

    public void setEntrymode(String entrymode) {
        this.entrymode = entrymode;
    }

    @Basic
    @Column(name = "SPECIALCATEGORY", nullable = true, length = 3)
    public String getSpecialcategory() {
        return specialcategory;
    }

    public void setSpecialcategory(String specialcategory) {
        this.specialcategory = specialcategory;
    }

    @Basic
    @Column(name = "SHINENG", nullable = true, length = 3)
    public String getShineng() {
        return shineng;
    }

    public void setShineng(String shineng) {
        this.shineng = shineng;
    }

    @Basic
    @Column(name = "SHIZHI", nullable = true, length = 3)
    public String getShizhi() {
        return shizhi;
    }

    public void setShizhi(String shizhi) {
        this.shizhi = shizhi;
    }

    @Basic
    @Column(name = "CANJI", nullable = true, length = 3)
    public String getCanji() {
        return canji;
    }

    public void setCanji(String canji) {
        this.canji = canji;
    }

    @Basic
    @Column(name = "ZILI", nullable = true, length = 3)
    public String getZili() {
        return zili;
    }

    public void setZili(String zili) {
        this.zili = zili;
    }

    @Basic
    @Column(name = "SERVICEPURCHASE", nullable = true, length = 3)
    public String getServicepurchase() {
        return servicepurchase;
    }

    public void setServicepurchase(String servicepurchase) {
        this.servicepurchase = servicepurchase;
    }

    @Basic
    @Column(name = "LIVECONDITIONS", nullable = true, length = 255)
    public String getLiveconditions() {
        return liveconditions;
    }

    public void setLiveconditions(String liveconditions) {
        this.liveconditions = liveconditions;
    }

    @Basic
    @Column(name = "ECONOMICSOURCE", nullable = true, length = 255)
    public String getEconomicsource() {
        return economicsource;
    }

    public void setEconomicsource(String economicsource) {
        this.economicsource = economicsource;
    }

    @Basic
    @Column(name = "HOBBY", nullable = true, length = 255)
    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    @Basic
    @Column(name = "FAITH", nullable = true, length = 255)
    public String getFaith() {
        return faith;
    }

    public void setFaith(String faith) {
        this.faith = faith;
    }

    @Basic
    @Column(name = "OTHERS", nullable = true, length = 255)
    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    @Basic
    @Column(name = "PHONENUM2", nullable = true, length = 255)
    public String getPhonenum2() {
        return phonenum2;
    }

    public void setPhonenum2(String phonenum2) {
        this.phonenum2 = phonenum2;
    }

    @Basic
    @Column(name = "SHENG", nullable = true, length = 32)
    public String getSheng() {
        return sheng;
    }

    public void setSheng(String sheng) {
        this.sheng = sheng;
    }

    @Basic
    @Column(name = "SHI", nullable = true, length = 32)
    public String getShi() {
        return shi;
    }

    public void setShi(String shi) {
        this.shi = shi;
    }

    @Basic
    @Column(name = "QU", nullable = true, length = 32)
    public String getQu() {
        return qu;
    }

    public void setQu(String qu) {
        this.qu = qu;
    }

    @Basic
    @Column(name = "JIEDAO", nullable = true, length = 32)
    public String getJiedao() {
        return jiedao;
    }

    public void setJiedao(String jiedao) {
        this.jiedao = jiedao;
    }

    @Basic
    @Column(name = "SHEQU", nullable = true, length = 32)
    public String getShequ() {
        return shequ;
    }

    public void setShequ(String shequ) {
        this.shequ = shequ;
    }

    @Basic
    @Column(name = "SERVICE_OBJ", nullable = true, precision = 0)
    public Long getServiceObj() {
        return serviceObj;
    }

    public void setServiceObj(Long serviceObj) {
        this.serviceObj = serviceObj;
    }

    @Basic
    @Column(name = "SERVICE_CLASS", nullable = true, precision = 0)
    public Long getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(Long serviceClass) {
        this.serviceClass = serviceClass;
    }

    @Basic
    @Column(name = "CLOSE_NOTE", nullable = true, length = 300)
    public String getCloseNote() {
        return closeNote;
    }

    public void setCloseNote(String closeNote) {
        this.closeNote = closeNote;
    }

    @Basic
    @Column(name = "IS_CLOSED", nullable = true, precision = 0)
    public Boolean getClosed() {
        return isClosed;
    }

    public void setClosed(Boolean closed) {
        isClosed = closed;
    }

    @Basic
    @Column(name = "JHSY", nullable = true, length = 1)
    public String getJhsy() {
        return jhsy;
    }

    public void setJhsy(String jhsy) {
        this.jhsy = jhsy;
    }

    @Basic
    @Column(name = "POVERTY_ALLEVIATION", nullable = true, length = 1)
    public String getPovertyAlleviation() {
        return povertyAlleviation;
    }

    public void setPovertyAlleviation(String povertyAlleviation) {
        this.povertyAlleviation = povertyAlleviation;
    }

    @Basic
    @Column(name = "GOV_OLDER_ID", nullable = true, length = 32)
    public String getGovOlderId() {
        return govOlderId;
    }

    public void setGovOlderId(String govOlderId) {
        this.govOlderId = govOlderId;
    }

    @Basic
    @Column(name = "SPECIAL_FLAGS", nullable = true, length = 1)
    public String getSpecialFlags() {
        return specialFlags;
    }

    public void setSpecialFlags(String specialFlags) {
        this.specialFlags = specialFlags;
    }

    @Basic
    @Column(name = "IS_DEAD", nullable = true, length = 1)
    public String getIsDead() {
        return isDead;
    }

    public void setIsDead(String isDead) {
        this.isDead = isDead;
    }

    @Basic
    @Column(name = "PATIENTCODE", nullable = true, length = 50)
    public String getPatientcode() {
        return patientcode;
    }

    public void setPatientcode(String patientcode) {
        this.patientcode = patientcode;
    }

    @Basic
    @Column(name = "EMAIL", nullable = true, length = 500)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "SUB_AREA", nullable = true, length = 255)
    public String getSubArea() {
        return subArea;
    }

    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

    @Basic
    @Column(name = "DWELL_STATE", nullable = true, length = 2)
    public String getDwellState() {
        return dwellState;
    }

    public void setDwellState(String dwellState) {
        this.dwellState = dwellState;
    }

    @Basic
    @Column(name = "MEDICARE_NO", nullable = true, length = 255)
    public String getMedicareNo() {
        return medicareNo;
    }

    public void setMedicareNo(String medicareNo) {
        this.medicareNo = medicareNo;
    }

    @Basic
    @Column(name = "NEW_FARMING_NO", nullable = true, length = 255)
    public String getNewFarmingNo() {
        return newFarmingNo;
    }

    public void setNewFarmingNo(String newFarmingNo) {
        this.newFarmingNo = newFarmingNo;
    }

    @Basic
    @Column(name = "IS_HEALTH_EXAMINATION", nullable = true, length = 2)
    public String getIsHealthExamination() {
        return isHealthExamination;
    }

    public void setIsHealthExamination(String isHealthExamination) {
        this.isHealthExamination = isHealthExamination;
    }

    @Basic
    @Column(name = "IS_HIGH_BLOOD_PRESSURE", nullable = true, length = 2)
    public String getIsHighBloodPressure() {
        return isHighBloodPressure;
    }

    public void setIsHighBloodPressure(String isHighBloodPressure) {
        this.isHighBloodPressure = isHighBloodPressure;
    }

    @Basic
    @Column(name = "IS_DIABETES", nullable = true, length = 2)
    public String getIsDiabetes() {
        return isDiabetes;
    }

    public void setIsDiabetes(String isDiabetes) {
        this.isDiabetes = isDiabetes;
    }

    @Basic
    @Column(name = "IS_HEART_CORONARY", nullable = true, length = 2)
    public String getIsHeartCoronary() {
        return isHeartCoronary;
    }

    public void setIsHeartCoronary(String isHeartCoronary) {
        this.isHeartCoronary = isHeartCoronary;
    }

    @Basic
    @Column(name = "IS_MENTAL_DISEASE", nullable = true, length = 2)
    public String getIsMentalDisease() {
        return isMentalDisease;
    }

    public void setIsMentalDisease(String isMentalDisease) {
        this.isMentalDisease = isMentalDisease;
    }

    @Basic
    @Column(name = "HOME_DOC_NO", nullable = true, length = 255)
    public String getHomeDocNo() {
        return homeDocNo;
    }

    public void setHomeDocNo(String homeDocNo) {
        this.homeDocNo = homeDocNo;
    }

    @Basic
    @Column(name = "ANAMNESIS", nullable = true, length = 255)
    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    @Basic
    @Column(name = "IS_HOME_DOCTOR", nullable = true, length = 2)
    public String getIsHomeDoctor() {
        return isHomeDoctor;
    }

    public void setIsHomeDoctor(String isHomeDoctor) {
        this.isHomeDoctor = isHomeDoctor;
    }

    @Basic
    @Column(name = "IS_HIGH_BLOOD_PRESSURE_MANGE", nullable = true, length = 2)
    public String getIsHighBloodPressureMange() {
        return isHighBloodPressureMange;
    }

    public void setIsHighBloodPressureMange(String isHighBloodPressureMange) {
        this.isHighBloodPressureMange = isHighBloodPressureMange;
    }

    @Basic
    @Column(name = "IS_DIABETES_MANGE", nullable = true, length = 2)
    public String getIsDiabetesMange() {
        return isDiabetesMange;
    }

    public void setIsDiabetesMange(String isDiabetesMange) {
        this.isDiabetesMange = isDiabetesMange;
    }

    @Basic
    @Column(name = "HOUSEHOLD_RELATION", nullable = true, length = 20)
    public String getHouseholdRelation() {
        return householdRelation;
    }

    public void setHouseholdRelation(String householdRelation) {
        this.householdRelation = householdRelation;
    }

    @Basic
    @Column(name = "BUSINESS_ARCHIVES_NO", nullable = true, length = 255)
    public String getBusinessArchivesNo() {
        return businessArchivesNo;
    }

    public void setBusinessArchivesNo(String businessArchivesNo) {
        this.businessArchivesNo = businessArchivesNo;
    }

    @Basic
    @Column(name = "IS_APOPLEXY", nullable = true, length = 2)
    public String getIsApoplexy() {
        return isApoplexy;
    }

    public void setIsApoplexy(String isApoplexy) {
        this.isApoplexy = isApoplexy;
    }

    @Basic
    @Column(name = "IS_MOTHERHOOD", nullable = true, length = 2)
    public String getIsMotherhood() {
        return isMotherhood;
    }

    public void setIsMotherhood(String isMotherhood) {
        this.isMotherhood = isMotherhood;
    }

    @Basic
    @Column(name = "IS_BODY_DISABLE", nullable = true, length = 2)
    public String getIsBodyDisable() {
        return isBodyDisable;
    }

    public void setIsBodyDisable(String isBodyDisable) {
        this.isBodyDisable = isBodyDisable;
    }

    @Basic
    @Column(name = "IS_FROM_GW", nullable = true, length = 2)
    public String getIsFromGw() {
        return isFromGw;
    }

    public void setIsFromGw(String isFromGw) {
        this.isFromGw = isFromGw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LrInfomation that = (LrInfomation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creater, that.creater) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifyer, that.modifyer) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(delFlag, that.delFlag) &&
                Objects.equals(remarks, that.remarks) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(birth, that.birth) &&
                Objects.equals(age, that.age) &&
                Objects.equals(idnum, that.idnum) &&
                Objects.equals(phonenum, that.phonenum) &&
                Objects.equals(company, that.company) &&
                Objects.equals(nation, that.nation) &&
                Objects.equals(xuexing, that.xuexing) &&
                Objects.equals(contact, that.contact) &&
                Objects.equals(contactphone, that.contactphone) &&
                Objects.equals(education, that.education) &&
                Objects.equals(occupation, that.occupation) &&
                Objects.equals(marriage, that.marriage) &&
                Objects.equals(payment, that.payment) &&
                Objects.equals(ywgms, that.ywgms) &&
                Objects.equals(bls, that.bls) &&
                Objects.equals(ycbs, that.ycbs) &&
                Objects.equals(cjqk, that.cjqk) &&
                Objects.equals(resident, that.resident) &&
                Objects.equals(cfpqss, that.cfpqss) &&
                Objects.equals(rllx, that.rllx) &&
                Objects.equals(drink, that.drink) &&
                Objects.equals(toilet, that.toilet) &&
                Objects.equals(qcl, that.qcl) &&
                Objects.equals(fqbs, that.fqbs) &&
                Objects.equals(mqbs, that.mqbs) &&
                Objects.equals(xdjmbs, that.xdjmbs) &&
                Objects.equals(znbs, that.znbs) &&
                Objects.equals(rhlx, that.rhlx) &&
                Objects.equals(path, that.path) &&
                Objects.equals(rwm, that.rwm) &&
                Objects.equals(archivesno, that.archivesno) &&
                Objects.equals(address, that.address) &&
                Objects.equals(jigou, that.jigou) &&
                Objects.equals(relations, that.relations) &&
                Objects.equals(qtywgm, that.qtywgm) &&
                Objects.equals(fqbsbc, that.fqbsbc) &&
                Objects.equals(mqbsbc, that.mqbsbc) &&
                Objects.equals(xdjmbsbc, that.xdjmbsbc) &&
                Objects.equals(znbsbc, that.znbsbc) &&
                Objects.equals(blsdetail, that.blsdetail) &&
                Objects.equals(ssmz, that.ssmz) &&
                Objects.equals(namecode, that.namecode) &&
                Objects.equals(entrymode, that.entrymode) &&
                Objects.equals(specialcategory, that.specialcategory) &&
                Objects.equals(shineng, that.shineng) &&
                Objects.equals(shizhi, that.shizhi) &&
                Objects.equals(canji, that.canji) &&
                Objects.equals(zili, that.zili) &&
                Objects.equals(servicepurchase, that.servicepurchase) &&
                Objects.equals(liveconditions, that.liveconditions) &&
                Objects.equals(economicsource, that.economicsource) &&
                Objects.equals(hobby, that.hobby) &&
                Objects.equals(faith, that.faith) &&
                Objects.equals(others, that.others) &&
                Objects.equals(phonenum2, that.phonenum2) &&
                Objects.equals(sheng, that.sheng) &&
                Objects.equals(shi, that.shi) &&
                Objects.equals(qu, that.qu) &&
                Objects.equals(jiedao, that.jiedao) &&
                Objects.equals(shequ, that.shequ) &&
                Objects.equals(serviceObj, that.serviceObj) &&
                Objects.equals(serviceClass, that.serviceClass) &&
                Objects.equals(closeNote, that.closeNote) &&
                Objects.equals(isClosed, that.isClosed) &&
                Objects.equals(jhsy, that.jhsy) &&
                Objects.equals(povertyAlleviation, that.povertyAlleviation) &&
                Objects.equals(govOlderId, that.govOlderId) &&
                Objects.equals(specialFlags, that.specialFlags) &&
                Objects.equals(isDead, that.isDead) &&
                Objects.equals(patientcode, that.patientcode) &&
                Objects.equals(email, that.email) &&
                Objects.equals(subArea, that.subArea) &&
                Objects.equals(dwellState, that.dwellState) &&
                Objects.equals(medicareNo, that.medicareNo) &&
                Objects.equals(newFarmingNo, that.newFarmingNo) &&
                Objects.equals(isHealthExamination, that.isHealthExamination) &&
                Objects.equals(isHighBloodPressure, that.isHighBloodPressure) &&
                Objects.equals(isDiabetes, that.isDiabetes) &&
                Objects.equals(isHeartCoronary, that.isHeartCoronary) &&
                Objects.equals(isMentalDisease, that.isMentalDisease) &&
                Objects.equals(homeDocNo, that.homeDocNo) &&
                Objects.equals(anamnesis, that.anamnesis) &&
                Objects.equals(isHomeDoctor, that.isHomeDoctor) &&
                Objects.equals(isHighBloodPressureMange, that.isHighBloodPressureMange) &&
                Objects.equals(isDiabetesMange, that.isDiabetesMange) &&
                Objects.equals(householdRelation, that.householdRelation) &&
                Objects.equals(businessArchivesNo, that.businessArchivesNo) &&
                Objects.equals(isApoplexy, that.isApoplexy) &&
                Objects.equals(isMotherhood, that.isMotherhood) &&
                Objects.equals(isBodyDisable, that.isBodyDisable) &&
                Objects.equals(isFromGw, that.isFromGw);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creater, createdate, modifyer, modifydate, delFlag, remarks, name, sex, birth, age, idnum, phonenum, company, nation, xuexing, contact, contactphone, education, occupation, marriage, payment, ywgms, bls, ycbs, cjqk, resident, cfpqss, rllx, drink, toilet, qcl, fqbs, mqbs, xdjmbs, znbs, rhlx, path, rwm, archivesno, address, jigou, relations, qtywgm, fqbsbc, mqbsbc, xdjmbsbc, znbsbc, blsdetail, ssmz, namecode, entrymode, specialcategory, shineng, shizhi, canji, zili, servicepurchase, liveconditions, economicsource, hobby, faith, others, phonenum2, sheng, shi, qu, jiedao, shequ, serviceObj, serviceClass, closeNote, isClosed, jhsy, povertyAlleviation, govOlderId, specialFlags, isDead, patientcode, email, subArea, dwellState, medicareNo, newFarmingNo, isHealthExamination, isHighBloodPressure, isDiabetes, isHeartCoronary, isMentalDisease, homeDocNo, anamnesis, isHomeDoctor, isHighBloodPressureMange, isDiabetesMange, householdRelation, businessArchivesNo, isApoplexy, isMotherhood, isBodyDisable, isFromGw);
    }
}