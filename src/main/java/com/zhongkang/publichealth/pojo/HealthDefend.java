package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 08:30
 */
@Entity
@Table(name = "HEALTH_DEFEND")
public class HealthDefend {
    private String id;
    private String creater;
    private Date createdate;
    private String modifyer;
    private Date modifydate;
    private String name;
    private Date defenddate;
    private String hospital;
    private String healthid;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CREATER", nullable = true, length = 32)
    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYER", nullable = true, length = 32)
    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "NAME", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "DEFENDDATE", nullable = true)
    public Date getDefenddate() {
        return defenddate;
    }

    public void setDefenddate(Date defenddate) {
        this.defenddate = defenddate;
    }

    @Basic
    @Column(name = "HOSPITAL", nullable = true, length = 32)
    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    @Basic
    @Column(name = "HEALTHID", nullable = true, length = 32)
    public String getHealthid() {
        return healthid;
    }

    public void setHealthid(String healthid) {
        this.healthid = healthid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthDefend that = (HealthDefend) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creater, that.creater) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifyer, that.modifyer) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(name, that.name) &&
                Objects.equals(defenddate, that.defenddate) &&
                Objects.equals(hospital, that.hospital) &&
                Objects.equals(healthid, that.healthid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creater, createdate, modifyer, modifydate, name, defenddate, hospital, healthid);
    }
}