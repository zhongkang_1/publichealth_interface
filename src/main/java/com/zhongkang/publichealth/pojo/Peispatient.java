package com.zhongkang.publichealth.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-07 09:18
 */
@Entity
@Table(name = "PEISPATIENT")
public class Peispatient {
    private String idPatient;
    private Long idOrgpatient;
    private String idCis;
    private String idPatientarchive;
    private String patientcode;
    private String patientcodeprn;
    private String patientarchiveno;
    private String patientcardno;
    private String patientbizno;
    private String idcardno;
    private Date dailynumberdate;
    private Long dailynumber;
    private Long numtotal;
    private Long numyear;
    private Long nummonth;
    private Long numday;
    private Long numorg;
    private Long numorgresv;
    private String idPatientlinked;
    private Long idNonorg;
    private String patientname;
    private String inputCode;
    private String idOrgreservationgroup;
    private String idOrgreservation;
    private String idOrg;
    private String orgName;
    private String orgDepart;
    private String orgDepartsuba;
    private String orgDepartsubb;
    private String orgDepartsubc;
    private String orgDepartsubd;
    private String orgDepartsube;
    private String idFeetype;
    private String idPayway;
    private String payway;
    private Long offpercent;
    private Long maxoffpercent;
    private Long personpricelimit;
    private String idSex;
    private String sex;
    private Date birthdate;
    private Long age;
    private String idAgeunit;
    private String ageunit;
    private Long ageofreal;
    private String idMarriage;
    private String marriage;
    private String idNation;
    private String nation;
    private String address;
    private String idInformway;
    private String idOpendoctor;
    private String email;
    private String phone;
    private String idPatientclass;
    private String idEducation;
    private String education;
    private String idOccupation;
    private String occupation;
    private String idResarea;
    private String resarea;
    private Date dateinorganization;
    private Boolean fIsforprepare;
    private Boolean fIsforreserve;
    private Date datecreated;
    private Boolean fRegistered;
    private Date dateregister;
    private String positionCode;
    private String jobtypeCode;
    private Long moneyamount;
    private Long moneyamountpaid;
    private String guidancenote;
    private String workno;
    private String idDoctorreg;
    private String doctorreg;
    private String idExamtype;
    private String idExamsuite;
    private String examsuiteName;
    private String examsuiteAlias;
    private Long idDoctorconclusion;
    private String doctorconclusion;
    private String idDoctorapply;
    private String doctorapply;
    private Boolean fGuidanceprinted;
    private Boolean fFeecharged;
    private Boolean fExamstarted;
    private Boolean fReadytofinal;
    private String idDoctorfee;
    private String doctorfee;
    private Boolean fPaused;
    private Boolean fFinallocked;
    private Boolean fFinalexamed;
    private Boolean fFinalapproved;
    private String idDoctorfinal;
    private String doctorfinalNameR;
    private Date datefinalexamed;
    private String idDoctorfinalapproved;
    private Date datefinalapproved;
    private Boolean fCardissued;
    private Boolean fCardreturned;
    private Boolean fCoverprinted;
    private Boolean fReportprinted;
    private String idReportprintedby;
    private Date datereportprinted;
    private Boolean fReportinformed;
    private Date datereportinformed;
    private Boolean fReportfetched;
    private Date datereportfetched;
    private Boolean fIssevere;
    private Boolean fClosed;
    private Date dateclosed;
    private Boolean fNeedtraced;
    private Boolean fDiffperson;
    private Long confidentiallevel;
    private Boolean fSettleall;
    private String signature;
    private String note;
    private Date dtLastmodifiedthisat;
    private Boolean fInneroper;
    private Long severedegree;
    private String severedegreenote;
    private Boolean fSevereinformed;
    private Date severeinformtime;
    private String idSevereinformby;
    private String conclusion;
    private String conclusionsummary;
    private String suggestion;
    private String conclusionrich;
    private String dietguide;
    private String sportguide;
    private String knowledge;
    private String message;
    private String positivesummary;
    private String resultcompare;
    private String interfacemarks;
    private String patientflag;
    private Date timingstartedat;
    private Date timeresultlastchange;
    private Date timeresultlastarchive;
    private Date timeresultlastolap;
    private String hospitalcode;
    private String hospitalname;
    private Long idExamplace;
    private String parsedassignedsuiteandfi;
    private String parsedassignedgroupandfi;
    private String parsedsuiteandfi;
    private String parsedsuiteandfilab;
    private Long idGuidenurse;
    private String patientnameencoded;
    private String patientcodehiden;
    private Boolean fPdfcreated;
    private Boolean fWordcreated;
    private Boolean fWordprinted;
    private String guidancenote2;
    private Boolean fUsecodehiden;
    private Long idPatientclass2;
    private Long idPatientclass3;
    private Date dateregisternotime;
    private Long counterreportprinted;
    private Boolean fPrintcomparingreport;
    private Boolean fIsrecheck;
    private Boolean fSettlenone;
    private Boolean fGuidancereturned;
    private Date dateguidancereturned;
    private Long idGuidancereturnedby;
    private Boolean fOutpatient;
    private String patientnamereceipt;
    private String patientnamepinyin;
    private Boolean fForpreparefinancialconfirm;
    private String statusofhm;
    private String instancetag;
    private String keybirthplace;
    private String keybloodtype;
    private String exammethod;
    private String inpatientno;
    private String insuranceno;
    private String keypayway;
    private String healthcard;
    private String exampoint;
    private String fingerprint;
    private Long countreportcoverprinted;
    private Long countreportprinted;
    private Long countreportpdf;
    private Long countreportword;
    private Long countreportxml;
    private Long countreportcompare;
    private Long countreportcomparepdf;
    private Long countreportcompareword;
    private Long countreportcomparexml;
    private Long countreportoccupation;
    private Long countreportoccupationpdf;
    private Long countreportoccupationword;
    private Long countreportoccupationxml;
    private Boolean scbs;
    private String idTjtc;
    private String jzdw;
    private String jzdwr;
    private String spr;
    private String tjr;
    private String lqfs;
    private Long yzbm;
    private String yjaddress;
    private String qtxz;
    private String isHmdb;
    private Boolean isHmd;
    private Boolean isjj;
    private Long zgl;
    private Long jhgl;
    private String jhys;
    private Long jktjzt;
    private Long zytjzt;
    private Long tmyd;
    private String id;
    private Date medicaldate;
    private String trades;
    private Boolean cjjgsfyhf;
    private Boolean bhgybsfyhf;
    private Boolean yxjgsfyhf;
    private String medicaltype;
    private Long prepayment;
    private Long tcprice;
    private Date createdate;
    private Date modifydate;
    private Boolean cultural;
    private String everOfDisease;
    private String ccnl;
    private String jq;
    private String zq;
    private String tjnl;
    private String familyNumber;
    private String zc;
    private String sc;
    private String lc;
    private String jt;
    private String ywrc;
    private String abstainSmokeNote;
    private String everydaySmokeN;
    private String smokeYear;
    private String noKissTheCup;
    private String betweenKissTheCup;
    private String evermoreKiss;
    private String abstainLostKiss;
    private String kissYearN;
    private String kissAmount;
    private String kissType;
    private String familyOfDisease;
    private String symptom;
    private Boolean isAudit;
    private String everOfDiseaseRemark;
    private Byte createReportNum;
    private Date workDate;
    private Date harmDate;
    private String picture;
    private String advice;
    private Long diseasePrintNum;
    private Long healthPrintNum;
    private Date readytofinalDate;
    private Long guideSignleCount;
    private Long shortCode;
    private Boolean isNoticed;
    private String reviewPdf;
    private String contraindicatedPdf;
    private String diseasePdf;
    private String signPicture;
    private Long isNewPacs;
    private Long tsLimit;
    private String physique;
    private String docName;
    private String committee;
    private String street;

    @Basic
    @Column(name = "ID_PATIENT", nullable = true, length = 32)
    public String getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(String idPatient) {
        this.idPatient = idPatient;
    }

    @Basic
    @Column(name = "ID_ORGPATIENT", nullable = true, precision = 0)
    public Long getIdOrgpatient() {
        return idOrgpatient;
    }

    public void setIdOrgpatient(Long idOrgpatient) {
        this.idOrgpatient = idOrgpatient;
    }

    @Basic
    @Column(name = "ID_CIS", nullable = true, length = 50)
    public String getIdCis() {
        return idCis;
    }

    public void setIdCis(String idCis) {
        this.idCis = idCis;
    }

    @Basic
    @Column(name = "ID_PATIENTARCHIVE", nullable = true, length = 32)
    public String getIdPatientarchive() {
        return idPatientarchive;
    }

    public void setIdPatientarchive(String idPatientarchive) {
        this.idPatientarchive = idPatientarchive;
    }

    @Basic
    @Column(name = "PATIENTCODE", nullable = true, length = 50)
    public String getPatientcode() {
        return patientcode;
    }

    public void setPatientcode(String patientcode) {
        this.patientcode = patientcode;
    }

    @Basic
    @Column(name = "PATIENTCODEPRN", nullable = true, length = 50)
    public String getPatientcodeprn() {
        return patientcodeprn;
    }

    public void setPatientcodeprn(String patientcodeprn) {
        this.patientcodeprn = patientcodeprn;
    }

    @Basic
    @Column(name = "PATIENTARCHIVENO", nullable = true, length = 30)
    public String getPatientarchiveno() {
        return patientarchiveno;
    }

    public void setPatientarchiveno(String patientarchiveno) {
        this.patientarchiveno = patientarchiveno;
    }

    @Basic
    @Column(name = "PATIENTCARDNO", nullable = true, length = 30)
    public String getPatientcardno() {
        return patientcardno;
    }

    public void setPatientcardno(String patientcardno) {
        this.patientcardno = patientcardno;
    }

    @Basic
    @Column(name = "PATIENTBIZNO", nullable = true, length = 50)
    public String getPatientbizno() {
        return patientbizno;
    }

    public void setPatientbizno(String patientbizno) {
        this.patientbizno = patientbizno;
    }

    @Basic
    @Column(name = "IDCARDNO", nullable = true, length = 30)
    public String getIdcardno() {
        return idcardno;
    }

    public void setIdcardno(String idcardno) {
        this.idcardno = idcardno;
    }

    @Basic
    @Column(name = "DAILYNUMBERDATE", nullable = true)
    public Date getDailynumberdate() {
        return dailynumberdate;
    }

    public void setDailynumberdate(Date dailynumberdate) {
        this.dailynumberdate = dailynumberdate;
    }

    @Basic
    @Column(name = "DAILYNUMBER", nullable = true, precision = 0)
    public Long getDailynumber() {
        return dailynumber;
    }

    public void setDailynumber(Long dailynumber) {
        this.dailynumber = dailynumber;
    }

    @Basic
    @Column(name = "NUMTOTAL", nullable = true, precision = 0)
    public Long getNumtotal() {
        return numtotal;
    }

    public void setNumtotal(Long numtotal) {
        this.numtotal = numtotal;
    }

    @Basic
    @Column(name = "NUMYEAR", nullable = true, precision = 0)
    public Long getNumyear() {
        return numyear;
    }

    public void setNumyear(Long numyear) {
        this.numyear = numyear;
    }

    @Basic
    @Column(name = "NUMMONTH", nullable = true, precision = 0)
    public Long getNummonth() {
        return nummonth;
    }

    public void setNummonth(Long nummonth) {
        this.nummonth = nummonth;
    }

    @Basic
    @Column(name = "NUMDAY", nullable = true, precision = 0)
    public Long getNumday() {
        return numday;
    }

    public void setNumday(Long numday) {
        this.numday = numday;
    }

    @Basic
    @Column(name = "NUMORG", nullable = true, precision = 0)
    public Long getNumorg() {
        return numorg;
    }

    public void setNumorg(Long numorg) {
        this.numorg = numorg;
    }

    @Basic
    @Column(name = "NUMORGRESV", nullable = true, precision = 0)
    public Long getNumorgresv() {
        return numorgresv;
    }

    public void setNumorgresv(Long numorgresv) {
        this.numorgresv = numorgresv;
    }

    @Basic
    @Column(name = "ID_PATIENTLINKED", nullable = true, length = 32)
    public String getIdPatientlinked() {
        return idPatientlinked;
    }

    public void setIdPatientlinked(String idPatientlinked) {
        this.idPatientlinked = idPatientlinked;
    }

    @Basic
    @Column(name = "ID_NONORG", nullable = true, precision = 0)
    public Long getIdNonorg() {
        return idNonorg;
    }

    public void setIdNonorg(Long idNonorg) {
        this.idNonorg = idNonorg;
    }

    @Basic
    @Column(name = "PATIENTNAME", nullable = true, length = 50)
    public String getPatientname() {
        return patientname;
    }

    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    @Basic
    @Column(name = "INPUT_CODE", nullable = true, length = 25)
    public String getInputCode() {
        return inputCode;
    }

    public void setInputCode(String inputCode) {
        this.inputCode = inputCode;
    }

    @Basic
    @Column(name = "ID_ORGRESERVATIONGROUP", nullable = true, length = 32)
    public String getIdOrgreservationgroup() {
        return idOrgreservationgroup;
    }

    public void setIdOrgreservationgroup(String idOrgreservationgroup) {
        this.idOrgreservationgroup = idOrgreservationgroup;
    }

    @Basic
    @Column(name = "ID_ORGRESERVATION", nullable = true, length = 32)
    public String getIdOrgreservation() {
        return idOrgreservation;
    }

    public void setIdOrgreservation(String idOrgreservation) {
        this.idOrgreservation = idOrgreservation;
    }

    @Basic
    @Column(name = "ID_ORG", nullable = true, length = 32)
    public String getIdOrg() {
        return idOrg;
    }

    public void setIdOrg(String idOrg) {
        this.idOrg = idOrg;
    }

    @Basic
    @Column(name = "ORG_NAME", nullable = true, length = 50)
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Basic
    @Column(name = "ORG_DEPART", nullable = true, length = 100)
    public String getOrgDepart() {
        return orgDepart;
    }

    public void setOrgDepart(String orgDepart) {
        this.orgDepart = orgDepart;
    }

    @Basic
    @Column(name = "ORG_DEPARTSUBA", nullable = true, length = 32)
    public String getOrgDepartsuba() {
        return orgDepartsuba;
    }

    public void setOrgDepartsuba(String orgDepartsuba) {
        this.orgDepartsuba = orgDepartsuba;
    }

    @Basic
    @Column(name = "ORG_DEPARTSUBB", nullable = true, length = 200)
    public String getOrgDepartsubb() {
        return orgDepartsubb;
    }

    public void setOrgDepartsubb(String orgDepartsubb) {
        this.orgDepartsubb = orgDepartsubb;
    }

    @Basic
    @Column(name = "ORG_DEPARTSUBC", nullable = true, length = 30)
    public String getOrgDepartsubc() {
        return orgDepartsubc;
    }

    public void setOrgDepartsubc(String orgDepartsubc) {
        this.orgDepartsubc = orgDepartsubc;
    }

    @Basic
    @Column(name = "ORG_DEPARTSUBD", nullable = true, length = 30)
    public String getOrgDepartsubd() {
        return orgDepartsubd;
    }

    public void setOrgDepartsubd(String orgDepartsubd) {
        this.orgDepartsubd = orgDepartsubd;
    }

    @Basic
    @Column(name = "ORG_DEPARTSUBE", nullable = true, length = 30)
    public String getOrgDepartsube() {
        return orgDepartsube;
    }

    public void setOrgDepartsube(String orgDepartsube) {
        this.orgDepartsube = orgDepartsube;
    }

    @Basic
    @Column(name = "ID_FEETYPE", nullable = true, length = 32)
    public String getIdFeetype() {
        return idFeetype;
    }

    public void setIdFeetype(String idFeetype) {
        this.idFeetype = idFeetype;
    }

    @Basic
    @Column(name = "ID_PAYWAY", nullable = true, length = 2000)
    public String getIdPayway() {
        return idPayway;
    }

    public void setIdPayway(String idPayway) {
        this.idPayway = idPayway;
    }

    @Basic
    @Column(name = "PAYWAY", nullable = true, length = 4000)
    public String getPayway() {
        return payway;
    }

    public void setPayway(String payway) {
        this.payway = payway;
    }

    @Basic
    @Column(name = "OFFPERCENT", nullable = true, precision = 0)
    public Long getOffpercent() {
        return offpercent;
    }

    public void setOffpercent(Long offpercent) {
        this.offpercent = offpercent;
    }

    @Basic
    @Column(name = "MAXOFFPERCENT", nullable = true, precision = 0)
    public Long getMaxoffpercent() {
        return maxoffpercent;
    }

    public void setMaxoffpercent(Long maxoffpercent) {
        this.maxoffpercent = maxoffpercent;
    }

    @Basic
    @Column(name = "PERSONPRICELIMIT", nullable = true, precision = 2)
    public Long getPersonpricelimit() {
        return personpricelimit;
    }

    public void setPersonpricelimit(Long personpricelimit) {
        this.personpricelimit = personpricelimit;
    }

    @Basic
    @Column(name = "ID_SEX", nullable = true, length = 32)
    public String getIdSex() {
        return idSex;
    }

    public void setIdSex(String idSex) {
        this.idSex = idSex;
    }

    @Basic
    @Column(name = "SEX", nullable = true, length = 8)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "BIRTHDATE", nullable = true)
    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Basic
    @Column(name = "AGE", nullable = true, precision = 1)
    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    @Basic
    @Column(name = "ID_AGEUNIT", nullable = true, length = 32)
    public String getIdAgeunit() {
        return idAgeunit;
    }

    public void setIdAgeunit(String idAgeunit) {
        this.idAgeunit = idAgeunit;
    }

    @Basic
    @Column(name = "AGEUNIT", nullable = true, length = 8)
    public String getAgeunit() {
        return ageunit;
    }

    public void setAgeunit(String ageunit) {
        this.ageunit = ageunit;
    }

    @Basic
    @Column(name = "AGEOFREAL", nullable = true, precision = 5)
    public Long getAgeofreal() {
        return ageofreal;
    }

    public void setAgeofreal(Long ageofreal) {
        this.ageofreal = ageofreal;
    }

    @Basic
    @Column(name = "ID_MARRIAGE", nullable = true, length = 32)
    public String getIdMarriage() {
        return idMarriage;
    }

    public void setIdMarriage(String idMarriage) {
        this.idMarriage = idMarriage;
    }

    @Basic
    @Column(name = "MARRIAGE", nullable = true, length = 4)
    public String getMarriage() {
        return marriage;
    }

    public void setMarriage(String marriage) {
        this.marriage = marriage;
    }

    @Basic
    @Column(name = "ID_NATION", nullable = true, length = 32)
    public String getIdNation() {
        return idNation;
    }

    public void setIdNation(String idNation) {
        this.idNation = idNation;
    }

    @Basic
    @Column(name = "NATION", nullable = true, length = 16)
    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    @Basic
    @Column(name = "ADDRESS", nullable = true, length = 120)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "ID_INFORMWAY", nullable = true, length = 32)
    public String getIdInformway() {
        return idInformway;
    }

    public void setIdInformway(String idInformway) {
        this.idInformway = idInformway;
    }

    @Basic
    @Column(name = "ID_OPENDOCTOR", nullable = true, length = 32)
    public String getIdOpendoctor() {
        return idOpendoctor;
    }

    public void setIdOpendoctor(String idOpendoctor) {
        this.idOpendoctor = idOpendoctor;
    }

    @Basic
    @Column(name = "EMAIL", nullable = true, length = 80)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "PHONE", nullable = true, length = 30)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "ID_PATIENTCLASS", nullable = true, length = 32)
    public String getIdPatientclass() {
        return idPatientclass;
    }

    public void setIdPatientclass(String idPatientclass) {
        this.idPatientclass = idPatientclass;
    }

    @Basic
    @Column(name = "ID_EDUCATION", nullable = true, length = 32)
    public String getIdEducation() {
        return idEducation;
    }

    public void setIdEducation(String idEducation) {
        this.idEducation = idEducation;
    }

    @Basic
    @Column(name = "EDUCATION", nullable = true, length = 20)
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    @Basic
    @Column(name = "ID_OCCUPATION", nullable = true, length = 32)
    public String getIdOccupation() {
        return idOccupation;
    }

    public void setIdOccupation(String idOccupation) {
        this.idOccupation = idOccupation;
    }

    @Basic
    @Column(name = "OCCUPATION", nullable = true, length = 20)
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Basic
    @Column(name = "ID_RESAREA", nullable = true, length = 32)
    public String getIdResarea() {
        return idResarea;
    }

    public void setIdResarea(String idResarea) {
        this.idResarea = idResarea;
    }

    @Basic
    @Column(name = "RESAREA", nullable = true, length = 40)
    public String getResarea() {
        return resarea;
    }

    public void setResarea(String resarea) {
        this.resarea = resarea;
    }

    @Basic
    @Column(name = "DATEINORGANIZATION", nullable = true)
    public Date getDateinorganization() {
        return dateinorganization;
    }

    public void setDateinorganization(Date dateinorganization) {
        this.dateinorganization = dateinorganization;
    }

    @Basic
    @Column(name = "F_ISFORPREPARE", nullable = true, precision = 0)
    public Boolean getfIsforprepare() {
        return fIsforprepare;
    }

    public void setfIsforprepare(Boolean fIsforprepare) {
        this.fIsforprepare = fIsforprepare;
    }

    @Basic
    @Column(name = "F_ISFORRESERVE", nullable = true, precision = 0)
    public Boolean getfIsforreserve() {
        return fIsforreserve;
    }

    public void setfIsforreserve(Boolean fIsforreserve) {
        this.fIsforreserve = fIsforreserve;
    }

    @Basic
    @Column(name = "DATECREATED", nullable = true)
    public Date getDatecreated() {
        return datecreated;
    }

    public void setDatecreated(Date datecreated) {
        this.datecreated = datecreated;
    }

    @Basic
    @Column(name = "F_REGISTERED", nullable = true, precision = 0)
    public Boolean getfRegistered() {
        return fRegistered;
    }

    public void setfRegistered(Boolean fRegistered) {
        this.fRegistered = fRegistered;
    }

    @Basic
    @Column(name = "DATEREGISTER", nullable = true)
    public Date getDateregister() {
        return dateregister;
    }

    public void setDateregister(Date dateregister) {
        this.dateregister = dateregister;
    }

    @Basic
    @Column(name = "POSITION_CODE", nullable = true, length = 6)
    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    @Basic
    @Column(name = "JOBTYPE_CODE", nullable = true, length = 6)
    public String getJobtypeCode() {
        return jobtypeCode;
    }

    public void setJobtypeCode(String jobtypeCode) {
        this.jobtypeCode = jobtypeCode;
    }

    @Basic
    @Column(name = "MONEYAMOUNT", nullable = true, precision = 2)
    public Long getMoneyamount() {
        return moneyamount;
    }

    public void setMoneyamount(Long moneyamount) {
        this.moneyamount = moneyamount;
    }

    @Basic
    @Column(name = "MONEYAMOUNTPAID", nullable = true, precision = 2)
    public Long getMoneyamountpaid() {
        return moneyamountpaid;
    }

    public void setMoneyamountpaid(Long moneyamountpaid) {
        this.moneyamountpaid = moneyamountpaid;
    }

    @Basic
    @Column(name = "GUIDANCENOTE", nullable = true, length = 200)
    public String getGuidancenote() {
        return guidancenote;
    }

    public void setGuidancenote(String guidancenote) {
        this.guidancenote = guidancenote;
    }

    @Basic
    @Column(name = "WORKNO", nullable = true, length = 20)
    public String getWorkno() {
        return workno;
    }

    public void setWorkno(String workno) {
        this.workno = workno;
    }

    @Basic
    @Column(name = "ID_DOCTORREG", nullable = true, length = 32)
    public String getIdDoctorreg() {
        return idDoctorreg;
    }

    public void setIdDoctorreg(String idDoctorreg) {
        this.idDoctorreg = idDoctorreg;
    }

    @Basic
    @Column(name = "DOCTORREG", nullable = true, length = 16)
    public String getDoctorreg() {
        return doctorreg;
    }

    public void setDoctorreg(String doctorreg) {
        this.doctorreg = doctorreg;
    }

    @Basic
    @Column(name = "ID_EXAMTYPE", nullable = true, length = 32)
    public String getIdExamtype() {
        return idExamtype;
    }

    public void setIdExamtype(String idExamtype) {
        this.idExamtype = idExamtype;
    }

    @Basic
    @Column(name = "ID_EXAMSUITE", nullable = true, length = 20)
    public String getIdExamsuite() {
        return idExamsuite;
    }

    public void setIdExamsuite(String idExamsuite) {
        this.idExamsuite = idExamsuite;
    }

    @Basic
    @Column(name = "EXAMSUITE_NAME", nullable = true, length = 200)
    public String getExamsuiteName() {
        return examsuiteName;
    }

    public void setExamsuiteName(String examsuiteName) {
        this.examsuiteName = examsuiteName;
    }

    @Basic
    @Column(name = "EXAMSUITE_ALIAS", nullable = true, length = 100)
    public String getExamsuiteAlias() {
        return examsuiteAlias;
    }

    public void setExamsuiteAlias(String examsuiteAlias) {
        this.examsuiteAlias = examsuiteAlias;
    }

    @Basic
    @Column(name = "ID_DOCTORCONCLUSION", nullable = true, precision = 0)
    public Long getIdDoctorconclusion() {
        return idDoctorconclusion;
    }

    public void setIdDoctorconclusion(Long idDoctorconclusion) {
        this.idDoctorconclusion = idDoctorconclusion;
    }

    @Basic
    @Column(name = "DOCTORCONCLUSION", nullable = true, length = 16)
    public String getDoctorconclusion() {
        return doctorconclusion;
    }

    public void setDoctorconclusion(String doctorconclusion) {
        this.doctorconclusion = doctorconclusion;
    }

    @Basic
    @Column(name = "ID_DOCTORAPPLY", nullable = true, length = 32)
    public String getIdDoctorapply() {
        return idDoctorapply;
    }

    public void setIdDoctorapply(String idDoctorapply) {
        this.idDoctorapply = idDoctorapply;
    }

    @Basic
    @Column(name = "DOCTORAPPLY", nullable = true, length = 100)
    public String getDoctorapply() {
        return doctorapply;
    }

    public void setDoctorapply(String doctorapply) {
        this.doctorapply = doctorapply;
    }

    @Basic
    @Column(name = "F_GUIDANCEPRINTED", nullable = true, precision = 0)
    public Boolean getfGuidanceprinted() {
        return fGuidanceprinted;
    }

    public void setfGuidanceprinted(Boolean fGuidanceprinted) {
        this.fGuidanceprinted = fGuidanceprinted;
    }

    @Basic
    @Column(name = "F_FEECHARGED", nullable = true, precision = 0)
    public Boolean getfFeecharged() {
        return fFeecharged;
    }

    public void setfFeecharged(Boolean fFeecharged) {
        this.fFeecharged = fFeecharged;
    }

    @Basic
    @Column(name = "F_EXAMSTARTED", nullable = true, precision = 0)
    public Boolean getfExamstarted() {
        return fExamstarted;
    }

    public void setfExamstarted(Boolean fExamstarted) {
        this.fExamstarted = fExamstarted;
    }

    @Basic
    @Column(name = "F_READYTOFINAL", nullable = true, precision = 0)
    public Boolean getfReadytofinal() {
        return fReadytofinal;
    }

    public void setfReadytofinal(Boolean fReadytofinal) {
        this.fReadytofinal = fReadytofinal;
    }

    @Basic
    @Column(name = "ID_DOCTORFEE", nullable = true, length = 32)
    public String getIdDoctorfee() {
        return idDoctorfee;
    }

    public void setIdDoctorfee(String idDoctorfee) {
        this.idDoctorfee = idDoctorfee;
    }

    @Basic
    @Column(name = "DOCTORFEE", nullable = true, length = 100)
    public String getDoctorfee() {
        return doctorfee;
    }

    public void setDoctorfee(String doctorfee) {
        this.doctorfee = doctorfee;
    }

    @Basic
    @Column(name = "F_PAUSED", nullable = true, precision = 0)
    public Boolean getfPaused() {
        return fPaused;
    }

    public void setfPaused(Boolean fPaused) {
        this.fPaused = fPaused;
    }

    @Basic
    @Column(name = "F_FINALLOCKED", nullable = true, precision = 0)
    public Boolean getfFinallocked() {
        return fFinallocked;
    }

    public void setfFinallocked(Boolean fFinallocked) {
        this.fFinallocked = fFinallocked;
    }

    @Basic
    @Column(name = "F_FINALEXAMED", nullable = true, precision = 0)
    public Boolean getfFinalexamed() {
        return fFinalexamed;
    }

    public void setfFinalexamed(Boolean fFinalexamed) {
        this.fFinalexamed = fFinalexamed;
    }

    @Basic
    @Column(name = "F_FINALAPPROVED", nullable = true, precision = 0)
    public Boolean getfFinalapproved() {
        return fFinalapproved;
    }

    public void setfFinalapproved(Boolean fFinalapproved) {
        this.fFinalapproved = fFinalapproved;
    }

    @Basic
    @Column(name = "ID_DOCTORFINAL", nullable = true, length = 32)
    public String getIdDoctorfinal() {
        return idDoctorfinal;
    }

    public void setIdDoctorfinal(String idDoctorfinal) {
        this.idDoctorfinal = idDoctorfinal;
    }

    @Basic
    @Column(name = "DOCTORFINAL_NAME_R", nullable = true, length = 12)
    public String getDoctorfinalNameR() {
        return doctorfinalNameR;
    }

    public void setDoctorfinalNameR(String doctorfinalNameR) {
        this.doctorfinalNameR = doctorfinalNameR;
    }

    @Basic
    @Column(name = "DATEFINALEXAMED", nullable = true)
    public Date getDatefinalexamed() {
        return datefinalexamed;
    }

    public void setDatefinalexamed(Date datefinalexamed) {
        this.datefinalexamed = datefinalexamed;
    }

    @Basic
    @Column(name = "ID_DOCTORFINALAPPROVED", nullable = true, length = 32)
    public String getIdDoctorfinalapproved() {
        return idDoctorfinalapproved;
    }

    public void setIdDoctorfinalapproved(String idDoctorfinalapproved) {
        this.idDoctorfinalapproved = idDoctorfinalapproved;
    }

    @Basic
    @Column(name = "DATEFINALAPPROVED", nullable = true)
    public Date getDatefinalapproved() {
        return datefinalapproved;
    }

    public void setDatefinalapproved(Date datefinalapproved) {
        this.datefinalapproved = datefinalapproved;
    }

    @Basic
    @Column(name = "F_CARDISSUED", nullable = true, precision = 0)
    public Boolean getfCardissued() {
        return fCardissued;
    }

    public void setfCardissued(Boolean fCardissued) {
        this.fCardissued = fCardissued;
    }

    @Basic
    @Column(name = "F_CARDRETURNED", nullable = true, precision = 0)
    public Boolean getfCardreturned() {
        return fCardreturned;
    }

    public void setfCardreturned(Boolean fCardreturned) {
        this.fCardreturned = fCardreturned;
    }

    @Basic
    @Column(name = "F_COVERPRINTED", nullable = true, precision = 0)
    public Boolean getfCoverprinted() {
        return fCoverprinted;
    }

    public void setfCoverprinted(Boolean fCoverprinted) {
        this.fCoverprinted = fCoverprinted;
    }

    @Basic
    @Column(name = "F_REPORTPRINTED", nullable = true, precision = 0)
    public Boolean getfReportprinted() {
        return fReportprinted;
    }

    public void setfReportprinted(Boolean fReportprinted) {
        this.fReportprinted = fReportprinted;
    }

    @Basic
    @Column(name = "ID_REPORTPRINTEDBY", nullable = true, length = 32)
    public String getIdReportprintedby() {
        return idReportprintedby;
    }

    public void setIdReportprintedby(String idReportprintedby) {
        this.idReportprintedby = idReportprintedby;
    }

    @Basic
    @Column(name = "DATEREPORTPRINTED", nullable = true)
    public Date getDatereportprinted() {
        return datereportprinted;
    }

    public void setDatereportprinted(Date datereportprinted) {
        this.datereportprinted = datereportprinted;
    }

    @Basic
    @Column(name = "F_REPORTINFORMED", nullable = true, precision = 0)
    public Boolean getfReportinformed() {
        return fReportinformed;
    }

    public void setfReportinformed(Boolean fReportinformed) {
        this.fReportinformed = fReportinformed;
    }

    @Basic
    @Column(name = "DATEREPORTINFORMED", nullable = true)
    public Date getDatereportinformed() {
        return datereportinformed;
    }

    public void setDatereportinformed(Date datereportinformed) {
        this.datereportinformed = datereportinformed;
    }

    @Basic
    @Column(name = "F_REPORTFETCHED", nullable = true, precision = 0)
    public Boolean getfReportfetched() {
        return fReportfetched;
    }

    public void setfReportfetched(Boolean fReportfetched) {
        this.fReportfetched = fReportfetched;
    }

    @Basic
    @Column(name = "DATEREPORTFETCHED", nullable = true)
    public Date getDatereportfetched() {
        return datereportfetched;
    }

    public void setDatereportfetched(Date datereportfetched) {
        this.datereportfetched = datereportfetched;
    }

    @Basic
    @Column(name = "F_ISSEVERE", nullable = true, precision = 0)
    public Boolean getfIssevere() {
        return fIssevere;
    }

    public void setfIssevere(Boolean fIssevere) {
        this.fIssevere = fIssevere;
    }

    @Basic
    @Column(name = "F_CLOSED", nullable = true, precision = 0)
    public Boolean getfClosed() {
        return fClosed;
    }

    public void setfClosed(Boolean fClosed) {
        this.fClosed = fClosed;
    }

    @Basic
    @Column(name = "DATECLOSED", nullable = true)
    public Date getDateclosed() {
        return dateclosed;
    }

    public void setDateclosed(Date dateclosed) {
        this.dateclosed = dateclosed;
    }

    @Basic
    @Column(name = "F_NEEDTRACED", nullable = true, precision = 0)
    public Boolean getfNeedtraced() {
        return fNeedtraced;
    }

    public void setfNeedtraced(Boolean fNeedtraced) {
        this.fNeedtraced = fNeedtraced;
    }

    @Basic
    @Column(name = "F_DIFFPERSON", nullable = true, precision = 0)
    public Boolean getfDiffperson() {
        return fDiffperson;
    }

    public void setfDiffperson(Boolean fDiffperson) {
        this.fDiffperson = fDiffperson;
    }

    @Basic
    @Column(name = "CONFIDENTIALLEVEL", nullable = true, precision = 0)
    public Long getConfidentiallevel() {
        return confidentiallevel;
    }

    public void setConfidentiallevel(Long confidentiallevel) {
        this.confidentiallevel = confidentiallevel;
    }

    @Basic
    @Column(name = "F_SETTLEALL", nullable = true, precision = 0)
    public Boolean getfSettleall() {
        return fSettleall;
    }

    public void setfSettleall(Boolean fSettleall) {
        this.fSettleall = fSettleall;
    }

    @Basic
    @Column(name = "SIGNATURE", nullable = true, length = 200)
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Basic
    @Column(name = "NOTE", nullable = true, length = 1000)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Basic
    @Column(name = "DT_LASTMODIFIEDTHISAT", nullable = true)
    public Date getDtLastmodifiedthisat() {
        return dtLastmodifiedthisat;
    }

    public void setDtLastmodifiedthisat(Date dtLastmodifiedthisat) {
        this.dtLastmodifiedthisat = dtLastmodifiedthisat;
    }

    @Basic
    @Column(name = "F_INNEROPER", nullable = true, precision = 0)
    public Boolean getfInneroper() {
        return fInneroper;
    }

    public void setfInneroper(Boolean fInneroper) {
        this.fInneroper = fInneroper;
    }

    @Basic
    @Column(name = "SEVEREDEGREE", nullable = true, precision = 0)
    public Long getSeveredegree() {
        return severedegree;
    }

    public void setSeveredegree(Long severedegree) {
        this.severedegree = severedegree;
    }

    @Basic
    @Column(name = "SEVEREDEGREENOTE", nullable = true, length = 200)
    public String getSeveredegreenote() {
        return severedegreenote;
    }

    public void setSeveredegreenote(String severedegreenote) {
        this.severedegreenote = severedegreenote;
    }

    @Basic
    @Column(name = "F_SEVEREINFORMED", nullable = true, precision = 0)
    public Boolean getfSevereinformed() {
        return fSevereinformed;
    }

    public void setfSevereinformed(Boolean fSevereinformed) {
        this.fSevereinformed = fSevereinformed;
    }

    @Basic
    @Column(name = "SEVEREINFORMTIME", nullable = true)
    public Date getSevereinformtime() {
        return severeinformtime;
    }

    public void setSevereinformtime(Date severeinformtime) {
        this.severeinformtime = severeinformtime;
    }

    @Basic
    @Column(name = "ID_SEVEREINFORMBY", nullable = true, length = 32)
    public String getIdSevereinformby() {
        return idSevereinformby;
    }

    public void setIdSevereinformby(String idSevereinformby) {
        this.idSevereinformby = idSevereinformby;
    }

    @Basic
    @Column(name = "CONCLUSION", nullable = true)
    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    @Basic
    @Column(name = "CONCLUSIONSUMMARY", nullable = true)
    public String getConclusionsummary() {
        return conclusionsummary;
    }

    public void setConclusionsummary(String conclusionsummary) {
        this.conclusionsummary = conclusionsummary;
    }

    @Basic
    @Column(name = "SUGGESTION", nullable = true)
    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    @Basic
    @Column(name = "CONCLUSIONRICH", nullable = true, length = 200)
    public String getConclusionrich() {
        return conclusionrich;
    }

    public void setConclusionrich(String conclusionrich) {
        this.conclusionrich = conclusionrich;
    }

    @Basic
    @Column(name = "DIETGUIDE", nullable = true, length = 200)
    public String getDietguide() {
        return dietguide;
    }

    public void setDietguide(String dietguide) {
        this.dietguide = dietguide;
    }

    @Basic
    @Column(name = "SPORTGUIDE", nullable = true, length = 200)
    public String getSportguide() {
        return sportguide;
    }

    public void setSportguide(String sportguide) {
        this.sportguide = sportguide;
    }

    @Basic
    @Column(name = "KNOWLEDGE", nullable = true, length = 1000)
    public String getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(String knowledge) {
        this.knowledge = knowledge;
    }

    @Basic
    @Column(name = "MESSAGE", nullable = true, length = 200)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "POSITIVESUMMARY", nullable = true, length = 200)
    public String getPositivesummary() {
        return positivesummary;
    }

    public void setPositivesummary(String positivesummary) {
        this.positivesummary = positivesummary;
    }

    @Basic
    @Column(name = "RESULTCOMPARE", nullable = true, length = 200)
    public String getResultcompare() {
        return resultcompare;
    }

    public void setResultcompare(String resultcompare) {
        this.resultcompare = resultcompare;
    }

    @Basic
    @Column(name = "INTERFACEMARKS", nullable = true, length = 50)
    public String getInterfacemarks() {
        return interfacemarks;
    }

    public void setInterfacemarks(String interfacemarks) {
        this.interfacemarks = interfacemarks;
    }

    @Basic
    @Column(name = "PATIENTFLAG", nullable = true, length = 50)
    public String getPatientflag() {
        return patientflag;
    }

    public void setPatientflag(String patientflag) {
        this.patientflag = patientflag;
    }

    @Basic
    @Column(name = "TIMINGSTARTEDAT", nullable = true)
    public Date getTimingstartedat() {
        return timingstartedat;
    }

    public void setTimingstartedat(Date timingstartedat) {
        this.timingstartedat = timingstartedat;
    }

    @Basic
    @Column(name = "TIMERESULTLASTCHANGE", nullable = true)
    public Date getDateresultlastchange() {
        return timeresultlastchange;
    }

    public void setDateresultlastchange(Date timeresultlastchange) {
        this.timeresultlastchange = timeresultlastchange;
    }

    @Basic
    @Column(name = "TIMERESULTLASTARCHIVE", nullable = true)
    public Date getDateresultlastarchive() {
        return timeresultlastarchive;
    }

    public void setDateresultlastarchive(Date timeresultlastarchive) {
        this.timeresultlastarchive = timeresultlastarchive;
    }

    @Basic
    @Column(name = "TIMERESULTLASTOLAP", nullable = true)
    public Date getDateresultlastolap() {
        return timeresultlastolap;
    }

    public void setDateresultlastolap(Date timeresultlastolap) {
        this.timeresultlastolap = timeresultlastolap;
    }

    @Basic
    @Column(name = "HOSPITALCODE", nullable = true, length = 50)
    public String getHospitalcode() {
        return hospitalcode;
    }

    public void setHospitalcode(String hospitalcode) {
        this.hospitalcode = hospitalcode;
    }

    @Basic
    @Column(name = "HOSPITALNAME", nullable = true, length = 100)
    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname;
    }

    @Basic
    @Column(name = "ID_EXAMPLACE", nullable = true, precision = 0)
    public Long getIdExamplace() {
        return idExamplace;
    }

    public void setIdExamplace(Long idExamplace) {
        this.idExamplace = idExamplace;
    }

    @Basic
    @Column(name = "PARSEDASSIGNEDSUITEANDFI", nullable = true, length = 300)
    public String getParsedassignedsuiteandfi() {
        return parsedassignedsuiteandfi;
    }

    public void setParsedassignedsuiteandfi(String parsedassignedsuiteandfi) {
        this.parsedassignedsuiteandfi = parsedassignedsuiteandfi;
    }

    @Basic
    @Column(name = "PARSEDASSIGNEDGROUPANDFI", nullable = true, length = 300)
    public String getParsedassignedgroupandfi() {
        return parsedassignedgroupandfi;
    }

    public void setParsedassignedgroupandfi(String parsedassignedgroupandfi) {
        this.parsedassignedgroupandfi = parsedassignedgroupandfi;
    }

    @Basic
    @Column(name = "PARSEDSUITEANDFI", nullable = true, length = 300)
    public String getParsedsuiteandfi() {
        return parsedsuiteandfi;
    }

    public void setParsedsuiteandfi(String parsedsuiteandfi) {
        this.parsedsuiteandfi = parsedsuiteandfi;
    }

    @Basic
    @Column(name = "PARSEDSUITEANDFILAB", nullable = true, length = 300)
    public String getParsedsuiteandfilab() {
        return parsedsuiteandfilab;
    }

    public void setParsedsuiteandfilab(String parsedsuiteandfilab) {
        this.parsedsuiteandfilab = parsedsuiteandfilab;
    }

    @Basic
    @Column(name = "ID_GUIDENURSE", nullable = true, precision = 0)
    public Long getIdGuidenurse() {
        return idGuidenurse;
    }

    public void setIdGuidenurse(Long idGuidenurse) {
        this.idGuidenurse = idGuidenurse;
    }

    @Basic
    @Column(name = "PATIENTNAMEENCODED", nullable = true, length = 100)
    public String getPatientnameencoded() {
        return patientnameencoded;
    }

    public void setPatientnameencoded(String patientnameencoded) {
        this.patientnameencoded = patientnameencoded;
    }

    @Basic
    @Column(name = "PATIENTCODEHIDEN", nullable = true, length = 50)
    public String getPatientcodehiden() {
        return patientcodehiden;
    }

    public void setPatientcodehiden(String patientcodehiden) {
        this.patientcodehiden = patientcodehiden;
    }

    @Basic
    @Column(name = "F_PDFCREATED", nullable = true, precision = 0)
    public Boolean getfPdfcreated() {
        return fPdfcreated;
    }

    public void setfPdfcreated(Boolean fPdfcreated) {
        this.fPdfcreated = fPdfcreated;
    }

    @Basic
    @Column(name = "F_WORDCREATED", nullable = true, precision = 0)
    public Boolean getfWordcreated() {
        return fWordcreated;
    }

    public void setfWordcreated(Boolean fWordcreated) {
        this.fWordcreated = fWordcreated;
    }

    @Basic
    @Column(name = "F_WORDPRINTED", nullable = true, precision = 0)
    public Boolean getfWordprinted() {
        return fWordprinted;
    }

    public void setfWordprinted(Boolean fWordprinted) {
        this.fWordprinted = fWordprinted;
    }

    @Basic
    @Column(name = "GUIDANCENOTE2", nullable = true, length = 200)
    public String getGuidancenote2() {
        return guidancenote2;
    }

    public void setGuidancenote2(String guidancenote2) {
        this.guidancenote2 = guidancenote2;
    }

    @Basic
    @Column(name = "F_USECODEHIDEN", nullable = true, precision = 0)
    public Boolean getfUsecodehiden() {
        return fUsecodehiden;
    }

    public void setfUsecodehiden(Boolean fUsecodehiden) {
        this.fUsecodehiden = fUsecodehiden;
    }

    @Basic
    @Column(name = "ID_PATIENTCLASS2", nullable = true, precision = 0)
    public Long getIdPatientclass2() {
        return idPatientclass2;
    }

    public void setIdPatientclass2(Long idPatientclass2) {
        this.idPatientclass2 = idPatientclass2;
    }

    @Basic
    @Column(name = "ID_PATIENTCLASS3", nullable = true, precision = 0)
    public Long getIdPatientclass3() {
        return idPatientclass3;
    }

    public void setIdPatientclass3(Long idPatientclass3) {
        this.idPatientclass3 = idPatientclass3;
    }

    @Basic
    @Column(name = "DATEREGISTERNOTIME", nullable = true)
    public Date getDateregisternotime() {
        return dateregisternotime;
    }

    public void setDateregisternotime(Date dateregisternotime) {
        this.dateregisternotime = dateregisternotime;
    }

    @Basic
    @Column(name = "COUNTERREPORTPRINTED", nullable = true, precision = 0)
    public Long getCounterreportprinted() {
        return counterreportprinted;
    }

    public void setCounterreportprinted(Long counterreportprinted) {
        this.counterreportprinted = counterreportprinted;
    }

    @Basic
    @Column(name = "F_PRINTCOMPARINGREPORT", nullable = true, precision = 0)
    public Boolean getfPrintcomparingreport() {
        return fPrintcomparingreport;
    }

    public void setfPrintcomparingreport(Boolean fPrintcomparingreport) {
        this.fPrintcomparingreport = fPrintcomparingreport;
    }

    @Basic
    @Column(name = "F_ISRECHECK", nullable = true, precision = 0)
    public Boolean getfIsrecheck() {
        return fIsrecheck;
    }

    public void setfIsrecheck(Boolean fIsrecheck) {
        this.fIsrecheck = fIsrecheck;
    }

    @Basic
    @Column(name = "F_SETTLENONE", nullable = true, precision = 0)
    public Boolean getfSettlenone() {
        return fSettlenone;
    }

    public void setfSettlenone(Boolean fSettlenone) {
        this.fSettlenone = fSettlenone;
    }

    @Basic
    @Column(name = "F_GUIDANCERETURNED", nullable = true, precision = 0)
    public Boolean getfGuidancereturned() {
        return fGuidancereturned;
    }

    public void setfGuidancereturned(Boolean fGuidancereturned) {
        this.fGuidancereturned = fGuidancereturned;
    }

    @Basic
    @Column(name = "DATEGUIDANCERETURNED", nullable = true)
    public Date getDateguidancereturned() {
        return dateguidancereturned;
    }

    public void setDateguidancereturned(Date dateguidancereturned) {
        this.dateguidancereturned = dateguidancereturned;
    }

    @Basic
    @Column(name = "ID_GUIDANCERETURNEDBY", nullable = true, precision = 0)
    public Long getIdGuidancereturnedby() {
        return idGuidancereturnedby;
    }

    public void setIdGuidancereturnedby(Long idGuidancereturnedby) {
        this.idGuidancereturnedby = idGuidancereturnedby;
    }

    @Basic
    @Column(name = "F_OUTPATIENT", nullable = true, precision = 0)
    public Boolean getfOutpatient() {
        return fOutpatient;
    }

    public void setfOutpatient(Boolean fOutpatient) {
        this.fOutpatient = fOutpatient;
    }

    @Basic
    @Column(name = "PATIENTNAMERECEIPT", nullable = true, length = 50)
    public String getPatientnamereceipt() {
        return patientnamereceipt;
    }

    public void setPatientnamereceipt(String patientnamereceipt) {
        this.patientnamereceipt = patientnamereceipt;
    }

    @Basic
    @Column(name = "PATIENTNAMEPINYIN", nullable = true, length = 50)
    public String getPatientnamepinyin() {
        return patientnamepinyin;
    }

    public void setPatientnamepinyin(String patientnamepinyin) {
        this.patientnamepinyin = patientnamepinyin;
    }

    @Basic
    @Column(name = "F_FORPREPAREFINANCIALCONFIRM", nullable = true, precision = 0)
    public Boolean getfForpreparefinancialconfirm() {
        return fForpreparefinancialconfirm;
    }

    public void setfForpreparefinancialconfirm(Boolean fForpreparefinancialconfirm) {
        this.fForpreparefinancialconfirm = fForpreparefinancialconfirm;
    }

    @Basic
    @Column(name = "STATUSOFHM", nullable = true, length = 10)
    public String getStatusofhm() {
        return statusofhm;
    }

    public void setStatusofhm(String statusofhm) {
        this.statusofhm = statusofhm;
    }

    @Basic
    @Column(name = "INSTANCETAG", nullable = true, length = 30)
    public String getInstancetag() {
        return instancetag;
    }

    public void setInstancetag(String instancetag) {
        this.instancetag = instancetag;
    }

    @Basic
    @Column(name = "KEYBIRTHPLACE", nullable = true, length = 50)
    public String getKeybirthplace() {
        return keybirthplace;
    }

    public void setKeybirthplace(String keybirthplace) {
        this.keybirthplace = keybirthplace;
    }

    @Basic
    @Column(name = "KEYBLOODTYPE", nullable = true, length = 20)
    public String getKeybloodtype() {
        return keybloodtype;
    }

    public void setKeybloodtype(String keybloodtype) {
        this.keybloodtype = keybloodtype;
    }

    @Basic
    @Column(name = "EXAMMETHOD", nullable = true, length = 20)
    public String getExammethod() {
        return exammethod;
    }

    public void setExammethod(String exammethod) {
        this.exammethod = exammethod;
    }

    @Basic
    @Column(name = "INPATIENTNO", nullable = true, length = 50)
    public String getInpatientno() {
        return inpatientno;
    }

    public void setInpatientno(String inpatientno) {
        this.inpatientno = inpatientno;
    }

    @Basic
    @Column(name = "INSURANCENO", nullable = true, length = 50)
    public String getInsuranceno() {
        return insuranceno;
    }

    public void setInsuranceno(String insuranceno) {
        this.insuranceno = insuranceno;
    }

    @Basic
    @Column(name = "KEYPAYWAY", nullable = true, length = 20)
    public String getKeypayway() {
        return keypayway;
    }

    public void setKeypayway(String keypayway) {
        this.keypayway = keypayway;
    }

    @Basic
    @Column(name = "HEALTHCARD", nullable = true, length = 500)
    public String getHealthcard() {
        return healthcard;
    }

    public void setHealthcard(String healthcard) {
        this.healthcard = healthcard;
    }

    @Basic
    @Column(name = "EXAMPOINT", nullable = true, length = 200)
    public String getExampoint() {
        return exampoint;
    }

    public void setExampoint(String exampoint) {
        this.exampoint = exampoint;
    }

    @Basic
    @Column(name = "FINGERPRINT", nullable = true, length = 255)
    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    @Basic
    @Column(name = "COUNTREPORTCOVERPRINTED", nullable = true, precision = 0)
    public Long getCountreportcoverprinted() {
        return countreportcoverprinted;
    }

    public void setCountreportcoverprinted(Long countreportcoverprinted) {
        this.countreportcoverprinted = countreportcoverprinted;
    }

    @Basic
    @Column(name = "COUNTREPORTPRINTED", nullable = true, precision = 0)
    public Long getCountreportprinted() {
        return countreportprinted;
    }

    public void setCountreportprinted(Long countreportprinted) {
        this.countreportprinted = countreportprinted;
    }

    @Basic
    @Column(name = "COUNTREPORTPDF", nullable = true, precision = 0)
    public Long getCountreportpdf() {
        return countreportpdf;
    }

    public void setCountreportpdf(Long countreportpdf) {
        this.countreportpdf = countreportpdf;
    }

    @Basic
    @Column(name = "COUNTREPORTWORD", nullable = true, precision = 0)
    public Long getCountreportword() {
        return countreportword;
    }

    public void setCountreportword(Long countreportword) {
        this.countreportword = countreportword;
    }

    @Basic
    @Column(name = "COUNTREPORTXML", nullable = true, precision = 0)
    public Long getCountreportxml() {
        return countreportxml;
    }

    public void setCountreportxml(Long countreportxml) {
        this.countreportxml = countreportxml;
    }

    @Basic
    @Column(name = "COUNTREPORTCOMPARE", nullable = true, precision = 0)
    public Long getCountreportcompare() {
        return countreportcompare;
    }

    public void setCountreportcompare(Long countreportcompare) {
        this.countreportcompare = countreportcompare;
    }

    @Basic
    @Column(name = "COUNTREPORTCOMPAREPDF", nullable = true, precision = 0)
    public Long getCountreportcomparepdf() {
        return countreportcomparepdf;
    }

    public void setCountreportcomparepdf(Long countreportcomparepdf) {
        this.countreportcomparepdf = countreportcomparepdf;
    }

    @Basic
    @Column(name = "COUNTREPORTCOMPAREWORD", nullable = true, precision = 0)
    public Long getCountreportcompareword() {
        return countreportcompareword;
    }

    public void setCountreportcompareword(Long countreportcompareword) {
        this.countreportcompareword = countreportcompareword;
    }

    @Basic
    @Column(name = "COUNTREPORTCOMPAREXML", nullable = true, precision = 0)
    public Long getCountreportcomparexml() {
        return countreportcomparexml;
    }

    public void setCountreportcomparexml(Long countreportcomparexml) {
        this.countreportcomparexml = countreportcomparexml;
    }

    @Basic
    @Column(name = "COUNTREPORTOCCUPATION", nullable = true, precision = 0)
    public Long getCountreportoccupation() {
        return countreportoccupation;
    }

    public void setCountreportoccupation(Long countreportoccupation) {
        this.countreportoccupation = countreportoccupation;
    }

    @Basic
    @Column(name = "COUNTREPORTOCCUPATIONPDF", nullable = true, precision = 0)
    public Long getCountreportoccupationpdf() {
        return countreportoccupationpdf;
    }

    public void setCountreportoccupationpdf(Long countreportoccupationpdf) {
        this.countreportoccupationpdf = countreportoccupationpdf;
    }

    @Basic
    @Column(name = "COUNTREPORTOCCUPATIONWORD", nullable = true, precision = 0)
    public Long getCountreportoccupationword() {
        return countreportoccupationword;
    }

    public void setCountreportoccupationword(Long countreportoccupationword) {
        this.countreportoccupationword = countreportoccupationword;
    }

    @Basic
    @Column(name = "COUNTREPORTOCCUPATIONXML", nullable = true, precision = 0)
    public Long getCountreportoccupationxml() {
        return countreportoccupationxml;
    }

    public void setCountreportoccupationxml(Long countreportoccupationxml) {
        this.countreportoccupationxml = countreportoccupationxml;
    }

    @Basic
    @Column(name = "SCBS", nullable = true, precision = 0)
    public Boolean getScbs() {
        return scbs;
    }

    public void setScbs(Boolean scbs) {
        this.scbs = scbs;
    }

    @Basic
    @Column(name = "ID_TJTC", nullable = true, length = 32)
    public String getIdTjtc() {
        return idTjtc;
    }

    public void setIdTjtc(String idTjtc) {
        this.idTjtc = idTjtc;
    }

    @Basic
    @Column(name = "JZDW", nullable = true, length = 100)
    public String getJzdw() {
        return jzdw;
    }

    public void setJzdw(String jzdw) {
        this.jzdw = jzdw;
    }

    @Basic
    @Column(name = "JZDWR", nullable = true, length = 20)
    public String getJzdwr() {
        return jzdwr;
    }

    public void setJzdwr(String jzdwr) {
        this.jzdwr = jzdwr;
    }

    @Basic
    @Column(name = "SPR", nullable = true, length = 20)
    public String getSpr() {
        return spr;
    }

    public void setSpr(String spr) {
        this.spr = spr;
    }

    @Basic
    @Column(name = "TJR", nullable = true, length = 50)
    public String getTjr() {
        return tjr;
    }

    public void setTjr(String tjr) {
        this.tjr = tjr;
    }

    @Basic
    @Column(name = "LQFS", nullable = true, length = 10)
    public String getLqfs() {
        return lqfs;
    }

    public void setLqfs(String lqfs) {
        this.lqfs = lqfs;
    }

    @Basic
    @Column(name = "YZBM", nullable = true, precision = 0)
    public Long getYzbm() {
        return yzbm;
    }

    public void setYzbm(Long yzbm) {
        this.yzbm = yzbm;
    }

    @Basic
    @Column(name = "YJADDRESS", nullable = true, length = 200)
    public String getYjaddress() {
        return yjaddress;
    }

    public void setYjaddress(String yjaddress) {
        this.yjaddress = yjaddress;
    }

    @Basic
    @Column(name = "QTXZ", nullable = true, length = 2000)
    public String getQtxz() {
        return qtxz;
    }

    public void setQtxz(String qtxz) {
        this.qtxz = qtxz;
    }

    @Basic
    @Column(name = "IS_HMDB", nullable = true, length = 2000)
    public String getIsHmdb() {
        return isHmdb;
    }

    public void setIsHmdb(String isHmdb) {
        this.isHmdb = isHmdb;
    }

    @Basic
    @Column(name = "IS_HMD", nullable = true, precision = 0)
    public Boolean getHmd() {
        return isHmd;
    }

    public void setHmd(Boolean hmd) {
        isHmd = hmd;
    }

    @Basic
    @Column(name = "ISJJ", nullable = true, precision = 0)
    public Boolean getIsjj() {
        return isjj;
    }

    public void setIsjj(Boolean isjj) {
        this.isjj = isjj;
    }

    @Basic
    @Column(name = "ZGL", nullable = true, precision = 0)
    public Long getZgl() {
        return zgl;
    }

    public void setZgl(Long zgl) {
        this.zgl = zgl;
    }

    @Basic
    @Column(name = "JHGL", nullable = true, precision = 0)
    public Long getJhgl() {
        return jhgl;
    }

    public void setJhgl(Long jhgl) {
        this.jhgl = jhgl;
    }

    @Basic
    @Column(name = "JHYS", nullable = true, length = 2000)
    public String getJhys() {
        return jhys;
    }

    public void setJhys(String jhys) {
        this.jhys = jhys;
    }

    @Basic
    @Column(name = "JKTJZT", nullable = true, precision = 0)
    public Long getJktjzt() {
        return jktjzt;
    }

    public void setJktjzt(Long jktjzt) {
        this.jktjzt = jktjzt;
    }

    @Basic
    @Column(name = "ZYTJZT", nullable = true, precision = 0)
    public Long getZytjzt() {
        return zytjzt;
    }

    public void setZytjzt(Long zytjzt) {
        this.zytjzt = zytjzt;
    }

    @Basic
    @Column(name = "TMYD", nullable = true, precision = 0)
    public Long getTmyd() {
        return tmyd;
    }

    public void setTmyd(Long tmyd) {
        this.tmyd = tmyd;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MEDICALDATE", nullable = true)
    public Date getMedicaldate() {
        return medicaldate;
    }

    public void setMedicaldate(Date medicaldate) {
        this.medicaldate = medicaldate;
    }

    @Basic
    @Column(name = "TRADES", nullable = true, length = 50)
    public String getTrades() {
        return trades;
    }

    public void setTrades(String trades) {
        this.trades = trades;
    }

    @Basic
    @Column(name = "CJJGSFYHF", nullable = true, precision = 0)
    public Boolean getCjjgsfyhf() {
        return cjjgsfyhf;
    }

    public void setCjjgsfyhf(Boolean cjjgsfyhf) {
        this.cjjgsfyhf = cjjgsfyhf;
    }

    @Basic
    @Column(name = "BHGYBSFYHF", nullable = true, precision = 0)
    public Boolean getBhgybsfyhf() {
        return bhgybsfyhf;
    }

    public void setBhgybsfyhf(Boolean bhgybsfyhf) {
        this.bhgybsfyhf = bhgybsfyhf;
    }

    @Basic
    @Column(name = "YXJGSFYHF", nullable = true, precision = 0)
    public Boolean getYxjgsfyhf() {
        return yxjgsfyhf;
    }

    public void setYxjgsfyhf(Boolean yxjgsfyhf) {
        this.yxjgsfyhf = yxjgsfyhf;
    }

    @Basic
    @Column(name = "MEDICALTYPE", nullable = true, length = 20)
    public String getMedicaltype() {
        return medicaltype;
    }

    public void setMedicaltype(String medicaltype) {
        this.medicaltype = medicaltype;
    }

    @Basic
    @Column(name = "PREPAYMENT", nullable = true, precision = 2)
    public Long getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(Long prepayment) {
        this.prepayment = prepayment;
    }

    @Basic
    @Column(name = "TCPRICE", nullable = true, precision = 2)
    public Long getTcprice() {
        return tcprice;
    }

    public void setTcprice(Long tcprice) {
        this.tcprice = tcprice;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "CULTURAL", nullable = true, precision = 0)
    public Boolean getCultural() {
        return cultural;
    }

    public void setCultural(Boolean cultural) {
        this.cultural = cultural;
    }

    @Basic
    @Column(name = "EVER_OF_DISEASE", nullable = true, length = 250)
    public String getEverOfDisease() {
        return everOfDisease;
    }

    public void setEverOfDisease(String everOfDisease) {
        this.everOfDisease = everOfDisease;
    }

    @Basic
    @Column(name = "CCNL", nullable = true, length = 10)
    public String getCcnl() {
        return ccnl;
    }

    public void setCcnl(String ccnl) {
        this.ccnl = ccnl;
    }

    @Basic
    @Column(name = "JQ", nullable = true, length = 10)
    public String getJq() {
        return jq;
    }

    public void setJq(String jq) {
        this.jq = jq;
    }

    @Basic
    @Column(name = "ZQ", nullable = true, length = 10)
    public String getZq() {
        return zq;
    }

    public void setZq(String zq) {
        this.zq = zq;
    }

    @Basic
    @Column(name = "TJNL", nullable = true, length = 10)
    public String getTjnl() {
        return tjnl;
    }

    public void setTjnl(String tjnl) {
        this.tjnl = tjnl;
    }

    @Basic
    @Column(name = "FAMILY_NUMBER", nullable = true, length = 4)
    public String getFamilyNumber() {
        return familyNumber;
    }

    public void setFamilyNumber(String familyNumber) {
        this.familyNumber = familyNumber;
    }

    @Basic
    @Column(name = "ZC", nullable = true, length = 4)
    public String getZc() {
        return zc;
    }

    public void setZc(String zc) {
        this.zc = zc;
    }

    @Basic
    @Column(name = "SC", nullable = true, length = 4)
    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    @Basic
    @Column(name = "LC", nullable = true, length = 4)
    public String getLc() {
        return lc;
    }

    public void setLc(String lc) {
        this.lc = lc;
    }

    @Basic
    @Column(name = "JT", nullable = true, length = 4)
    public String getJt() {
        return jt;
    }

    public void setJt(String jt) {
        this.jt = jt;
    }

    @Basic
    @Column(name = "YWRC", nullable = true, length = 4)
    public String getYwrc() {
        return ywrc;
    }

    public void setYwrc(String ywrc) {
        this.ywrc = ywrc;
    }

    @Basic
    @Column(name = "ABSTAIN_SMOKE_NOTE", nullable = true, length = 10)
    public String getAbstainSmokeNote() {
        return abstainSmokeNote;
    }

    public void setAbstainSmokeNote(String abstainSmokeNote) {
        this.abstainSmokeNote = abstainSmokeNote;
    }

    @Basic
    @Column(name = "EVERYDAY_SMOKE_N", nullable = true, length = 10)
    public String getEverydaySmokeN() {
        return everydaySmokeN;
    }

    public void setEverydaySmokeN(String everydaySmokeN) {
        this.everydaySmokeN = everydaySmokeN;
    }

    @Basic
    @Column(name = "SMOKE_YEAR", nullable = true, length = 10)
    public String getSmokeYear() {
        return smokeYear;
    }

    public void setSmokeYear(String smokeYear) {
        this.smokeYear = smokeYear;
    }

    @Basic
    @Column(name = "NO_KISS_THE_CUP", nullable = true, length = 30)
    public String getNoKissTheCup() {
        return noKissTheCup;
    }

    public void setNoKissTheCup(String noKissTheCup) {
        this.noKissTheCup = noKissTheCup;
    }

    @Basic
    @Column(name = "BETWEEN_KISS_THE_CUP", nullable = true, length = 30)
    public String getBetweenKissTheCup() {
        return betweenKissTheCup;
    }

    public void setBetweenKissTheCup(String betweenKissTheCup) {
        this.betweenKissTheCup = betweenKissTheCup;
    }

    @Basic
    @Column(name = "EVERMORE_KISS", nullable = true, length = 100)
    public String getEvermoreKiss() {
        return evermoreKiss;
    }

    public void setEvermoreKiss(String evermoreKiss) {
        this.evermoreKiss = evermoreKiss;
    }

    @Basic
    @Column(name = "ABSTAIN_LOST_KISS", nullable = true, length = 100)
    public String getAbstainLostKiss() {
        return abstainLostKiss;
    }

    public void setAbstainLostKiss(String abstainLostKiss) {
        this.abstainLostKiss = abstainLostKiss;
    }

    @Basic
    @Column(name = "KISS_YEAR_N", nullable = true, length = 10)
    public String getKissYearN() {
        return kissYearN;
    }

    public void setKissYearN(String kissYearN) {
        this.kissYearN = kissYearN;
    }

    @Basic
    @Column(name = "KISS_AMOUNT", nullable = true, length = 100)
    public String getKissAmount() {
        return kissAmount;
    }

    public void setKissAmount(String kissAmount) {
        this.kissAmount = kissAmount;
    }

    @Basic
    @Column(name = "KISS_TYPE", nullable = true, length = 20)
    public String getKissType() {
        return kissType;
    }

    public void setKissType(String kissType) {
        this.kissType = kissType;
    }

    @Basic
    @Column(name = "FAMILY_OF_DISEASE", nullable = true, length = 1000)
    public String getFamilyOfDisease() {
        return familyOfDisease;
    }

    public void setFamilyOfDisease(String familyOfDisease) {
        this.familyOfDisease = familyOfDisease;
    }

    @Basic
    @Column(name = "SYMPTOM", nullable = true, length = 1000)
    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    @Basic
    @Column(name = "IS_AUDIT", nullable = true, precision = 0)
    public Boolean getAudit() {
        return isAudit;
    }

    public void setAudit(Boolean audit) {
        isAudit = audit;
    }

    @Basic
    @Column(name = "EVER_OF_DISEASE_REMARK", nullable = true, length = 1000)
    public String getEverOfDiseaseRemark() {
        return everOfDiseaseRemark;
    }

    public void setEverOfDiseaseRemark(String everOfDiseaseRemark) {
        this.everOfDiseaseRemark = everOfDiseaseRemark;
    }

    @Basic
    @Column(name = "CREATE_REPORT_NUM", nullable = true, precision = 0)
    public Byte getCreateReportNum() {
        return createReportNum;
    }

    public void setCreateReportNum(Byte createReportNum) {
        this.createReportNum = createReportNum;
    }

    @Basic
    @Column(name = "WORK_DATE", nullable = true)
    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    @Basic
    @Column(name = "HARM_DATE", nullable = true)
    public Date getHarmDate() {
        return harmDate;
    }

    public void setHarmDate(Date harmDate) {
        this.harmDate = harmDate;
    }

    @Basic
    @Column(name = "PICTURE", nullable = true)
    @JsonIgnore
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Basic
    @Column(name = "ADVICE", nullable = true)
    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    @Basic
    @Column(name = "DISEASE_PRINT_NUM", nullable = true, precision = 0)
    public Long getDiseasePrintNum() {
        return diseasePrintNum;
    }

    public void setDiseasePrintNum(Long diseasePrintNum) {
        this.diseasePrintNum = diseasePrintNum;
    }

    @Basic
    @Column(name = "HEALTH_PRINT_NUM", nullable = true, precision = 0)
    public Long getHealthPrintNum() {
        return healthPrintNum;
    }

    public void setHealthPrintNum(Long healthPrintNum) {
        this.healthPrintNum = healthPrintNum;
    }

    @Basic
    @Column(name = "READYTOFINAL_DATE", nullable = true)
    public Date getReadytofinalDate() {
        return readytofinalDate;
    }

    public void setReadytofinalDate(Date readytofinalDate) {
        this.readytofinalDate = readytofinalDate;
    }

    @Basic
    @Column(name = "GUIDE_SIGNLE_COUNT", nullable = true, precision = 0)
    public Long getGuideSignleCount() {
        return guideSignleCount;
    }

    public void setGuideSignleCount(Long guideSignleCount) {
        this.guideSignleCount = guideSignleCount;
    }

    @Basic
    @Column(name = "SHORT_CODE", nullable = true, precision = 0)
    public Long getShortCode() {
        return shortCode;
    }

    public void setShortCode(Long shortCode) {
        this.shortCode = shortCode;
    }

    @Basic
    @Column(name = "IS_NOTICED", nullable = true, precision = 0)
    public Boolean getNoticed() {
        return isNoticed;
    }

    public void setNoticed(Boolean noticed) {
        isNoticed = noticed;
    }

    @Basic
    @Column(name = "REVIEW_PDF", nullable = true, length = 200)
    public String getReviewPdf() {
        return reviewPdf;
    }

    public void setReviewPdf(String reviewPdf) {
        this.reviewPdf = reviewPdf;
    }

    @Basic
    @Column(name = "CONTRAINDICATED_PDF", nullable = true, length = 200)
    public String getContraindicatedPdf() {
        return contraindicatedPdf;
    }

    public void setContraindicatedPdf(String contraindicatedPdf) {
        this.contraindicatedPdf = contraindicatedPdf;
    }

    @Basic
    @Column(name = "DISEASE_PDF", nullable = true, length = 200)
    public String getDiseasePdf() {
        return diseasePdf;
    }

    public void setDiseasePdf(String diseasePdf) {
        this.diseasePdf = diseasePdf;
    }

    @Basic
    @Column(name = "SIGN_PICTURE", nullable = true)
    public String getSignPicture() {
        return signPicture;
    }

    public void setSignPicture(String signPicture) {
        this.signPicture = signPicture;
    }

    @Basic
    @Column(name = "IS_NEW_PACS", nullable = true, precision = 0)
    public Long getIsNewPacs() {
        return isNewPacs;
    }

    public void setIsNewPacs(Long isNewPacs) {
        this.isNewPacs = isNewPacs;
    }

    @Basic
    @Column(name = "TS_LIMIT", nullable = true, precision = 2)
    public Long getTsLimit() {
        return tsLimit;
    }

    public void setTsLimit(Long tsLimit) {
        this.tsLimit = tsLimit;
    }

    @Basic
    @Column(name = "PHYSIQUE", nullable = true, length = 105)
    public String getPhysique() {
        return physique;
    }

    public void setPhysique(String physique) {
        this.physique = physique;
    }

    @Basic
    @Column(name = "DOC_NAME", nullable = true, length = 50)
    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    @Basic
    @Column(name = "COMMITTEE", nullable = true, length = 300)
    public String getCommittee() {
        return committee;
    }

    public void setCommittee(String committee) {
        this.committee = committee;
    }

    @Basic
    @Column(name = "STREET", nullable = true, length = 300)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peispatient that = (Peispatient) o;
        return Objects.equals(idPatient, that.idPatient) &&
                Objects.equals(idOrgpatient, that.idOrgpatient) &&
                Objects.equals(idCis, that.idCis) &&
                Objects.equals(idPatientarchive, that.idPatientarchive) &&
                Objects.equals(patientcode, that.patientcode) &&
                Objects.equals(patientcodeprn, that.patientcodeprn) &&
                Objects.equals(patientarchiveno, that.patientarchiveno) &&
                Objects.equals(patientcardno, that.patientcardno) &&
                Objects.equals(patientbizno, that.patientbizno) &&
                Objects.equals(idcardno, that.idcardno) &&
                Objects.equals(dailynumberdate, that.dailynumberdate) &&
                Objects.equals(dailynumber, that.dailynumber) &&
                Objects.equals(numtotal, that.numtotal) &&
                Objects.equals(numyear, that.numyear) &&
                Objects.equals(nummonth, that.nummonth) &&
                Objects.equals(numday, that.numday) &&
                Objects.equals(numorg, that.numorg) &&
                Objects.equals(numorgresv, that.numorgresv) &&
                Objects.equals(idPatientlinked, that.idPatientlinked) &&
                Objects.equals(idNonorg, that.idNonorg) &&
                Objects.equals(patientname, that.patientname) &&
                Objects.equals(inputCode, that.inputCode) &&
                Objects.equals(idOrgreservationgroup, that.idOrgreservationgroup) &&
                Objects.equals(idOrgreservation, that.idOrgreservation) &&
                Objects.equals(idOrg, that.idOrg) &&
                Objects.equals(orgName, that.orgName) &&
                Objects.equals(orgDepart, that.orgDepart) &&
                Objects.equals(orgDepartsuba, that.orgDepartsuba) &&
                Objects.equals(orgDepartsubb, that.orgDepartsubb) &&
                Objects.equals(orgDepartsubc, that.orgDepartsubc) &&
                Objects.equals(orgDepartsubd, that.orgDepartsubd) &&
                Objects.equals(orgDepartsube, that.orgDepartsube) &&
                Objects.equals(idFeetype, that.idFeetype) &&
                Objects.equals(idPayway, that.idPayway) &&
                Objects.equals(payway, that.payway) &&
                Objects.equals(offpercent, that.offpercent) &&
                Objects.equals(maxoffpercent, that.maxoffpercent) &&
                Objects.equals(personpricelimit, that.personpricelimit) &&
                Objects.equals(idSex, that.idSex) &&
                Objects.equals(sex, that.sex) &&
                Objects.equals(birthdate, that.birthdate) &&
                Objects.equals(age, that.age) &&
                Objects.equals(idAgeunit, that.idAgeunit) &&
                Objects.equals(ageunit, that.ageunit) &&
                Objects.equals(ageofreal, that.ageofreal) &&
                Objects.equals(idMarriage, that.idMarriage) &&
                Objects.equals(marriage, that.marriage) &&
                Objects.equals(idNation, that.idNation) &&
                Objects.equals(nation, that.nation) &&
                Objects.equals(address, that.address) &&
                Objects.equals(idInformway, that.idInformway) &&
                Objects.equals(idOpendoctor, that.idOpendoctor) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(idPatientclass, that.idPatientclass) &&
                Objects.equals(idEducation, that.idEducation) &&
                Objects.equals(education, that.education) &&
                Objects.equals(idOccupation, that.idOccupation) &&
                Objects.equals(occupation, that.occupation) &&
                Objects.equals(idResarea, that.idResarea) &&
                Objects.equals(resarea, that.resarea) &&
                Objects.equals(dateinorganization, that.dateinorganization) &&
                Objects.equals(fIsforprepare, that.fIsforprepare) &&
                Objects.equals(fIsforreserve, that.fIsforreserve) &&
                Objects.equals(datecreated, that.datecreated) &&
                Objects.equals(fRegistered, that.fRegistered) &&
                Objects.equals(dateregister, that.dateregister) &&
                Objects.equals(positionCode, that.positionCode) &&
                Objects.equals(jobtypeCode, that.jobtypeCode) &&
                Objects.equals(moneyamount, that.moneyamount) &&
                Objects.equals(moneyamountpaid, that.moneyamountpaid) &&
                Objects.equals(guidancenote, that.guidancenote) &&
                Objects.equals(workno, that.workno) &&
                Objects.equals(idDoctorreg, that.idDoctorreg) &&
                Objects.equals(doctorreg, that.doctorreg) &&
                Objects.equals(idExamtype, that.idExamtype) &&
                Objects.equals(idExamsuite, that.idExamsuite) &&
                Objects.equals(examsuiteName, that.examsuiteName) &&
                Objects.equals(examsuiteAlias, that.examsuiteAlias) &&
                Objects.equals(idDoctorconclusion, that.idDoctorconclusion) &&
                Objects.equals(doctorconclusion, that.doctorconclusion) &&
                Objects.equals(idDoctorapply, that.idDoctorapply) &&
                Objects.equals(doctorapply, that.doctorapply) &&
                Objects.equals(fGuidanceprinted, that.fGuidanceprinted) &&
                Objects.equals(fFeecharged, that.fFeecharged) &&
                Objects.equals(fExamstarted, that.fExamstarted) &&
                Objects.equals(fReadytofinal, that.fReadytofinal) &&
                Objects.equals(idDoctorfee, that.idDoctorfee) &&
                Objects.equals(doctorfee, that.doctorfee) &&
                Objects.equals(fPaused, that.fPaused) &&
                Objects.equals(fFinallocked, that.fFinallocked) &&
                Objects.equals(fFinalexamed, that.fFinalexamed) &&
                Objects.equals(fFinalapproved, that.fFinalapproved) &&
                Objects.equals(idDoctorfinal, that.idDoctorfinal) &&
                Objects.equals(doctorfinalNameR, that.doctorfinalNameR) &&
                Objects.equals(datefinalexamed, that.datefinalexamed) &&
                Objects.equals(idDoctorfinalapproved, that.idDoctorfinalapproved) &&
                Objects.equals(datefinalapproved, that.datefinalapproved) &&
                Objects.equals(fCardissued, that.fCardissued) &&
                Objects.equals(fCardreturned, that.fCardreturned) &&
                Objects.equals(fCoverprinted, that.fCoverprinted) &&
                Objects.equals(fReportprinted, that.fReportprinted) &&
                Objects.equals(idReportprintedby, that.idReportprintedby) &&
                Objects.equals(datereportprinted, that.datereportprinted) &&
                Objects.equals(fReportinformed, that.fReportinformed) &&
                Objects.equals(datereportinformed, that.datereportinformed) &&
                Objects.equals(fReportfetched, that.fReportfetched) &&
                Objects.equals(datereportfetched, that.datereportfetched) &&
                Objects.equals(fIssevere, that.fIssevere) &&
                Objects.equals(fClosed, that.fClosed) &&
                Objects.equals(dateclosed, that.dateclosed) &&
                Objects.equals(fNeedtraced, that.fNeedtraced) &&
                Objects.equals(fDiffperson, that.fDiffperson) &&
                Objects.equals(confidentiallevel, that.confidentiallevel) &&
                Objects.equals(fSettleall, that.fSettleall) &&
                Objects.equals(signature, that.signature) &&
                Objects.equals(note, that.note) &&
                Objects.equals(dtLastmodifiedthisat, that.dtLastmodifiedthisat) &&
                Objects.equals(fInneroper, that.fInneroper) &&
                Objects.equals(severedegree, that.severedegree) &&
                Objects.equals(severedegreenote, that.severedegreenote) &&
                Objects.equals(fSevereinformed, that.fSevereinformed) &&
                Objects.equals(severeinformtime, that.severeinformtime) &&
                Objects.equals(idSevereinformby, that.idSevereinformby) &&
                Objects.equals(conclusion, that.conclusion) &&
                Objects.equals(conclusionsummary, that.conclusionsummary) &&
                Objects.equals(suggestion, that.suggestion) &&
                Objects.equals(conclusionrich, that.conclusionrich) &&
                Objects.equals(dietguide, that.dietguide) &&
                Objects.equals(sportguide, that.sportguide) &&
                Objects.equals(knowledge, that.knowledge) &&
                Objects.equals(message, that.message) &&
                Objects.equals(positivesummary, that.positivesummary) &&
                Objects.equals(resultcompare, that.resultcompare) &&
                Objects.equals(interfacemarks, that.interfacemarks) &&
                Objects.equals(patientflag, that.patientflag) &&
                Objects.equals(timingstartedat, that.timingstartedat) &&
                Objects.equals(timeresultlastchange, that.timeresultlastchange) &&
                Objects.equals(timeresultlastarchive, that.timeresultlastarchive) &&
                Objects.equals(timeresultlastolap, that.timeresultlastolap) &&
                Objects.equals(hospitalcode, that.hospitalcode) &&
                Objects.equals(hospitalname, that.hospitalname) &&
                Objects.equals(idExamplace, that.idExamplace) &&
                Objects.equals(parsedassignedsuiteandfi, that.parsedassignedsuiteandfi) &&
                Objects.equals(parsedassignedgroupandfi, that.parsedassignedgroupandfi) &&
                Objects.equals(parsedsuiteandfi, that.parsedsuiteandfi) &&
                Objects.equals(parsedsuiteandfilab, that.parsedsuiteandfilab) &&
                Objects.equals(idGuidenurse, that.idGuidenurse) &&
                Objects.equals(patientnameencoded, that.patientnameencoded) &&
                Objects.equals(patientcodehiden, that.patientcodehiden) &&
                Objects.equals(fPdfcreated, that.fPdfcreated) &&
                Objects.equals(fWordcreated, that.fWordcreated) &&
                Objects.equals(fWordprinted, that.fWordprinted) &&
                Objects.equals(guidancenote2, that.guidancenote2) &&
                Objects.equals(fUsecodehiden, that.fUsecodehiden) &&
                Objects.equals(idPatientclass2, that.idPatientclass2) &&
                Objects.equals(idPatientclass3, that.idPatientclass3) &&
                Objects.equals(dateregisternotime, that.dateregisternotime) &&
                Objects.equals(counterreportprinted, that.counterreportprinted) &&
                Objects.equals(fPrintcomparingreport, that.fPrintcomparingreport) &&
                Objects.equals(fIsrecheck, that.fIsrecheck) &&
                Objects.equals(fSettlenone, that.fSettlenone) &&
                Objects.equals(fGuidancereturned, that.fGuidancereturned) &&
                Objects.equals(dateguidancereturned, that.dateguidancereturned) &&
                Objects.equals(idGuidancereturnedby, that.idGuidancereturnedby) &&
                Objects.equals(fOutpatient, that.fOutpatient) &&
                Objects.equals(patientnamereceipt, that.patientnamereceipt) &&
                Objects.equals(patientnamepinyin, that.patientnamepinyin) &&
                Objects.equals(fForpreparefinancialconfirm, that.fForpreparefinancialconfirm) &&
                Objects.equals(statusofhm, that.statusofhm) &&
                Objects.equals(instancetag, that.instancetag) &&
                Objects.equals(keybirthplace, that.keybirthplace) &&
                Objects.equals(keybloodtype, that.keybloodtype) &&
                Objects.equals(exammethod, that.exammethod) &&
                Objects.equals(inpatientno, that.inpatientno) &&
                Objects.equals(insuranceno, that.insuranceno) &&
                Objects.equals(keypayway, that.keypayway) &&
                Objects.equals(healthcard, that.healthcard) &&
                Objects.equals(exampoint, that.exampoint) &&
                Objects.equals(fingerprint, that.fingerprint) &&
                Objects.equals(countreportcoverprinted, that.countreportcoverprinted) &&
                Objects.equals(countreportprinted, that.countreportprinted) &&
                Objects.equals(countreportpdf, that.countreportpdf) &&
                Objects.equals(countreportword, that.countreportword) &&
                Objects.equals(countreportxml, that.countreportxml) &&
                Objects.equals(countreportcompare, that.countreportcompare) &&
                Objects.equals(countreportcomparepdf, that.countreportcomparepdf) &&
                Objects.equals(countreportcompareword, that.countreportcompareword) &&
                Objects.equals(countreportcomparexml, that.countreportcomparexml) &&
                Objects.equals(countreportoccupation, that.countreportoccupation) &&
                Objects.equals(countreportoccupationpdf, that.countreportoccupationpdf) &&
                Objects.equals(countreportoccupationword, that.countreportoccupationword) &&
                Objects.equals(countreportoccupationxml, that.countreportoccupationxml) &&
                Objects.equals(scbs, that.scbs) &&
                Objects.equals(idTjtc, that.idTjtc) &&
                Objects.equals(jzdw, that.jzdw) &&
                Objects.equals(jzdwr, that.jzdwr) &&
                Objects.equals(spr, that.spr) &&
                Objects.equals(tjr, that.tjr) &&
                Objects.equals(lqfs, that.lqfs) &&
                Objects.equals(yzbm, that.yzbm) &&
                Objects.equals(yjaddress, that.yjaddress) &&
                Objects.equals(qtxz, that.qtxz) &&
                Objects.equals(isHmdb, that.isHmdb) &&
                Objects.equals(isHmd, that.isHmd) &&
                Objects.equals(isjj, that.isjj) &&
                Objects.equals(zgl, that.zgl) &&
                Objects.equals(jhgl, that.jhgl) &&
                Objects.equals(jhys, that.jhys) &&
                Objects.equals(jktjzt, that.jktjzt) &&
                Objects.equals(zytjzt, that.zytjzt) &&
                Objects.equals(tmyd, that.tmyd) &&
                Objects.equals(id, that.id) &&
                Objects.equals(medicaldate, that.medicaldate) &&
                Objects.equals(trades, that.trades) &&
                Objects.equals(cjjgsfyhf, that.cjjgsfyhf) &&
                Objects.equals(bhgybsfyhf, that.bhgybsfyhf) &&
                Objects.equals(yxjgsfyhf, that.yxjgsfyhf) &&
                Objects.equals(medicaltype, that.medicaltype) &&
                Objects.equals(prepayment, that.prepayment) &&
                Objects.equals(tcprice, that.tcprice) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(cultural, that.cultural) &&
                Objects.equals(everOfDisease, that.everOfDisease) &&
                Objects.equals(ccnl, that.ccnl) &&
                Objects.equals(jq, that.jq) &&
                Objects.equals(zq, that.zq) &&
                Objects.equals(tjnl, that.tjnl) &&
                Objects.equals(familyNumber, that.familyNumber) &&
                Objects.equals(zc, that.zc) &&
                Objects.equals(sc, that.sc) &&
                Objects.equals(lc, that.lc) &&
                Objects.equals(jt, that.jt) &&
                Objects.equals(ywrc, that.ywrc) &&
                Objects.equals(abstainSmokeNote, that.abstainSmokeNote) &&
                Objects.equals(everydaySmokeN, that.everydaySmokeN) &&
                Objects.equals(smokeYear, that.smokeYear) &&
                Objects.equals(noKissTheCup, that.noKissTheCup) &&
                Objects.equals(betweenKissTheCup, that.betweenKissTheCup) &&
                Objects.equals(evermoreKiss, that.evermoreKiss) &&
                Objects.equals(abstainLostKiss, that.abstainLostKiss) &&
                Objects.equals(kissYearN, that.kissYearN) &&
                Objects.equals(kissAmount, that.kissAmount) &&
                Objects.equals(kissType, that.kissType) &&
                Objects.equals(familyOfDisease, that.familyOfDisease) &&
                Objects.equals(symptom, that.symptom) &&
                Objects.equals(isAudit, that.isAudit) &&
                Objects.equals(everOfDiseaseRemark, that.everOfDiseaseRemark) &&
                Objects.equals(createReportNum, that.createReportNum) &&
                Objects.equals(workDate, that.workDate) &&
                Objects.equals(harmDate, that.harmDate) &&
                Objects.equals(picture, that.picture) &&
                Objects.equals(advice, that.advice) &&
                Objects.equals(diseasePrintNum, that.diseasePrintNum) &&
                Objects.equals(healthPrintNum, that.healthPrintNum) &&
                Objects.equals(readytofinalDate, that.readytofinalDate) &&
                Objects.equals(guideSignleCount, that.guideSignleCount) &&
                Objects.equals(shortCode, that.shortCode) &&
                Objects.equals(isNoticed, that.isNoticed) &&
                Objects.equals(reviewPdf, that.reviewPdf) &&
                Objects.equals(contraindicatedPdf, that.contraindicatedPdf) &&
                Objects.equals(diseasePdf, that.diseasePdf) &&
                Objects.equals(signPicture, that.signPicture) &&
                Objects.equals(isNewPacs, that.isNewPacs) &&
                Objects.equals(tsLimit, that.tsLimit) &&
                Objects.equals(physique, that.physique) &&
                Objects.equals(docName, that.docName) &&
                Objects.equals(committee, that.committee) &&
                Objects.equals(street, that.street);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPatient, idOrgpatient, idCis, idPatientarchive, patientcode, patientcodeprn, patientarchiveno, patientcardno, patientbizno, idcardno, dailynumberdate, dailynumber, numtotal, numyear, nummonth, numday, numorg, numorgresv, idPatientlinked, idNonorg, patientname, inputCode, idOrgreservationgroup, idOrgreservation, idOrg, orgName, orgDepart, orgDepartsuba, orgDepartsubb, orgDepartsubc, orgDepartsubd, orgDepartsube, idFeetype, idPayway, payway, offpercent, maxoffpercent, personpricelimit, idSex, sex, birthdate, age, idAgeunit, ageunit, ageofreal, idMarriage, marriage, idNation, nation, address, idInformway, idOpendoctor, email, phone, idPatientclass, idEducation, education, idOccupation, occupation, idResarea, resarea, dateinorganization, fIsforprepare, fIsforreserve, datecreated, fRegistered, dateregister, positionCode, jobtypeCode, moneyamount, moneyamountpaid, guidancenote, workno, idDoctorreg, doctorreg, idExamtype, idExamsuite, examsuiteName, examsuiteAlias, idDoctorconclusion, doctorconclusion, idDoctorapply, doctorapply, fGuidanceprinted, fFeecharged, fExamstarted, fReadytofinal, idDoctorfee, doctorfee, fPaused, fFinallocked, fFinalexamed, fFinalapproved, idDoctorfinal, doctorfinalNameR, datefinalexamed, idDoctorfinalapproved, datefinalapproved, fCardissued, fCardreturned, fCoverprinted, fReportprinted, idReportprintedby, datereportprinted, fReportinformed, datereportinformed, fReportfetched, datereportfetched, fIssevere, fClosed, dateclosed, fNeedtraced, fDiffperson, confidentiallevel, fSettleall, signature, note, dtLastmodifiedthisat, fInneroper, severedegree, severedegreenote, fSevereinformed, severeinformtime, idSevereinformby, conclusion, conclusionsummary, suggestion, conclusionrich, dietguide, sportguide, knowledge, message, positivesummary, resultcompare, interfacemarks, patientflag, timingstartedat, timeresultlastchange, timeresultlastarchive, timeresultlastolap, hospitalcode, hospitalname, idExamplace, parsedassignedsuiteandfi, parsedassignedgroupandfi, parsedsuiteandfi, parsedsuiteandfilab, idGuidenurse, patientnameencoded, patientcodehiden, fPdfcreated, fWordcreated, fWordprinted, guidancenote2, fUsecodehiden, idPatientclass2, idPatientclass3, dateregisternotime, counterreportprinted, fPrintcomparingreport, fIsrecheck, fSettlenone, fGuidancereturned, dateguidancereturned, idGuidancereturnedby, fOutpatient, patientnamereceipt, patientnamepinyin, fForpreparefinancialconfirm, statusofhm, instancetag, keybirthplace, keybloodtype, exammethod, inpatientno, insuranceno, keypayway, healthcard, exampoint, fingerprint, countreportcoverprinted, countreportprinted, countreportpdf, countreportword, countreportxml, countreportcompare, countreportcomparepdf, countreportcompareword, countreportcomparexml, countreportoccupation, countreportoccupationpdf, countreportoccupationword, countreportoccupationxml, scbs, idTjtc, jzdw, jzdwr, spr, tjr, lqfs, yzbm, yjaddress, qtxz, isHmdb, isHmd, isjj, zgl, jhgl, jhys, jktjzt, zytjzt, tmyd, id, medicaldate, trades, cjjgsfyhf, bhgybsfyhf, yxjgsfyhf, medicaltype, prepayment, tcprice, createdate, modifydate, cultural, everOfDisease, ccnl, jq, zq, tjnl, familyNumber, zc, sc, lc, jt, ywrc, abstainSmokeNote, everydaySmokeN, smokeYear, noKissTheCup, betweenKissTheCup, evermoreKiss, abstainLostKiss, kissYearN, kissAmount, kissType, familyOfDisease, symptom, isAudit, everOfDiseaseRemark, createReportNum, workDate, harmDate, picture, advice, diseasePrintNum, healthPrintNum, readytofinalDate, guideSignleCount, shortCode, isNoticed, reviewPdf, contraindicatedPdf, diseasePdf, signPicture, isNewPacs, tsLimit, physique, docName, committee, street);
    }
}