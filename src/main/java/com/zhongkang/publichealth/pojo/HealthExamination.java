package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-08-06 15:39
 */
@Entity
@Table(name = "HEALTH_EXAMINATION")
public class HealthExamination {
    private String id;
    private String creater;
    private Date createdate;
    private String modifyer;
    private Date modifydate;
    private String olderid;
    private String idnum;
    private Date examdate;
    private String doctor;
    private String symptom;
    private Long temperature;
    private Long pulserate;
    private Integer breathrate;
    private Integer leftbldpressure;
    private Integer rightbldpressure;
    private Long height;
    private Long weight;
    private Long waistline;
    private Long bodyindex;
    private Integer olderZwpg;
    private Integer olderZlnl;
    private String olderRzgn;
    private String olderQgzt;
    private Integer sportrate;
    private Integer pertimesport;
    private Double sporttime;
    private String sporttype;
    private String eatstate;
    private Integer smokestate;
    private Integer perdaysmoke;
    private Integer smokestart;
    private Integer smokeend;
    private Integer drinkstate;
    private Long perdaydrink;
    private Integer quietdrink;
    private Integer startdrink;
    private Integer drunked;
    private String drinktype;
    private String danger;
    private Integer kqKc;
    private String kqCl;
    private Integer kqYb;
    private Long lefteye;
    private Long righteye;
    private Long jzLefteye;
    private Long jzRighteye;
    private Integer tl;
    private Integer sportability;
    private Integer eyebottom;
    private Integer skin;
    private Integer gm;
    private Integer lbj;
    private Integer tzx;
    private Integer hxy;
    private Integer ly;
    private Long heartrate;
    private Integer heartrhythm;
    private Integer othervoice;
    private Integer fbYt;
    private Integer fbBk;
    private Integer fbGd;
    private Integer fbPd;
    private Integer fbZy;
    private Integer xzsz;
    private Integer zbdmbd;
    private Integer gmzz;
    private String rx;
    private Integer fkWy;
    private Integer fkYd;
    private Integer fkGj;
    private Integer fkGt;
    private Integer fkFj;
    private String ctOther;
    private String xhdb;
    private String bxb;
    private String xxb;
    private String xcgOther;
    private String ndb;
    private String nt;
    private String ntt;
    private String nqx;
    private String ncgOther;
    private String kfxt;
    private Integer xdt;
    private Long nwlbdb;
    private Integer dbqx;
    private Long thxhdb;
    private Integer gybmky;
    private Long xqgbzam;
    private Long xqgczam;
    private Long bdb;
    private Long zdhs;
    private Long jhdhs;
    private Long xqjg;
    private Long xns;
    private Long xjnd;
    private Long xnnd;
    private Long zdgc;
    private Long gysz;
    private Long xqdmdzdb;
    private Long xqgmdzdb;
    private Integer xbxxp;
    private Integer fbbc;
    private Integer bcOther;
    private Integer gjtp;
    private String fzjcOther;
    private String nxgjb;
    private String nxgjbOther;
    private String szjb;
    private String szjbOther;
    private String xzjb;
    private String xzjbOther;
    private String xgjb;
    private String ybjb;
    private String ybjbOther;
    private Integer jkpj;
    private String jkzd;
    private String wxkz;
    private Integer quietdrinkage;
    private String dangertype;
    private String xgjbOther;
    private String jkpjYc;
    private String jkzdRemarks;
    private Integer leftlowpressure;
    private Integer rightlowpressure;
    private String xdtfx;
    private Long bloodoxygen;
    private String bloodlustate;
    private Date bloodglutime;
    private String tempter;
    private String codenum;
    private String patientcode;
    private String olderRzgnFen;
    private String olderQgztFen;
    private String sjxtjbOther;
    private String qtxtjbOther;
    private String drinkTypeOther;
    private String workerType;
    private String workDate;
    private String fenchen;
    private String fchcs;
    private String fchy;
    private String shexian;
    private String sxfhcs;
    private String sxfhcsqt;
    private String wuli;
    private String wlcs;
    private String wly;
    private String huaxue;
    private String hxpfhcs;
    private String hxpfhcsjt;
    private String gmqt;
    private String blqita;
    private String blqtcs;
    private Long qty;
    private String lbjqt;
    private String lyqt;
    private String fbYtDesc;
    private String fbBkDesc;
    private String fbGdDesc;
    private String fbPdDesc;
    private String skinOther;
    private String fbZyDesc;
    private String xdtqt;
    private String otherVoiceDesc;
    private String hxyYc;
    private Long txbgas;
    private Integer isAudit;
    private String xdtConclusions;
    private Integer eatNum;
    private Integer childNum;
    private Long saltNum;
    private Long soyNum;
    private Long loseWeight;
    private String fbbcConclusions;
    private String sjxtjb;
    private String qtxtjb;
    private Date rummagerDate;
    private String rummagername;
    private String wxkzqt;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CREATER", nullable = true, length = 32)
    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = true)
    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYER", nullable = true, length = 32)
    public String getModifyer() {
        return modifyer;
    }

    public void setModifyer(String modifyer) {
        this.modifyer = modifyer;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = true)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "OLDERID", nullable = true, length = 32)
    public String getOlderid() {
        return olderid;
    }

    public void setOlderid(String olderid) {
        this.olderid = olderid;
    }

    @Basic
    @Column(name = "IDNUM", nullable = true, length = 12)
    public String getIdnum() {
        return idnum;
    }

    public void setIdnum(String idnum) {
        this.idnum = idnum;
    }

    @Basic
    @Column(name = "EXAMDATE", nullable = true)
    public Date getExamdate() {
        return examdate;
    }

    public void setExamdate(Date examdate) {
        this.examdate = examdate;
    }

    @Basic
    @Column(name = "DOCTOR", nullable = true, length = 32)
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Basic
    @Column(name = "SYMPTOM", nullable = true, length = 100)
    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    @Basic
    @Column(name = "TEMPERATURE", nullable = true, precision = 1)
    public Long getTemperature() {
        return temperature;
    }

    public void setTemperature(Long temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "PULSERATE", nullable = true, precision = 0)
    public Long getPulserate() {
        return pulserate;
    }

    public void setPulserate(Long pulserate) {
        this.pulserate = pulserate;
    }

    @Basic
    @Column(name = "BREATHRATE", nullable = true, precision = 0)
    public Integer getBreathrate() {
        return breathrate;
    }

    public void setBreathrate(Integer breathrate) {
        this.breathrate = breathrate;
    }

    @Basic
    @Column(name = "LEFTBLDPRESSURE", nullable = true, precision = 0)
    public Integer getLeftbldpressure() {
        return leftbldpressure;
    }

    public void setLeftbldpressure(Integer leftbldpressure) {
        this.leftbldpressure = leftbldpressure;
    }

    @Basic
    @Column(name = "RIGHTBLDPRESSURE", nullable = true, precision = 0)
    public Integer getRightbldpressure() {
        return rightbldpressure;
    }

    public void setRightbldpressure(Integer rightbldpressure) {
        this.rightbldpressure = rightbldpressure;
    }

    @Basic
    @Column(name = "HEIGHT", nullable = true, precision = 1)
    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    @Basic
    @Column(name = "WEIGHT", nullable = true, precision = 1)
    public Long getWeight() {
        return weight;
    }

    public void setWeight(Long weight) {
        this.weight = weight;
    }

    @Basic
    @Column(name = "WAISTLINE", nullable = true, precision = 1)
    public Long getWaistline() {
        return waistline;
    }

    public void setWaistline(Long waistline) {
        this.waistline = waistline;
    }

    @Basic
    @Column(name = "BODYINDEX", nullable = true, precision = 1)
    public Long getBodyindex() {
        return bodyindex;
    }

    public void setBodyindex(Long bodyindex) {
        this.bodyindex = bodyindex;
    }

    @Basic
    @Column(name = "OLDER_ZWPG", nullable = true, precision = 0)
    public Integer getOlderZwpg() {
        return olderZwpg;
    }

    public void setOlderZwpg(Integer olderZwpg) {
        this.olderZwpg = olderZwpg;
    }

    @Basic
    @Column(name = "OLDER_ZLNL", nullable = true, precision = 0)
    public Integer getOlderZlnl() {
        return olderZlnl;
    }

    public void setOlderZlnl(Integer olderZlnl) {
        this.olderZlnl = olderZlnl;
    }

    @Basic
    @Column(name = "OLDER_RZGN", nullable = true, length = 32)
    public String getOlderRzgn() {
        return olderRzgn;
    }

    public void setOlderRzgn(String olderRzgn) {
        this.olderRzgn = olderRzgn;
    }

    @Basic
    @Column(name = "OLDER_QGZT", nullable = true, length = 32)
    public String getOlderQgzt() {
        return olderQgzt;
    }

    public void setOlderQgzt(String olderQgzt) {
        this.olderQgzt = olderQgzt;
    }

    @Basic
    @Column(name = "SPORTRATE", nullable = true, precision = 0)
    public Integer getSportrate() {
        return sportrate;
    }

    public void setSportrate(Integer sportrate) {
        this.sportrate = sportrate;
    }

    @Basic
    @Column(name = "PERTIMESPORT", nullable = true, precision = 0)
    public Integer getPertimesport() {
        return pertimesport;
    }

    public void setPertimesport(Integer pertimesport) {
        this.pertimesport = pertimesport;
    }

    @Basic
    @Column(name = "SPORTTIME", nullable = true, precision = 0)
    public Double getSporttime() {
        return sporttime;
    }

    public void setSporttime(Double sporttime) {
        this.sporttime = sporttime;
    }

    @Basic
    @Column(name = "SPORTTYPE", nullable = true, length = 32)
    public String getSporttype() {
        return sporttype;
    }

    public void setSporttype(String sporttype) {
        this.sporttype = sporttype;
    }

    @Basic
    @Column(name = "EATSTATE", nullable = true, length = 12)
    public String getEatstate() {
        return eatstate;
    }

    public void setEatstate(String eatstate) {
        this.eatstate = eatstate;
    }

    @Basic
    @Column(name = "SMOKESTATE", nullable = true, precision = 0)
    public Integer getSmokestate() {
        return smokestate;
    }

    public void setSmokestate(Integer smokestate) {
        this.smokestate = smokestate;
    }

    @Basic
    @Column(name = "PERDAYSMOKE", nullable = true, precision = 0)
    public Integer getPerdaysmoke() {
        return perdaysmoke;
    }

    public void setPerdaysmoke(Integer perdaysmoke) {
        this.perdaysmoke = perdaysmoke;
    }

    @Basic
    @Column(name = "SMOKESTART", nullable = true, precision = 0)
    public Integer getSmokestart() {
        return smokestart;
    }

    public void setSmokestart(Integer smokestart) {
        this.smokestart = smokestart;
    }

    @Basic
    @Column(name = "SMOKEEND", nullable = true, precision = 0)
    public Integer getSmokeend() {
        return smokeend;
    }

    public void setSmokeend(Integer smokeend) {
        this.smokeend = smokeend;
    }

    @Basic
    @Column(name = "DRINKSTATE", nullable = true, precision = 0)
    public Integer getDrinkstate() {
        return drinkstate;
    }

    public void setDrinkstate(Integer drinkstate) {
        this.drinkstate = drinkstate;
    }

    @Basic
    @Column(name = "PERDAYDRINK", nullable = true, precision = 1)
    public Long getPerdaydrink() {
        return perdaydrink;
    }

    public void setPerdaydrink(Long perdaydrink) {
        this.perdaydrink = perdaydrink;
    }

    @Basic
    @Column(name = "QUIETDRINK", nullable = true, precision = 0)
    public Integer getQuietdrink() {
        return quietdrink;
    }

    public void setQuietdrink(Integer quietdrink) {
        this.quietdrink = quietdrink;
    }

    @Basic
    @Column(name = "STARTDRINK", nullable = true, precision = 0)
    public Integer getStartdrink() {
        return startdrink;
    }

    public void setStartdrink(Integer startdrink) {
        this.startdrink = startdrink;
    }

    @Basic
    @Column(name = "DRUNKED", nullable = true, precision = 0)
    public Integer getDrunked() {
        return drunked;
    }

    public void setDrunked(Integer drunked) {
        this.drunked = drunked;
    }

    @Basic
    @Column(name = "DRINKTYPE", nullable = true, length = 10)
    public String getDrinktype() {
        return drinktype;
    }

    public void setDrinktype(String drinktype) {
        this.drinktype = drinktype;
    }

    @Basic
    @Column(name = "DANGER", nullable = true, length = 255)
    public String getDanger() {
        return danger;
    }

    public void setDanger(String danger) {
        this.danger = danger;
    }

    @Basic
    @Column(name = "KQ_KC", nullable = true, precision = 0)
    public Integer getKqKc() {
        return kqKc;
    }

    public void setKqKc(Integer kqKc) {
        this.kqKc = kqKc;
    }

    @Basic
    @Column(name = "KQ_CL", nullable = true, length = 10)
    public String getKqCl() {
        return kqCl;
    }

    public void setKqCl(String kqCl) {
        this.kqCl = kqCl;
    }

    @Basic
    @Column(name = "KQ_YB", nullable = true, precision = 0)
    public Integer getKqYb() {
        return kqYb;
    }

    public void setKqYb(Integer kqYb) {
        this.kqYb = kqYb;
    }

    @Basic
    @Column(name = "LEFTEYE", nullable = true, precision = 1)
    public Long getLefteye() {
        return lefteye;
    }

    public void setLefteye(Long lefteye) {
        this.lefteye = lefteye;
    }

    @Basic
    @Column(name = "RIGHTEYE", nullable = true, precision = 1)
    public Long getRighteye() {
        return righteye;
    }

    public void setRighteye(Long righteye) {
        this.righteye = righteye;
    }

    @Basic
    @Column(name = "JZ_LEFTEYE", nullable = true, precision = 1)
    public Long getJzLefteye() {
        return jzLefteye;
    }

    public void setJzLefteye(Long jzLefteye) {
        this.jzLefteye = jzLefteye;
    }

    @Basic
    @Column(name = "JZ_RIGHTEYE", nullable = true, precision = 1)
    public Long getJzRighteye() {
        return jzRighteye;
    }

    public void setJzRighteye(Long jzRighteye) {
        this.jzRighteye = jzRighteye;
    }

    @Basic
    @Column(name = "TL", nullable = true, precision = 0)
    public Integer getTl() {
        return tl;
    }

    public void setTl(Integer tl) {
        this.tl = tl;
    }

    @Basic
    @Column(name = "SPORTABILITY", nullable = true, precision = 0)
    public Integer getSportability() {
        return sportability;
    }

    public void setSportability(Integer sportability) {
        this.sportability = sportability;
    }

    @Basic
    @Column(name = "EYEBOTTOM", nullable = true, precision = 0)
    public Integer getEyebottom() {
        return eyebottom;
    }

    public void setEyebottom(Integer eyebottom) {
        this.eyebottom = eyebottom;
    }

    @Basic
    @Column(name = "SKIN", nullable = true, precision = 0)
    public Integer getSkin() {
        return skin;
    }

    public void setSkin(Integer skin) {
        this.skin = skin;
    }

    @Basic
    @Column(name = "GM", nullable = true, precision = 0)
    public Integer getGm() {
        return gm;
    }

    public void setGm(Integer gm) {
        this.gm = gm;
    }

    @Basic
    @Column(name = "LBJ", nullable = true, precision = 0)
    public Integer getLbj() {
        return lbj;
    }

    public void setLbj(Integer lbj) {
        this.lbj = lbj;
    }

    @Basic
    @Column(name = "TZX", nullable = true, precision = 0)
    public Integer getTzx() {
        return tzx;
    }

    public void setTzx(Integer tzx) {
        this.tzx = tzx;
    }

    @Basic
    @Column(name = "HXY", nullable = true, precision = 0)
    public Integer getHxy() {
        return hxy;
    }

    public void setHxy(Integer hxy) {
        this.hxy = hxy;
    }

    @Basic
    @Column(name = "LY", nullable = true, precision = 0)
    public Integer getLy() {
        return ly;
    }

    public void setLy(Integer ly) {
        this.ly = ly;
    }

    @Basic
    @Column(name = "HEARTRATE", nullable = true, precision = 0)
    public Long getHeartrate() {
        return heartrate;
    }

    public void setHeartrate(Long heartrate) {
        this.heartrate = heartrate;
    }

    @Basic
    @Column(name = "HEARTRHYTHM", nullable = true, precision = 0)
    public Integer getHeartrhythm() {
        return heartrhythm;
    }

    public void setHeartrhythm(Integer heartrhythm) {
        this.heartrhythm = heartrhythm;
    }

    @Basic
    @Column(name = "OTHERVOICE", nullable = true, precision = 0)
    public Integer getOthervoice() {
        return othervoice;
    }

    public void setOthervoice(Integer othervoice) {
        this.othervoice = othervoice;
    }

    @Basic
    @Column(name = "FB_YT", nullable = true, precision = 0)
    public Integer getFbYt() {
        return fbYt;
    }

    public void setFbYt(Integer fbYt) {
        this.fbYt = fbYt;
    }

    @Basic
    @Column(name = "FB_BK", nullable = true, precision = 0)
    public Integer getFbBk() {
        return fbBk;
    }

    public void setFbBk(Integer fbBk) {
        this.fbBk = fbBk;
    }

    @Basic
    @Column(name = "FB_GD", nullable = true, precision = 0)
    public Integer getFbGd() {
        return fbGd;
    }

    public void setFbGd(Integer fbGd) {
        this.fbGd = fbGd;
    }

    @Basic
    @Column(name = "FB_PD", nullable = true, precision = 0)
    public Integer getFbPd() {
        return fbPd;
    }

    public void setFbPd(Integer fbPd) {
        this.fbPd = fbPd;
    }

    @Basic
    @Column(name = "FB_ZY", nullable = true, precision = 0)
    public Integer getFbZy() {
        return fbZy;
    }

    public void setFbZy(Integer fbZy) {
        this.fbZy = fbZy;
    }

    @Basic
    @Column(name = "XZSZ", nullable = true, precision = 0)
    public Integer getXzsz() {
        return xzsz;
    }

    public void setXzsz(Integer xzsz) {
        this.xzsz = xzsz;
    }

    @Basic
    @Column(name = "ZBDMBD", nullable = true, precision = 0)
    public Integer getZbdmbd() {
        return zbdmbd;
    }

    public void setZbdmbd(Integer zbdmbd) {
        this.zbdmbd = zbdmbd;
    }

    @Basic
    @Column(name = "GMZZ", nullable = true, precision = 0)
    public Integer getGmzz() {
        return gmzz;
    }

    public void setGmzz(Integer gmzz) {
        this.gmzz = gmzz;
    }

    @Basic
    @Column(name = "RX", nullable = true, length = 10)
    public String getRx() {
        return rx;
    }

    public void setRx(String rx) {
        this.rx = rx;
    }

    @Basic
    @Column(name = "FK_WY", nullable = true, precision = 0)
    public Integer getFkWy() {
        return fkWy;
    }

    public void setFkWy(Integer fkWy) {
        this.fkWy = fkWy;
    }

    @Basic
    @Column(name = "FK_YD", nullable = true, precision = 0)
    public Integer getFkYd() {
        return fkYd;
    }

    public void setFkYd(Integer fkYd) {
        this.fkYd = fkYd;
    }

    @Basic
    @Column(name = "FK_GJ", nullable = true, precision = 0)
    public Integer getFkGj() {
        return fkGj;
    }

    public void setFkGj(Integer fkGj) {
        this.fkGj = fkGj;
    }

    @Basic
    @Column(name = "FK_GT", nullable = true, precision = 0)
    public Integer getFkGt() {
        return fkGt;
    }

    public void setFkGt(Integer fkGt) {
        this.fkGt = fkGt;
    }

    @Basic
    @Column(name = "FK_FJ", nullable = true, precision = 0)
    public Integer getFkFj() {
        return fkFj;
    }

    public void setFkFj(Integer fkFj) {
        this.fkFj = fkFj;
    }

    @Basic
    @Column(name = "CT_OTHER", nullable = true, length = 255)
    public String getCtOther() {
        return ctOther;
    }

    public void setCtOther(String ctOther) {
        this.ctOther = ctOther;
    }

    @Basic
    @Column(name = "XHDB", nullable = true, length = 60)
    public String getXhdb() {
        return xhdb;
    }

    public void setXhdb(String xhdb) {
        this.xhdb = xhdb;
    }

    @Basic
    @Column(name = "BXB", nullable = true, length = 60)
    public String getBxb() {
        return bxb;
    }

    public void setBxb(String bxb) {
        this.bxb = bxb;
    }

    @Basic
    @Column(name = "XXB", nullable = true, length = 60)
    public String getXxb() {
        return xxb;
    }

    public void setXxb(String xxb) {
        this.xxb = xxb;
    }

    @Basic
    @Column(name = "XCG_OTHER", nullable = true, length = 255)
    public String getXcgOther() {
        return xcgOther;
    }

    public void setXcgOther(String xcgOther) {
        this.xcgOther = xcgOther;
    }

    @Basic
    @Column(name = "NDB", nullable = true, length = 32)
    public String getNdb() {
        return ndb;
    }

    public void setNdb(String ndb) {
        this.ndb = ndb;
    }

    @Basic
    @Column(name = "NT", nullable = true, length = 32)
    public String getNt() {
        return nt;
    }

    public void setNt(String nt) {
        this.nt = nt;
    }

    @Basic
    @Column(name = "NTT", nullable = true, length = 32)
    public String getNtt() {
        return ntt;
    }

    public void setNtt(String ntt) {
        this.ntt = ntt;
    }

    @Basic
    @Column(name = "NQX", nullable = true, length = 32)
    public String getNqx() {
        return nqx;
    }

    public void setNqx(String nqx) {
        this.nqx = nqx;
    }

    @Basic
    @Column(name = "NCG_OTHER", nullable = true, length = 255)
    public String getNcgOther() {
        return ncgOther;
    }

    public void setNcgOther(String ncgOther) {
        this.ncgOther = ncgOther;
    }

    @Basic
    @Column(name = "KFXT", nullable = true, length = 32)
    public String getKfxt() {
        return kfxt;
    }

    public void setKfxt(String kfxt) {
        this.kfxt = kfxt;
    }

    @Basic
    @Column(name = "XDT", nullable = true, precision = 0)
    public Integer getXdt() {
        return xdt;
    }

    public void setXdt(Integer xdt) {
        this.xdt = xdt;
    }

    @Basic
    @Column(name = "NWLBDB", nullable = true, precision = 2)
    public Long getNwlbdb() {
        return nwlbdb;
    }

    public void setNwlbdb(Long nwlbdb) {
        this.nwlbdb = nwlbdb;
    }

    @Basic
    @Column(name = "DBQX", nullable = true, precision = 0)
    public Integer getDbqx() {
        return dbqx;
    }

    public void setDbqx(Integer dbqx) {
        this.dbqx = dbqx;
    }

    @Basic
    @Column(name = "THXHDB", nullable = true, precision = 1)
    public Long getThxhdb() {
        return thxhdb;
    }

    public void setThxhdb(Long thxhdb) {
        this.thxhdb = thxhdb;
    }

    @Basic
    @Column(name = "GYBMKY", nullable = true, precision = 0)
    public Integer getGybmky() {
        return gybmky;
    }

    public void setGybmky(Integer gybmky) {
        this.gybmky = gybmky;
    }

    @Basic
    @Column(name = "XQGBZAM", nullable = true, precision = 0)
    public Long getXqgbzam() {
        return xqgbzam;
    }

    public void setXqgbzam(Long xqgbzam) {
        this.xqgbzam = xqgbzam;
    }

    @Basic
    @Column(name = "XQGCZAM", nullable = true, precision = 0)
    public Long getXqgczam() {
        return xqgczam;
    }

    public void setXqgczam(Long xqgczam) {
        this.xqgczam = xqgczam;
    }

    @Basic
    @Column(name = "BDB", nullable = true, precision = 0)
    public Long getBdb() {
        return bdb;
    }

    public void setBdb(Long bdb) {
        this.bdb = bdb;
    }

    @Basic
    @Column(name = "ZDHS", nullable = true, precision = 0)
    public Long getZdhs() {
        return zdhs;
    }

    public void setZdhs(Long zdhs) {
        this.zdhs = zdhs;
    }

    @Basic
    @Column(name = "JHDHS", nullable = true, precision = 0)
    public Long getJhdhs() {
        return jhdhs;
    }

    public void setJhdhs(Long jhdhs) {
        this.jhdhs = jhdhs;
    }

    @Basic
    @Column(name = "XQJG", nullable = true, precision = 0)
    public Long getXqjg() {
        return xqjg;
    }

    public void setXqjg(Long xqjg) {
        this.xqjg = xqjg;
    }

    @Basic
    @Column(name = "XNS", nullable = true, precision = 0)
    public Long getXns() {
        return xns;
    }

    public void setXns(Long xns) {
        this.xns = xns;
    }

    @Basic
    @Column(name = "XJND", nullable = true, precision = 0)
    public Long getXjnd() {
        return xjnd;
    }

    public void setXjnd(Long xjnd) {
        this.xjnd = xjnd;
    }

    @Basic
    @Column(name = "XNND", nullable = true, precision = 0)
    public Long getXnnd() {
        return xnnd;
    }

    public void setXnnd(Long xnnd) {
        this.xnnd = xnnd;
    }

    @Basic
    @Column(name = "ZDGC", nullable = true, precision = 0)
    public Long getZdgc() {
        return zdgc;
    }

    public void setZdgc(Long zdgc) {
        this.zdgc = zdgc;
    }

    @Basic
    @Column(name = "GYSZ", nullable = true, precision = 0)
    public Long getGysz() {
        return gysz;
    }

    public void setGysz(Long gysz) {
        this.gysz = gysz;
    }

    @Basic
    @Column(name = "XQDMDZDB", nullable = true, precision = 0)
    public Long getXqdmdzdb() {
        return xqdmdzdb;
    }

    public void setXqdmdzdb(Long xqdmdzdb) {
        this.xqdmdzdb = xqdmdzdb;
    }

    @Basic
    @Column(name = "XQGMDZDB", nullable = true, precision = 0)
    public Long getXqgmdzdb() {
        return xqgmdzdb;
    }

    public void setXqgmdzdb(Long xqgmdzdb) {
        this.xqgmdzdb = xqgmdzdb;
    }

    @Basic
    @Column(name = "XBXXP", nullable = true, precision = 0)
    public Integer getXbxxp() {
        return xbxxp;
    }

    public void setXbxxp(Integer xbxxp) {
        this.xbxxp = xbxxp;
    }

    @Basic
    @Column(name = "FBBC", nullable = true, precision = 0)
    public Integer getFbbc() {
        return fbbc;
    }

    public void setFbbc(Integer fbbc) {
        this.fbbc = fbbc;
    }

    @Basic
    @Column(name = "BC_OTHER", nullable = true, precision = 0)
    public Integer getBcOther() {
        return bcOther;
    }

    public void setBcOther(Integer bcOther) {
        this.bcOther = bcOther;
    }

    @Basic
    @Column(name = "GJTP", nullable = true, precision = 0)
    public Integer getGjtp() {
        return gjtp;
    }

    public void setGjtp(Integer gjtp) {
        this.gjtp = gjtp;
    }

    @Basic
    @Column(name = "FZJC_OTHER", nullable = true, length = 255)
    public String getFzjcOther() {
        return fzjcOther;
    }

    public void setFzjcOther(String fzjcOther) {
        this.fzjcOther = fzjcOther;
    }

    @Basic
    @Column(name = "NXGJB", nullable = true, length = 10)
    public String getNxgjb() {
        return nxgjb;
    }

    public void setNxgjb(String nxgjb) {
        this.nxgjb = nxgjb;
    }

    @Basic
    @Column(name = "NXGJB_OTHER", nullable = true, length = 255)
    public String getNxgjbOther() {
        return nxgjbOther;
    }

    public void setNxgjbOther(String nxgjbOther) {
        this.nxgjbOther = nxgjbOther;
    }

    @Basic
    @Column(name = "SZJB", nullable = true, length = 10)
    public String getSzjb() {
        return szjb;
    }

    public void setSzjb(String szjb) {
        this.szjb = szjb;
    }

    @Basic
    @Column(name = "SZJB_OTHER", nullable = true, length = 255)
    public String getSzjbOther() {
        return szjbOther;
    }

    public void setSzjbOther(String szjbOther) {
        this.szjbOther = szjbOther;
    }

    @Basic
    @Column(name = "XZJB", nullable = true, length = 20)
    public String getXzjb() {
        return xzjb;
    }

    public void setXzjb(String xzjb) {
        this.xzjb = xzjb;
    }

    @Basic
    @Column(name = "XZJB_OTHER", nullable = true, length = 255)
    public String getXzjbOther() {
        return xzjbOther;
    }

    public void setXzjbOther(String xzjbOther) {
        this.xzjbOther = xzjbOther;
    }

    @Basic
    @Column(name = "XGJB", nullable = true, length = 8)
    public String getXgjb() {
        return xgjb;
    }

    public void setXgjb(String xgjb) {
        this.xgjb = xgjb;
    }

    @Basic
    @Column(name = "YBJB", nullable = true, length = 8)
    public String getYbjb() {
        return ybjb;
    }

    public void setYbjb(String ybjb) {
        this.ybjb = ybjb;
    }

    @Basic
    @Column(name = "YBJB_OTHER", nullable = true, length = 255)
    public String getYbjbOther() {
        return ybjbOther;
    }

    public void setYbjbOther(String ybjbOther) {
        this.ybjbOther = ybjbOther;
    }

    @Basic
    @Column(name = "JKPJ", nullable = true, precision = 0)
    public Integer getJkpj() {
        return jkpj;
    }

    public void setJkpj(Integer jkpj) {
        this.jkpj = jkpj;
    }

    @Basic
    @Column(name = "JKZD", nullable = true, length = 60)
    public String getJkzd() {
        return jkzd;
    }

    public void setJkzd(String jkzd) {
        this.jkzd = jkzd;
    }

    @Basic
    @Column(name = "WXKZ", nullable = true, length = 60)
    public String getWxkz() {
        return wxkz;
    }

    public void setWxkz(String wxkz) {
        this.wxkz = wxkz;
    }

    @Basic
    @Column(name = "QUIETDRINKAGE", nullable = true, precision = 0)
    public Integer getQuietdrinkage() {
        return quietdrinkage;
    }

    public void setQuietdrinkage(Integer quietdrinkage) {
        this.quietdrinkage = quietdrinkage;
    }

    @Basic
    @Column(name = "DANGERTYPE", nullable = true, length = 255)
    public String getDangertype() {
        return dangertype;
    }

    public void setDangertype(String dangertype) {
        this.dangertype = dangertype;
    }

    @Basic
    @Column(name = "XGJB_OTHER", nullable = true, length = 255)
    public String getXgjbOther() {
        return xgjbOther;
    }

    public void setXgjbOther(String xgjbOther) {
        this.xgjbOther = xgjbOther;
    }

    @Basic
    @Column(name = "JKPJ_YC", nullable = true, length = 4000)
    public String getJkpjYc() {
        return jkpjYc;
    }

    public void setJkpjYc(String jkpjYc) {
        this.jkpjYc = jkpjYc;
    }

    @Basic
    @Column(name = "JKZD_REMARKS", nullable = true, length = 100)
    public String getJkzdRemarks() {
        return jkzdRemarks;
    }

    public void setJkzdRemarks(String jkzdRemarks) {
        this.jkzdRemarks = jkzdRemarks;
    }

    @Basic
    @Column(name = "LEFTLOWPRESSURE", nullable = true, precision = 0)
    public Integer getLeftlowpressure() {
        return leftlowpressure;
    }

    public void setLeftlowpressure(Integer leftlowpressure) {
        this.leftlowpressure = leftlowpressure;
    }

    @Basic
    @Column(name = "RIGHTLOWPRESSURE", nullable = true, precision = 0)
    public Integer getRightlowpressure() {
        return rightlowpressure;
    }

    public void setRightlowpressure(Integer rightlowpressure) {
        this.rightlowpressure = rightlowpressure;
    }

    @Basic
    @Column(name = "XDTFX", nullable = true, length = 255)
    public String getXdtfx() {
        return xdtfx;
    }

    public void setXdtfx(String xdtfx) {
        this.xdtfx = xdtfx;
    }

    @Basic
    @Column(name = "BLOODOXYGEN", nullable = true, precision = 0)
    public Long getBloodoxygen() {
        return bloodoxygen;
    }

    public void setBloodoxygen(Long bloodoxygen) {
        this.bloodoxygen = bloodoxygen;
    }

    @Basic
    @Column(name = "BLOODLUSTATE", nullable = true, length = 255)
    public String getBloodlustate() {
        return bloodlustate;
    }

    public void setBloodlustate(String bloodlustate) {
        this.bloodlustate = bloodlustate;
    }

    @Basic
    @Column(name = "BLOODGLUTIME", nullable = true)
    public Date getBloodglutime() {
        return bloodglutime;
    }

    public void setBloodglutime(Date bloodglutime) {
        this.bloodglutime = bloodglutime;
    }

    @Basic
    @Column(name = "TEMPTER", nullable = true, length = 255)
    public String getTempter() {
        return tempter;
    }

    public void setTempter(String tempter) {
        this.tempter = tempter;
    }

    @Basic
    @Column(name = "CODENUM", nullable = true, length = 255)
    public String getCodenum() {
        return codenum;
    }

    public void setCodenum(String codenum) {
        this.codenum = codenum;
    }

    @Basic
    @Column(name = "PATIENTCODE", nullable = true)
    public String getPatientcode() {
        return patientcode;
    }

    public void setPatientcode(String patientcode) {
        this.patientcode = patientcode;
    }

    @Basic
    @Column(name = "OLDER_RZGN_FEN", nullable = true, length = 10)
    public String getOlderRzgnFen() {
        return olderRzgnFen;
    }

    public void setOlderRzgnFen(String olderRzgnFen) {
        this.olderRzgnFen = olderRzgnFen;
    }

    @Basic
    @Column(name = "OLDER_QGZT_FEN", nullable = true, length = 10)
    public String getOlderQgztFen() {
        return olderQgztFen;
    }

    public void setOlderQgztFen(String olderQgztFen) {
        this.olderQgztFen = olderQgztFen;
    }

    @Basic
    @Column(name = "SJXTJB_OTHER", nullable = true, length = 50)
    public String getSjxtjbOther() {
        return sjxtjbOther;
    }

    public void setSjxtjbOther(String sjxtjbOther) {
        this.sjxtjbOther = sjxtjbOther;
    }

    @Basic
    @Column(name = "QTXTJB_OTHER", nullable = true, length = 50)
    public String getQtxtjbOther() {
        return qtxtjbOther;
    }

    public void setQtxtjbOther(String qtxtjbOther) {
        this.qtxtjbOther = qtxtjbOther;
    }

    @Basic
    @Column(name = "DRINK_TYPE_OTHER", nullable = true, length = 50)
    public String getDrinkTypeOther() {
        return drinkTypeOther;
    }

    public void setDrinkTypeOther(String drinkTypeOther) {
        this.drinkTypeOther = drinkTypeOther;
    }

    @Basic
    @Column(name = "WORKER_TYPE", nullable = true, length = 50)
    public String getWorkerType() {
        return workerType;
    }

    public void setWorkerType(String workerType) {
        this.workerType = workerType;
    }

    @Basic
    @Column(name = "WORK_TIME", nullable = true, length = 20)
    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    @Basic
    @Column(name = "FENCHEN", nullable = true, length = 300)
    public String getFenchen() {
        return fenchen;
    }

    public void setFenchen(String fenchen) {
        this.fenchen = fenchen;
    }

    @Basic
    @Column(name = "FCHCS", nullable = true, length = 2)
    public String getFchcs() {
        return fchcs;
    }

    public void setFchcs(String fchcs) {
        this.fchcs = fchcs;
    }

    @Basic
    @Column(name = "FCHY", nullable = true, length = 50)
    public String getFchy() {
        return fchy;
    }

    public void setFchy(String fchy) {
        this.fchy = fchy;
    }

    @Basic
    @Column(name = "SHEXIAN", nullable = true, length = 100)
    public String getShexian() {
        return shexian;
    }

    public void setShexian(String shexian) {
        this.shexian = shexian;
    }

    @Basic
    @Column(name = "SXFHCS", nullable = true, length = 2)
    public String getSxfhcs() {
        return sxfhcs;
    }

    public void setSxfhcs(String sxfhcs) {
        this.sxfhcs = sxfhcs;
    }

    @Basic
    @Column(name = "SXFHCSQT", nullable = true, length = 100)
    public String getSxfhcsqt() {
        return sxfhcsqt;
    }

    public void setSxfhcsqt(String sxfhcsqt) {
        this.sxfhcsqt = sxfhcsqt;
    }

    @Basic
    @Column(name = "WULI", nullable = true, length = 50)
    public String getWuli() {
        return wuli;
    }

    public void setWuli(String wuli) {
        this.wuli = wuli;
    }

    @Basic
    @Column(name = "WLCS", nullable = true, length = 2)
    public String getWlcs() {
        return wlcs;
    }

    public void setWlcs(String wlcs) {
        this.wlcs = wlcs;
    }

    @Basic
    @Column(name = "WLY", nullable = true, length = 50)
    public String getWly() {
        return wly;
    }

    public void setWly(String wly) {
        this.wly = wly;
    }

    @Basic
    @Column(name = "HUAXUE", nullable = true, length = 50)
    public String getHuaxue() {
        return huaxue;
    }

    public void setHuaxue(String huaxue) {
        this.huaxue = huaxue;
    }

    @Basic
    @Column(name = "HXPFHCS", nullable = true, length = 2)
    public String getHxpfhcs() {
        return hxpfhcs;
    }

    public void setHxpfhcs(String hxpfhcs) {
        this.hxpfhcs = hxpfhcs;
    }

    @Basic
    @Column(name = "HXPFHCSJT", nullable = true, length = 50)
    public String getHxpfhcsjt() {
        return hxpfhcsjt;
    }

    public void setHxpfhcsjt(String hxpfhcsjt) {
        this.hxpfhcsjt = hxpfhcsjt;
    }

    @Basic
    @Column(name = "GMQT", nullable = true, length = 100)
    public String getGmqt() {
        return gmqt;
    }

    public void setGmqt(String gmqt) {
        this.gmqt = gmqt;
    }

    @Basic
    @Column(name = "BLQITA", nullable = true, length = 50)
    public String getBlqita() {
        return blqita;
    }

    public void setBlqita(String blqita) {
        this.blqita = blqita;
    }

    @Basic
    @Column(name = "BLQTCS", nullable = true, length = 2)
    public String getBlqtcs() {
        return blqtcs;
    }

    public void setBlqtcs(String blqtcs) {
        this.blqtcs = blqtcs;
    }

    @Basic
    @Column(name = "QTY", nullable = true, precision = 0)
    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    @Basic
    @Column(name = "LBJQT", nullable = true, length = 2)
    public String getLbjqt() {
        return lbjqt;
    }

    public void setLbjqt(String lbjqt) {
        this.lbjqt = lbjqt;
    }

    @Basic
    @Column(name = "LYQT", nullable = true, length = 100)
    public String getLyqt() {
        return lyqt;
    }

    public void setLyqt(String lyqt) {
        this.lyqt = lyqt;
    }

    @Basic
    @Column(name = "FB_YT_DESC", nullable = true, length = 100)
    public String getFbYtDesc() {
        return fbYtDesc;
    }

    public void setFbYtDesc(String fbYtDesc) {
        this.fbYtDesc = fbYtDesc;
    }

    @Basic
    @Column(name = "FB_BK_DESC", nullable = true, length = 100)
    public String getFbBkDesc() {
        return fbBkDesc;
    }

    public void setFbBkDesc(String fbBkDesc) {
        this.fbBkDesc = fbBkDesc;
    }

    @Basic
    @Column(name = "FB_GD_DESC", nullable = true, length = 100)
    public String getFbGdDesc() {
        return fbGdDesc;
    }

    public void setFbGdDesc(String fbGdDesc) {
        this.fbGdDesc = fbGdDesc;
    }

    @Basic
    @Column(name = "FB_PD_DESC", nullable = true, length = 100)
    public String getFbPdDesc() {
        return fbPdDesc;
    }

    public void setFbPdDesc(String fbPdDesc) {
        this.fbPdDesc = fbPdDesc;
    }

    @Basic
    @Column(name = "SKIN_OTHER", nullable = true, length = 100)
    public String getSkinOther() {
        return skinOther;
    }

    public void setSkinOther(String skinOther) {
        this.skinOther = skinOther;
    }

    @Basic
    @Column(name = "FB_ZY_DESC", nullable = true, length = 100)
    public String getFbZyDesc() {
        return fbZyDesc;
    }

    public void setFbZyDesc(String fbZyDesc) {
        this.fbZyDesc = fbZyDesc;
    }

    @Basic
    @Column(name = "XDTQT", nullable = true, length = 200)
    public String getXdtqt() {
        return xdtqt;
    }

    public void setXdtqt(String xdtqt) {
        this.xdtqt = xdtqt;
    }

    @Basic
    @Column(name = "OTHER_VOICE_DESC", nullable = true, length = 255)
    public String getOtherVoiceDesc() {
        return otherVoiceDesc;
    }

    public void setOtherVoiceDesc(String otherVoiceDesc) {
        this.otherVoiceDesc = otherVoiceDesc;
    }

    @Basic
    @Column(name = "HXY_YC", nullable = true, length = 50)
    public String getHxyYc() {
        return hxyYc;
    }

    public void setHxyYc(String hxyYc) {
        this.hxyYc = hxyYc;
    }

    @Basic
    @Column(name = "TXBGAS", nullable = true, precision = 2)
    public Long getTxbgas() {
        return txbgas;
    }

    public void setTxbgas(Long txbgas) {
        this.txbgas = txbgas;
    }

    @Basic
    @Column(name = "IS_AUDIT", nullable = true, precision = 0)
    public Integer getAudit() {
        return isAudit;
    }

    public void setAudit(Integer audit) {
        isAudit = audit;
    }

    @Basic
    @Column(name = "XDT_CONCLUSIONS", nullable = true, length = 4000)
    public String getXdtConclusions() {
        return xdtConclusions;
    }

    public void setXdtConclusions(String xdtConclusions) {
        this.xdtConclusions = xdtConclusions;
    }

    @Basic
    @Column(name = "EAT_NUM", nullable = true, precision = 0)
    public Integer getEatNum() {
        return eatNum;
    }

    public void setEatNum(Integer eatNum) {
        this.eatNum = eatNum;
    }

    @Basic
    @Column(name = "CHILD_NUM", nullable = true, precision = 0)
    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }

    @Basic
    @Column(name = "SALT_NUM", nullable = true, precision = 1)
    public Long getSaltNum() {
        return saltNum;
    }

    public void setSaltNum(Long saltNum) {
        this.saltNum = saltNum;
    }

    @Basic
    @Column(name = "SOY_NUM", nullable = true, precision = 1)
    public Long getSoyNum() {
        return soyNum;
    }

    public void setSoyNum(Long soyNum) {
        this.soyNum = soyNum;
    }

    @Basic
    @Column(name = "LOSE_WEIGHT", nullable = true, precision = 1)
    public Long getLoseWeight() {
        return loseWeight;
    }

    public void setLoseWeight(Long loseWeight) {
        this.loseWeight = loseWeight;
    }

    @Basic
    @Column(name = "FBBC_CONCLUSIONS", nullable = true, length = 4000)
    public String getFbbcConclusions() {
        return fbbcConclusions;
    }

    public void setFbbcConclusions(String fbbcConclusions) {
        this.fbbcConclusions = fbbcConclusions;
    }

    @Basic
    @Column(name = "SJXTJB", nullable = true, length = 20)
    public String getSjxtjb() {
        return sjxtjb;
    }

    public void setSjxtjb(String sjxtjb) {
        this.sjxtjb = sjxtjb;
    }

    @Basic
    @Column(name = "QTXTJB", nullable = true, length = 20)
    public String getQtxtjb() {
        return qtxtjb;
    }

    public void setQtxtjb(String qtxtjb) {
        this.qtxtjb = qtxtjb;
    }

    @Basic
    @Column(name = "RUMMAGER_TIME", nullable = true)
    public Date getRummagerDate() {
        return rummagerDate;
    }

    public void setRummagerDate(Date rummagerDate) {
        this.rummagerDate = rummagerDate;
    }

    @Basic
    @Column(name = "RUMMAGERNAME", nullable = true, length = 255)
    public String getRummagername() {
        return rummagername;
    }

    public void setRummagername(String rummagername) {
        this.rummagername = rummagername;
    }

    @Basic
    @Column(name = "WXKZQT", nullable = true, length = 90)
    public String getWxkzqt() {
        return wxkzqt;
    }

    public void setWxkzqt(String wxkzqt) {
        this.wxkzqt = wxkzqt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthExamination that = (HealthExamination) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(creater, that.creater) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifyer, that.modifyer) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(olderid, that.olderid) &&
                Objects.equals(idnum, that.idnum) &&
                Objects.equals(examdate, that.examdate) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(symptom, that.symptom) &&
                Objects.equals(temperature, that.temperature) &&
                Objects.equals(pulserate, that.pulserate) &&
                Objects.equals(breathrate, that.breathrate) &&
                Objects.equals(leftbldpressure, that.leftbldpressure) &&
                Objects.equals(rightbldpressure, that.rightbldpressure) &&
                Objects.equals(height, that.height) &&
                Objects.equals(weight, that.weight) &&
                Objects.equals(waistline, that.waistline) &&
                Objects.equals(bodyindex, that.bodyindex) &&
                Objects.equals(olderZwpg, that.olderZwpg) &&
                Objects.equals(olderZlnl, that.olderZlnl) &&
                Objects.equals(olderRzgn, that.olderRzgn) &&
                Objects.equals(olderQgzt, that.olderQgzt) &&
                Objects.equals(sportrate, that.sportrate) &&
                Objects.equals(pertimesport, that.pertimesport) &&
                Objects.equals(sporttime, that.sporttime) &&
                Objects.equals(sporttype, that.sporttype) &&
                Objects.equals(eatstate, that.eatstate) &&
                Objects.equals(smokestate, that.smokestate) &&
                Objects.equals(perdaysmoke, that.perdaysmoke) &&
                Objects.equals(smokestart, that.smokestart) &&
                Objects.equals(smokeend, that.smokeend) &&
                Objects.equals(drinkstate, that.drinkstate) &&
                Objects.equals(perdaydrink, that.perdaydrink) &&
                Objects.equals(quietdrink, that.quietdrink) &&
                Objects.equals(startdrink, that.startdrink) &&
                Objects.equals(drunked, that.drunked) &&
                Objects.equals(drinktype, that.drinktype) &&
                Objects.equals(danger, that.danger) &&
                Objects.equals(kqKc, that.kqKc) &&
                Objects.equals(kqCl, that.kqCl) &&
                Objects.equals(kqYb, that.kqYb) &&
                Objects.equals(lefteye, that.lefteye) &&
                Objects.equals(righteye, that.righteye) &&
                Objects.equals(jzLefteye, that.jzLefteye) &&
                Objects.equals(jzRighteye, that.jzRighteye) &&
                Objects.equals(tl, that.tl) &&
                Objects.equals(sportability, that.sportability) &&
                Objects.equals(eyebottom, that.eyebottom) &&
                Objects.equals(skin, that.skin) &&
                Objects.equals(gm, that.gm) &&
                Objects.equals(lbj, that.lbj) &&
                Objects.equals(tzx, that.tzx) &&
                Objects.equals(hxy, that.hxy) &&
                Objects.equals(ly, that.ly) &&
                Objects.equals(heartrate, that.heartrate) &&
                Objects.equals(heartrhythm, that.heartrhythm) &&
                Objects.equals(othervoice, that.othervoice) &&
                Objects.equals(fbYt, that.fbYt) &&
                Objects.equals(fbBk, that.fbBk) &&
                Objects.equals(fbGd, that.fbGd) &&
                Objects.equals(fbPd, that.fbPd) &&
                Objects.equals(fbZy, that.fbZy) &&
                Objects.equals(xzsz, that.xzsz) &&
                Objects.equals(zbdmbd, that.zbdmbd) &&
                Objects.equals(gmzz, that.gmzz) &&
                Objects.equals(rx, that.rx) &&
                Objects.equals(fkWy, that.fkWy) &&
                Objects.equals(fkYd, that.fkYd) &&
                Objects.equals(fkGj, that.fkGj) &&
                Objects.equals(fkGt, that.fkGt) &&
                Objects.equals(fkFj, that.fkFj) &&
                Objects.equals(ctOther, that.ctOther) &&
                Objects.equals(xhdb, that.xhdb) &&
                Objects.equals(bxb, that.bxb) &&
                Objects.equals(xxb, that.xxb) &&
                Objects.equals(xcgOther, that.xcgOther) &&
                Objects.equals(ndb, that.ndb) &&
                Objects.equals(nt, that.nt) &&
                Objects.equals(ntt, that.ntt) &&
                Objects.equals(nqx, that.nqx) &&
                Objects.equals(ncgOther, that.ncgOther) &&
                Objects.equals(kfxt, that.kfxt) &&
                Objects.equals(xdt, that.xdt) &&
                Objects.equals(nwlbdb, that.nwlbdb) &&
                Objects.equals(dbqx, that.dbqx) &&
                Objects.equals(thxhdb, that.thxhdb) &&
                Objects.equals(gybmky, that.gybmky) &&
                Objects.equals(xqgbzam, that.xqgbzam) &&
                Objects.equals(xqgczam, that.xqgczam) &&
                Objects.equals(bdb, that.bdb) &&
                Objects.equals(zdhs, that.zdhs) &&
                Objects.equals(jhdhs, that.jhdhs) &&
                Objects.equals(xqjg, that.xqjg) &&
                Objects.equals(xns, that.xns) &&
                Objects.equals(xjnd, that.xjnd) &&
                Objects.equals(xnnd, that.xnnd) &&
                Objects.equals(zdgc, that.zdgc) &&
                Objects.equals(gysz, that.gysz) &&
                Objects.equals(xqdmdzdb, that.xqdmdzdb) &&
                Objects.equals(xqgmdzdb, that.xqgmdzdb) &&
                Objects.equals(xbxxp, that.xbxxp) &&
                Objects.equals(fbbc, that.fbbc) &&
                Objects.equals(bcOther, that.bcOther) &&
                Objects.equals(gjtp, that.gjtp) &&
                Objects.equals(fzjcOther, that.fzjcOther) &&
                Objects.equals(nxgjb, that.nxgjb) &&
                Objects.equals(nxgjbOther, that.nxgjbOther) &&
                Objects.equals(szjb, that.szjb) &&
                Objects.equals(szjbOther, that.szjbOther) &&
                Objects.equals(xzjb, that.xzjb) &&
                Objects.equals(xzjbOther, that.xzjbOther) &&
                Objects.equals(xgjb, that.xgjb) &&
                Objects.equals(ybjb, that.ybjb) &&
                Objects.equals(ybjbOther, that.ybjbOther) &&
                Objects.equals(jkpj, that.jkpj) &&
                Objects.equals(jkzd, that.jkzd) &&
                Objects.equals(wxkz, that.wxkz) &&
                Objects.equals(quietdrinkage, that.quietdrinkage) &&
                Objects.equals(dangertype, that.dangertype) &&
                Objects.equals(xgjbOther, that.xgjbOther) &&
                Objects.equals(jkpjYc, that.jkpjYc) &&
                Objects.equals(jkzdRemarks, that.jkzdRemarks) &&
                Objects.equals(leftlowpressure, that.leftlowpressure) &&
                Objects.equals(rightlowpressure, that.rightlowpressure) &&
                Objects.equals(xdtfx, that.xdtfx) &&
                Objects.equals(bloodoxygen, that.bloodoxygen) &&
                Objects.equals(bloodlustate, that.bloodlustate) &&
                Objects.equals(bloodglutime, that.bloodglutime) &&
                Objects.equals(tempter, that.tempter) &&
                Objects.equals(codenum, that.codenum) &&
                Objects.equals(patientcode, that.patientcode) &&
                Objects.equals(olderRzgnFen, that.olderRzgnFen) &&
                Objects.equals(olderQgztFen, that.olderQgztFen) &&
                Objects.equals(sjxtjbOther, that.sjxtjbOther) &&
                Objects.equals(qtxtjbOther, that.qtxtjbOther) &&
                Objects.equals(drinkTypeOther, that.drinkTypeOther) &&
                Objects.equals(workerType, that.workerType) &&
                Objects.equals(workDate, that.workDate) &&
                Objects.equals(fenchen, that.fenchen) &&
                Objects.equals(fchcs, that.fchcs) &&
                Objects.equals(fchy, that.fchy) &&
                Objects.equals(shexian, that.shexian) &&
                Objects.equals(sxfhcs, that.sxfhcs) &&
                Objects.equals(sxfhcsqt, that.sxfhcsqt) &&
                Objects.equals(wuli, that.wuli) &&
                Objects.equals(wlcs, that.wlcs) &&
                Objects.equals(wly, that.wly) &&
                Objects.equals(huaxue, that.huaxue) &&
                Objects.equals(hxpfhcs, that.hxpfhcs) &&
                Objects.equals(hxpfhcsjt, that.hxpfhcsjt) &&
                Objects.equals(gmqt, that.gmqt) &&
                Objects.equals(blqita, that.blqita) &&
                Objects.equals(blqtcs, that.blqtcs) &&
                Objects.equals(qty, that.qty) &&
                Objects.equals(lbjqt, that.lbjqt) &&
                Objects.equals(lyqt, that.lyqt) &&
                Objects.equals(fbYtDesc, that.fbYtDesc) &&
                Objects.equals(fbBkDesc, that.fbBkDesc) &&
                Objects.equals(fbGdDesc, that.fbGdDesc) &&
                Objects.equals(fbPdDesc, that.fbPdDesc) &&
                Objects.equals(skinOther, that.skinOther) &&
                Objects.equals(fbZyDesc, that.fbZyDesc) &&
                Objects.equals(xdtqt, that.xdtqt) &&
                Objects.equals(otherVoiceDesc, that.otherVoiceDesc) &&
                Objects.equals(hxyYc, that.hxyYc) &&
                Objects.equals(txbgas, that.txbgas) &&
                Objects.equals(isAudit, that.isAudit) &&
                Objects.equals(xdtConclusions, that.xdtConclusions) &&
                Objects.equals(eatNum, that.eatNum) &&
                Objects.equals(childNum, that.childNum) &&
                Objects.equals(saltNum, that.saltNum) &&
                Objects.equals(soyNum, that.soyNum) &&
                Objects.equals(loseWeight, that.loseWeight) &&
                Objects.equals(fbbcConclusions, that.fbbcConclusions) &&
                Objects.equals(sjxtjb, that.sjxtjb) &&
                Objects.equals(qtxtjb, that.qtxtjb) &&
                Objects.equals(rummagerDate, that.rummagerDate) &&
                Objects.equals(rummagername, that.rummagername) &&
                Objects.equals(wxkzqt, that.wxkzqt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creater, createdate, modifyer, modifydate, olderid, idnum, examdate, doctor, symptom, temperature, pulserate, breathrate, leftbldpressure, rightbldpressure, height, weight, waistline, bodyindex, olderZwpg, olderZlnl, olderRzgn, olderQgzt, sportrate, pertimesport, sporttime, sporttype, eatstate, smokestate, perdaysmoke, smokestart, smokeend, drinkstate, perdaydrink, quietdrink, startdrink, drunked, drinktype, danger, kqKc, kqCl, kqYb, lefteye, righteye, jzLefteye, jzRighteye, tl, sportability, eyebottom, skin, gm, lbj, tzx, hxy, ly, heartrate, heartrhythm, othervoice, fbYt, fbBk, fbGd, fbPd, fbZy, xzsz, zbdmbd, gmzz, rx, fkWy, fkYd, fkGj, fkGt, fkFj, ctOther, xhdb, bxb, xxb, xcgOther, ndb, nt, ntt, nqx, ncgOther, kfxt, xdt, nwlbdb, dbqx, thxhdb, gybmky, xqgbzam, xqgczam, bdb, zdhs, jhdhs, xqjg, xns, xjnd, xnnd, zdgc, gysz, xqdmdzdb, xqgmdzdb, xbxxp, fbbc, bcOther, gjtp, fzjcOther, nxgjb, nxgjbOther, szjb, szjbOther, xzjb, xzjbOther, xgjb, ybjb, ybjbOther, jkpj, jkzd, wxkz, quietdrinkage, dangertype, xgjbOther, jkpjYc, jkzdRemarks, leftlowpressure, rightlowpressure, xdtfx, bloodoxygen, bloodlustate, bloodglutime, tempter, codenum, patientcode, olderRzgnFen, olderQgztFen, sjxtjbOther, qtxtjbOther, drinkTypeOther, workerType, workDate, fenchen, fchcs, fchy, shexian, sxfhcs, sxfhcsqt, wuli, wlcs, wly, huaxue, hxpfhcs, hxpfhcsjt, gmqt, blqita, blqtcs, qty, lbjqt, lyqt, fbYtDesc, fbBkDesc, fbGdDesc, fbPdDesc, skinOther, fbZyDesc, xdtqt, otherVoiceDesc, hxyYc, txbgas, isAudit, xdtConclusions, eatNum, childNum, saltNum, soyNum, loseWeight, fbbcConclusions, sjxtjb, qtxtjb, rummagerDate, rummagername, wxkzqt);
    }
}