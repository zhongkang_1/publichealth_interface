package com.zhongkang.publichealth.pojo;

import lombok.Data;

import java.util.List;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 10:10
 */
@Data
public class ExamDTO {
    private Peispatient peispatient;
    private HealthExamination examination;
    private List<HealthHospital> hospitals;
    private List<HealthFamily> families;
    private List<HealthDefend> defends;
    private List<HealthMedicineOracle> medicines;
}