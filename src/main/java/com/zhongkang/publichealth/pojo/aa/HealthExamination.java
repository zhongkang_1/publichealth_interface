package com.zhongkang.publichealth.pojo.aa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@Data
public class HealthExamination implements Serializable {
    /**
     *
     */
    private String id;

    /**
     * 创建人
     */
    private String creater;

    /**
     * 创建时间
     */
    private Date createdate;

    /**
     * 修改者
     */
    private String modifyer;

    /**
     * 修改时间
     */
    private Date modifydate;

    /**
     * 老人id
     */
    private String olderid;

    /**
     * 档案号
     */
    private String idnum;

    /**
     * 体检日期
     */
    private Date examdate;

    /**
     * 责任医生
     */
    private String doctor;

    /**
     * 症状
     */
    private String symptom;

    /**
     * 体温
     */
    private BigDecimal temperature;

    /**
     * 脉率
     */
    private Short pulserate;

    /**
     * 呼吸频率
     */
    private Short breathrate;

    /**
     * 左侧高压
     */
    private Short leftbldpressure;

    /**
     * 右侧高压
     */
    private Short rightbldpressure;

    /**
     * 身高
     */
    private BigDecimal height;

    /**
     * 体重
     */
    private BigDecimal weight;

    /**
     * 腰围
     */
    private BigDecimal waistline;

    /**
     * 体质指数
     */
    private BigDecimal bodyindex;

    /**
     * 老年人状态自我评估（1 满意 2 基本满意 3 说不清楚 4 不太满意 5 不满意）
     */
    private Short olderZwpg;

    /**
     * 老年人生活自理能力自我评估（1 可自理（0～3 分） 2 轻度依赖（4～8 分）
     * 3 中度依赖（9～18 分) 4 不能自理（≥19 分） ）
     */
    private Short olderZlnl;

    /**
     * 老年人
     * 认知功能
     */
    private String olderRzgn;

    /**
     * 老年人情感状态
     */
    private String olderQgzt;

    /**
     * 运动频率（1 每天 2 每周一次以上 3 偶尔 4 不锻炼 ）
     */
    private Short sportrate;

    /**
     * 每次锻炼时间
     */
    private Short pertimesport;

    /**
     * 坚持锻炼的时间
     */
    private BigDecimal sporttime;

    /**
     * 锻炼方式
     */
    private String sporttype;

    /**
     * 饮食习惯（1 荤素均衡 2 荤食为主 3 素食为主 4 嗜盐 5 嗜油 6 嗜糖）
     */
    private String eatstate;

    /**
     * 吸烟情况（1 从不吸烟 2 已戒烟 3 吸烟 ）
     */
    private Short smokestate;

    /**
     * 日吸烟量
     */
    private Short perdaysmoke;

    /**
     * 开始吸烟年龄
     */
    private Short smokestart;

    /**
     * 戒烟年龄
     */
    private Short smokeend;

    /**
     * 饮酒情况（1 从不 2 偶尔 3 经常 4 每天）
     */
    private Short drinkstate;

    /**
     * 日饮酒量  平均__两
     */
    private BigDecimal perdaydrink;

    /**
     * 是否戒酒（1 未戒酒 2 已戒酒，戒酒年龄： 岁 ）
     */
    private Short quietdrink;

    /**
     * 开始饮酒年龄
     */
    private Short startdrink;

    /**
     * 进一年内是否醉酒（ 1 是 2 否 ）
     */
    private Short drunked;

    /**
     * 饮酒种类（1 白酒 2 啤酒 3 红酒 4 黄酒 ５其他）
     */
    private String drinktype;

    /**
     * 职业病危害因素接触史
     */
    private String danger;

    /**
     * 口唇情况（1 红润 2 苍白 3 发绀 4 皲裂 5 疱疹）
     */
    private Short kqKc;

    /**
     * 齿列情况（1 正常 2 缺齿 3 龋齿 4 义齿(假牙)）
     */
    private String kqCl;

    /**
     * 咽部情况（1 无充血 2 充血 3 淋巴滤泡增生）
     */
    private Short kqYb;

    /**
     * 左眼情况
     */
    private BigDecimal lefteye;

    /**
     * 右眼情况
     */
    private BigDecimal righteye;

    /**
     * 矫正左眼情况
     */
    private BigDecimal jzLefteye;

    /**
     * 矫正右眼情况
     */
    private BigDecimal jzRighteye;

    /**
     * 听力情况（1，听见2，听不清或无法听见）
     */
    private Short tl;

    /**
     * 运动功能（1，可独立完成、2无法独立完成任何一个动作）
     */
    private Short sportability;

    /**
     * 眼底(1,正常。2，异常)
     */
    private Short eyebottom;

    /**
     * 皮肤（1 正常 2 潮红 3 苍白 4发绀 5 黄染 6 色素沉着 7 其他 ）
     */
    private Short skin;

    /**
     * 巩膜（1 正常 2 黄染 3 充血 4 其他 ）
     */
    private Short gm;

    /**
     * 淋巴结（1 未触及 2 锁骨上 3 腋窝 4 其他）
     */
    private Short lbj;

    /**
     * 桶状胸（1 否 2 是）
     */
    private Short tzx;

    /**
     * 呼吸音（1 正常 2 异常 ）
     */
    private Short hxy;

    /**
     * 罗音（1 无 2 干罗音 3 湿罗音 4 其他 ）
     */
    private Short ly;

    /**
     * 心率
     */
    private Short heartrate;

    /**
     * 心律（1 齐 2 不齐 3 绝对不齐）
     */
    private Short heartrhythm;

    /**
     * 杂音（1 无 2 有 ）
     */
    private Short othervoice;

    /**
     * 腹部压痛（1 无 2 有 ）
     */
    private Short fbYt;

    /**
     * 腹部包块（1 无 2 有 ）
     */
    private Short fbBk;

    /**
     * 腹部肝大（1 无 2 有 ）
     */
    private Short fbGd;

    /**
     * 腹部脾大（1 无 2 有 ）
     */
    private Short fbPd;

    /**
     * 腹部移动性浊音（1 无 2 有 ）
     */
    private Short fbZy;

    /**
     * 下肢水肿（1 无 2 单侧 3 双侧不对称 4 双侧对称 ）
     */
    private Short xzsz;

    /**
     * 足背动脉搏动（1 无 2 单侧 3 双侧不对称 4 双侧对称 ）
     */
    private Short zbdmbd;

    /**
     * 肛门指诊（1 未及异常 2 触痛 3 包块 4 前列腺异常 5 其他 ）
     */
    private Short gmzz;

    /**
     * 乳腺（1 未见异常 2 乳房切除 3 异常泌乳 4 乳腺包块 5 其他 ）
     */
    private String rx;

    /**
     * 外阴（1 未见异常 2 异常 ）
     */
    private Short fkWy;

    /**
     * 阴道（1 未见异常 2 异常 ）
     */
    private Short fkYd;

    /**
     * 宫颈（1 未见异常 2 异常 ）
     */
    private Short fkGj;

    /**
     * 宫体（1 未见异常 2 异常 ）
     */
    private Short fkGt;

    /**
     * 附件（1 未见异常 2 异常 ）
     */
    private Short fkFj;

    /**
     * 查体其他
     */
    private String ctOther;

    /**
     * 血红蛋白g/L
     */
    private String xhdb;

    /**
     * 白细胞×109
     * /L
     */
    private String bxb;

    /**
     * 血小板×109
     * /L
     */
    private String xxb;

    /**
     * 血常规（其他）
     */
    private String xcgOther;

    /**
     * 尿蛋白
     */
    private String ndb;

    /**
     * 尿糖
     */
    private String nt;

    /**
     * 尿酮体
     */
    private String ntt;

    /**
     * 尿潜血
     */
    private String nqx;

    /**
     * 尿常规（其他）
     */
    private String ncgOther;

    /**
     * 空腹血糖mmol/L或mg/dL
     */
    private String kfxt;

    /**
     * 心电图（1 正常 2 异常 ）
     */
    private Short xdt;

    /**
     * 尿微量白蛋白mg/dL
     */
    private BigDecimal nwlbdb;

    /**
     * 大便潜血（1 阴性 2 阳性）
     */
    private Short dbqx;

    /**
     * 糖化血红蛋白%
     */
    private BigDecimal thxhdb;

    /**
     * 乙型肝炎
     * 表面抗原(1 阴性 2 阳性)
     */
    private Short gybmky;

    /**
     * 血清谷丙转氨酶U/L
     */
    private BigDecimal xqgbzam;

    /**
     * 血清谷草转氨酶U/L
     */
    private BigDecimal xqgczam;

    /**
     * 白蛋白g/L
     */
    private BigDecimal bdb;

    /**
     * 总胆红素μmol/L
     */
    private BigDecimal zdhs;

    /**
     * 结合胆红素μmol/L
     */
    private BigDecimal jhdhs;

    /**
     * 血清肌酐μmol/L
     */
    private BigDecimal xqjg;

    /**
     * 血尿素mmol/L
     */
    private BigDecimal xns;

    /**
     * 血钾浓度mmol/L
     */
    private BigDecimal xjnd;

    /**
     * 血钠浓度mmol/L
     */
    private BigDecimal xnnd;

    /**
     * 总胆固醇mmol/L
     */
    private BigDecimal zdgc;

    /**
     * 甘油三酯mmol/L
     */
    private BigDecimal gysz;

    /**
     * 血清低密度脂蛋白胆固醇mmol/L
     */
    private BigDecimal xqdmdzdb;

    /**
     * 血清高密度脂蛋白胆固醇mmol/L
     */
    private BigDecimal xqgmdzdb;

    /**
     * 胸部X线片（1 正常 2 异常）
     */
    private Short xbxxp;

    /**
     * 腹部B超（1 正常 2 异常）
     */
    private Short fbbc;

    /**
     * B超其他（1 正常 2 异常）
     */
    private Short bcOther;

    /**
     * 宫颈涂片（1 正常 2 异常）
     */
    private Short gjtp;

    /**
     * 辅助检查其他
     */
    private String fzjcOther;

    /**
     * 脑血管疾病（1 未发现 2 缺血性卒中 3 脑出血 4 蛛网膜下腔出血 5 短暂性脑缺血发作 ）
     */
    private String nxgjb;

    /**
     * 脑血管疾病其他
     */
    private String nxgjbOther;

    /**
     * 肾脏疾病（1 未发现 2 糖尿病肾病 3 肾功能衰竭 4 急性肾炎 5 慢性肾炎 ）
     */
    private String szjb;

    /**
     * 肾脏疾病其他
     */
    private String szjbOther;

    /**
     * 心脏疾病（1 未发现 2 心肌梗死 3 心绞痛 4 冠状动脉血运重建 5 充血性心力衰竭
     * 6 心前区疼痛）
     */
    private String xzjb;

    /**
     * 心脏疾病其他
     */
    private String xzjbOther;

    /**
     * 血管疾病（1 未发现 2 夹层动脉瘤 3 动脉闭塞性疾病 4 其他 ）
     */
    private String xgjb;

    /**
     * 眼部疾病（1 未发现 2 视网膜出血或渗出 3 视乳头水肿 4 白内障）
     */
    private String ybjb;

    /**
     * 眼部疾病其他
     */
    private String ybjbOther;

    /**
     * 健康评价（1体检无异常  2 有异常）
     */
    private Short jkpj;

    /**
     * 健康指导（1 纳入慢性病患者健康管理
     * 2 建议复查
     * 3 建议转诊
     * ）
     */
    private String jkzd;

    /**
     * 危险因素控制（1 戒烟 2 健康饮酒 3 饮食 4 锻炼 5 减体重（目标 Kg）
     * 6 建议接种疫苗
     * 7 其他）
     */
    private String wxkz;

    /**
     * 戒酒年龄
     */
    private Short quietdrinkage;

    /**
     * 毒物种类
     */
    private String dangertype;

    /**
     * 血管疾病其他
     */
    private String xgjbOther;

    /**
     * 健康评价 异常
     */
    private String jkpjYc;

    /**
     * 健康指导 备注
     */
    private String jkzdRemarks;

    /**
     * 左侧低压
     */
    private Short leftlowpressure;

    /**
     * 右侧低压
     */
    private Short rightlowpressure;

    /**
     * 心电图结果分析
     */
    private String xdtfx;

    /**
     * 血氧
     */
    private Integer bloodoxygen;

    /**
     * 测量血糖状态（餐前为2，餐后为1）
     */
    private String bloodlustate;

    /**
     * 测量时间
     */
    private Date bloodglutime;

    /**
     * 测量环境温度
     */
    private String tempter;

    /**
     * code编号
     */
    private String codenum;

    /**
     *
     */
    private String patientcode;

    /**
     * 老人认知分数
     */
    private String olderRzgnFen;

    /**
     * 老人情感分数
     */
    private String olderQgztFen;

    /**
     * 神经系统疾病
     */
    private String sjxtjbOther;

    /**
     * 其他系统疾病
     */
    private String qtxtjbOther;

    /**
     * 饮酒类型其他
     */
    private String drinkTypeOther;

    /**
     * 工种
     */
    private String workerType;

    /**
     * 从业时间
     */
    private String workTime;

    /**
     * 粉尘
     */
    private String fenchen;

    /**
     * 职业病危害因素接触史-粉尘-防护措施  1.无;2.有;
     */
    private String fchcs;

    /**
     * 粉尘措施  和字段fchcs有关系，当为2时，使用此字段
     */
    private String fchy;

    /**
     * 放射物质
     */
    private String shexian;

    /**
     * 职业病危害因素接触史-放射物质-防护措施 1.无;2.有;
     */
    private String sxfhcs;

    /**
     * 放射物质措施
     */
    private String sxfhcsqt;

    /**
     * 物理因素
     */
    private String wuli;

    /**
     * 职业病危害因素接触史-物理因素-防护措施 1.无;2.有;
     */
    private String wlcs;

    /**
     * 物理因素措施
     */
    private String wly;

    /**
     * 化学因素
     */
    private String huaxue;

    /**
     * 职业病危害因素接触史-化学物质-防护措施 1.无;2.有;
     */
    private String hxpfhcs;

    /**
     * 化学物质措施
     */
    private String hxpfhcsjt;

    /**
     * 巩膜其他
     */
    private String gmqt;

    /**
     * 职业病危害因素接触史
     */
    private String blqita;

    /**
     * 职业病危害因素接触史-其他-防护措施 1.无;2.有;
     */
    private String blqtcs;

    /**
     * 职业病危害因素接触史-其他措施
     */
    private BigDecimal qty;

    /**
     * 淋巴结其他
     */
    private String lbjqt;

    /**
     * 罗音其他
     */
    private String lyqt;

    /**
     * 腹部有压痛（描述）
     */
    private String fbYtDesc;

    /**
     * 腹部包块（描述）
     */
    private String fbBkDesc;

    /**
     * 腹部肝大（描述）
     */
    private String fbGdDesc;

    /**
     * 腹部脾大（描述）
     */
    private String fbPdDesc;

    /**
     * 皮肤其他
     */
    private String skinOther;

    /**
     * 有移动性浊音
     */
    private String fbZyDesc;

    /**
     * 心电图其他
     */
    private String xdtqt;

    /**
     * 有杂音（描述）
     */
    private String otherVoiceDesc;

    /**
     * 呼吸音异常
     */
    private String hxyYc;

    /**
     *
     */
    private BigDecimal txbgas;

    /**
     *
     */
    private Short isAudit;

    /**
     *
     */
    private String xdtConclusions;

    /**
     *
     */
    private Short eatNum;

    /**
     *
     */
    private Short childNum;

    /**
     *
     */
    private BigDecimal saltNum;

    /**
     *
     */
    private BigDecimal soyNum;

    /**
     *
     */
    private BigDecimal loseWeight;

    /**
     *
     */
    private String fbbcConclusions;

    /**
     *
     */
    private String sjxtjb;

    /**
     *
     */
    private String qtxtjb;

    /**
     *
     */
    private Date rummagerTime;

    /**
     *
     */
    private String rummagername;

    /**
     *
     */
    private String wxkzqt;

    private static final long serialVersionUID = 1L;
}

