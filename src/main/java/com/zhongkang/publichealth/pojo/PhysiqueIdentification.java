package com.zhongkang.publichealth.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

/**
 * @program: publichealth
 * @description:
 * @author: yuane
 * @create: 2020-06-12 12:27
 */
@Entity
@Table(name = "PHYSIQUE_IDENTIFICATION")
public class PhysiqueIdentification {
    private String id;
    private Date createdate;
    private Date modifydate;
    private Long question1;
    private Long question2;
    private Long question3;
    private Long question4;
    private Long question5;
    private Long question6;
    private Long question7;
    private Long question8;
    private Long question9;
    private Long question10;
    private Long question11;
    private Long question12;
    private Long question13;
    private Long question14;
    private Long question15;
    private Long question16;
    private Long question17;
    private Long question18;
    private Long question19;
    private Long question20;
    private Long question21;
    private Long question22;
    private Long question23;
    private Long question24;
    private Long question25;
    private Long question26;
    private Long question27;
    private Long question28;
    private Long question29;
    private Long question30;
    private Long question31;
    private Long question32;
    private Long question33;
    private String patientcode;
    private Date fillDate;
    private String doctor;
    private Long qiXuScore;
    private Long yangXuScore;
    private Long yinXuScore;
    private Long tanShiScore;
    private Long shiReScore;
    private Long xueYuScore;
    private Long qiYuScore;
    private Long teBingScore;
    private Long pingHeScore;
    private Long qiXu;
    private Long yangXu;
    private Long yinXu;
    private Long tanShi;
    private Long shiRe;
    private Long xueYu;
    private Long qiYu;
    private Long teBing;
    private Long pingHe;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid")
    @Column(name = "ID", nullable = false, length = 32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CREATEDATE", nullable = false)
    public Date getCreatedate() {
        return createdate;
    }


    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    @Basic
    @Column(name = "MODIFYDATE", nullable = false)
    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    @Basic
    @Column(name = "QUESTION1", nullable = true, precision = 0)
    public Long getQuestion1() {
        return question1;
    }

    public void setQuestion1(Long question1) {
        this.question1 = question1;
    }

    @Basic
    @Column(name = "QUESTION2", nullable = true, precision = 0)
    public Long getQuestion2() {
        return question2;
    }

    public void setQuestion2(Long question2) {
        this.question2 = question2;
    }

    @Basic
    @Column(name = "QUESTION3", nullable = true, precision = 0)
    public Long getQuestion3() {
        return question3;
    }

    public void setQuestion3(Long question3) {
        this.question3 = question3;
    }

    @Basic
    @Column(name = "QUESTION4", nullable = true, precision = 0)
    public Long getQuestion4() {
        return question4;
    }

    public void setQuestion4(Long question4) {
        this.question4 = question4;
    }

    @Basic
    @Column(name = "QUESTION5", nullable = true, precision = 0)
    public Long getQuestion5() {
        return question5;
    }

    public void setQuestion5(Long question5) {
        this.question5 = question5;
    }

    @Basic
    @Column(name = "QUESTION6", nullable = true, precision = 0)
    public Long getQuestion6() {
        return question6;
    }

    public void setQuestion6(Long question6) {
        this.question6 = question6;
    }

    @Basic
    @Column(name = "QUESTION7", nullable = true, precision = 0)
    public Long getQuestion7() {
        return question7;
    }

    public void setQuestion7(Long question7) {
        this.question7 = question7;
    }

    @Basic
    @Column(name = "QUESTION8", nullable = true, precision = 0)
    public Long getQuestion8() {
        return question8;
    }

    public void setQuestion8(Long question8) {
        this.question8 = question8;
    }

    @Basic
    @Column(name = "QUESTION9", nullable = true, precision = 0)
    public Long getQuestion9() {
        return question9;
    }

    public void setQuestion9(Long question9) {
        this.question9 = question9;
    }

    @Basic
    @Column(name = "QUESTION10", nullable = true, precision = 0)
    public Long getQuestion10() {
        return question10;
    }

    public void setQuestion10(Long question10) {
        this.question10 = question10;
    }

    @Basic
    @Column(name = "QUESTION11", nullable = true, precision = 0)
    public Long getQuestion11() {
        return question11;
    }

    public void setQuestion11(Long question11) {
        this.question11 = question11;
    }

    @Basic
    @Column(name = "QUESTION12", nullable = true, precision = 0)
    public Long getQuestion12() {
        return question12;
    }

    public void setQuestion12(Long question12) {
        this.question12 = question12;
    }

    @Basic
    @Column(name = "QUESTION13", nullable = true, precision = 0)
    public Long getQuestion13() {
        return question13;
    }

    public void setQuestion13(Long question13) {
        this.question13 = question13;
    }

    @Basic
    @Column(name = "QUESTION14", nullable = true, precision = 0)
    public Long getQuestion14() {
        return question14;
    }

    public void setQuestion14(Long question14) {
        this.question14 = question14;
    }

    @Basic
    @Column(name = "QUESTION15", nullable = true, precision = 0)
    public Long getQuestion15() {
        return question15;
    }

    public void setQuestion15(Long question15) {
        this.question15 = question15;
    }

    @Basic
    @Column(name = "QUESTION16", nullable = true, precision = 0)
    public Long getQuestion16() {
        return question16;
    }

    public void setQuestion16(Long question16) {
        this.question16 = question16;
    }

    @Basic
    @Column(name = "QUESTION17", nullable = true, precision = 0)
    public Long getQuestion17() {
        return question17;
    }

    public void setQuestion17(Long question17) {
        this.question17 = question17;
    }

    @Basic
    @Column(name = "QUESTION18", nullable = true, precision = 0)
    public Long getQuestion18() {
        return question18;
    }

    public void setQuestion18(Long question18) {
        this.question18 = question18;
    }

    @Basic
    @Column(name = "QUESTION19", nullable = true, precision = 0)
    public Long getQuestion19() {
        return question19;
    }

    public void setQuestion19(Long question19) {
        this.question19 = question19;
    }

    @Basic
    @Column(name = "QUESTION20", nullable = true, precision = 0)
    public Long getQuestion20() {
        return question20;
    }

    public void setQuestion20(Long question20) {
        this.question20 = question20;
    }

    @Basic
    @Column(name = "QUESTION21", nullable = true, precision = 0)
    public Long getQuestion21() {
        return question21;
    }

    public void setQuestion21(Long question21) {
        this.question21 = question21;
    }

    @Basic
    @Column(name = "QUESTION22", nullable = true, precision = 0)
    public Long getQuestion22() {
        return question22;
    }

    public void setQuestion22(Long question22) {
        this.question22 = question22;
    }

    @Basic
    @Column(name = "QUESTION23", nullable = true, precision = 0)
    public Long getQuestion23() {
        return question23;
    }

    public void setQuestion23(Long question23) {
        this.question23 = question23;
    }

    @Basic
    @Column(name = "QUESTION24", nullable = true, precision = 0)
    public Long getQuestion24() {
        return question24;
    }

    public void setQuestion24(Long question24) {
        this.question24 = question24;
    }

    @Basic
    @Column(name = "QUESTION25", nullable = true, precision = 0)
    public Long getQuestion25() {
        return question25;
    }

    public void setQuestion25(Long question25) {
        this.question25 = question25;
    }

    @Basic
    @Column(name = "QUESTION26", nullable = true, precision = 0)
    public Long getQuestion26() {
        return question26;
    }

    public void setQuestion26(Long question26) {
        this.question26 = question26;
    }

    @Basic
    @Column(name = "QUESTION27", nullable = true, precision = 0)
    public Long getQuestion27() {
        return question27;
    }

    public void setQuestion27(Long question27) {
        this.question27 = question27;
    }

    @Basic
    @Column(name = "QUESTION28", nullable = true, precision = 0)
    public Long getQuestion28() {
        return question28;
    }

    public void setQuestion28(Long question28) {
        this.question28 = question28;
    }

    @Basic
    @Column(name = "QUESTION29", nullable = true, precision = 0)
    public Long getQuestion29() {
        return question29;
    }

    public void setQuestion29(Long question29) {
        this.question29 = question29;
    }

    @Basic
    @Column(name = "QUESTION30", nullable = true, precision = 0)
    public Long getQuestion30() {
        return question30;
    }

    public void setQuestion30(Long question30) {
        this.question30 = question30;
    }

    @Basic
    @Column(name = "QUESTION31", nullable = true, precision = 0)
    public Long getQuestion31() {
        return question31;
    }

    public void setQuestion31(Long question31) {
        this.question31 = question31;
    }

    @Basic
    @Column(name = "QUESTION32", nullable = true, precision = 0)
    public Long getQuestion32() {
        return question32;
    }

    public void setQuestion32(Long question32) {
        this.question32 = question32;
    }

    @Basic
    @Column(name = "QUESTION33", nullable = true, precision = 0)
    public Long getQuestion33() {
        return question33;
    }

    public void setQuestion33(Long question33) {
        this.question33 = question33;
    }

    @Basic
    @Column(name = "PATIENTCODE", nullable = true, length = 50)
    public String getPatientcode() {
        return patientcode;
    }

    public void setPatientcode(String patientcode) {
        this.patientcode = patientcode;
    }

    @Basic
    @Column(name = "FILL_DATE", nullable = true)
    public Date getFillDate() {
        return fillDate;
    }

    public void setFillDate(Date fillDate) {
        this.fillDate = fillDate;
    }

    @Basic
    @Column(name = "DOCTOR", nullable = true, length = 255)
    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Basic
    @Column(name = "QI_XU_SCORE", nullable = true, precision = 0)
    public Long getQiXuScore() {
        return qiXuScore;
    }

    public void setQiXuScore(Long qiXuScore) {
        this.qiXuScore = qiXuScore;
    }

    @Basic
    @Column(name = "YANG_XU_SCORE", nullable = true, precision = 0)
    public Long getYangXuScore() {
        return yangXuScore;
    }

    public void setYangXuScore(Long yangXuScore) {
        this.yangXuScore = yangXuScore;
    }

    @Basic
    @Column(name = "YIN_XU_SCORE", nullable = true, precision = 0)
    public Long getYinXuScore() {
        return yinXuScore;
    }

    public void setYinXuScore(Long yinXuScore) {
        this.yinXuScore = yinXuScore;
    }

    @Basic
    @Column(name = "TAN_SHI_SCORE", nullable = true, precision = 0)
    public Long getTanShiScore() {
        return tanShiScore;
    }

    public void setTanShiScore(Long tanShiScore) {
        this.tanShiScore = tanShiScore;
    }

    @Basic
    @Column(name = "SHI_RE_SCORE", nullable = true, precision = 0)
    public Long getShiReScore() {
        return shiReScore;
    }

    public void setShiReScore(Long shiReScore) {
        this.shiReScore = shiReScore;
    }

    @Basic
    @Column(name = "XUE_YU_SCORE", nullable = true, precision = 0)
    public Long getXueYuScore() {
        return xueYuScore;
    }

    public void setXueYuScore(Long xueYuScore) {
        this.xueYuScore = xueYuScore;
    }

    @Basic
    @Column(name = "QI_YU_SCORE", nullable = true, precision = 0)
    public Long getQiYuScore() {
        return qiYuScore;
    }

    public void setQiYuScore(Long qiYuScore) {
        this.qiYuScore = qiYuScore;
    }

    @Basic
    @Column(name = "TE_BING_SCORE", nullable = true, precision = 0)
    public Long getTeBingScore() {
        return teBingScore;
    }

    public void setTeBingScore(Long teBingScore) {
        this.teBingScore = teBingScore;
    }

    @Basic
    @Column(name = "PING_HE_SCORE", nullable = true, precision = 0)
    public Long getPingHeScore() {
        return pingHeScore;
    }

    public void setPingHeScore(Long pingHeScore) {
        this.pingHeScore = pingHeScore;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        PhysiqueIdentification that = (PhysiqueIdentification) object;
        return Objects.equals(id, that.id) &&
                Objects.equals(createdate, that.createdate) &&
                Objects.equals(modifydate, that.modifydate) &&
                Objects.equals(question1, that.question1) &&
                Objects.equals(question2, that.question2) &&
                Objects.equals(question3, that.question3) &&
                Objects.equals(question4, that.question4) &&
                Objects.equals(question5, that.question5) &&
                Objects.equals(question6, that.question6) &&
                Objects.equals(question7, that.question7) &&
                Objects.equals(question8, that.question8) &&
                Objects.equals(question9, that.question9) &&
                Objects.equals(question10, that.question10) &&
                Objects.equals(question11, that.question11) &&
                Objects.equals(question12, that.question12) &&
                Objects.equals(question13, that.question13) &&
                Objects.equals(question14, that.question14) &&
                Objects.equals(question15, that.question15) &&
                Objects.equals(question16, that.question16) &&
                Objects.equals(question17, that.question17) &&
                Objects.equals(question18, that.question18) &&
                Objects.equals(question19, that.question19) &&
                Objects.equals(question20, that.question20) &&
                Objects.equals(question21, that.question21) &&
                Objects.equals(question22, that.question22) &&
                Objects.equals(question23, that.question23) &&
                Objects.equals(question24, that.question24) &&
                Objects.equals(question25, that.question25) &&
                Objects.equals(question26, that.question26) &&
                Objects.equals(question27, that.question27) &&
                Objects.equals(question28, that.question28) &&
                Objects.equals(question29, that.question29) &&
                Objects.equals(question30, that.question30) &&
                Objects.equals(question31, that.question31) &&
                Objects.equals(question32, that.question32) &&
                Objects.equals(question33, that.question33) &&
                Objects.equals(patientcode, that.patientcode) &&
                Objects.equals(fillDate, that.fillDate) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(qiXuScore, that.qiXuScore) &&
                Objects.equals(yangXuScore, that.yangXuScore) &&
                Objects.equals(yinXuScore, that.yinXuScore) &&
                Objects.equals(tanShiScore, that.tanShiScore) &&
                Objects.equals(shiReScore, that.shiReScore) &&
                Objects.equals(xueYuScore, that.xueYuScore) &&
                Objects.equals(qiYuScore, that.qiYuScore) &&
                Objects.equals(teBingScore, that.teBingScore) &&
                Objects.equals(pingHeScore, that.pingHeScore);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdate, modifydate, question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11, question12, question13, question14, question15, question16, question17, question18, question19, question20, question21, question22, question23, question24, question25, question26, question27, question28, question29, question30, question31, question32, question33, patientcode, fillDate, doctor, qiXuScore, yangXuScore, yinXuScore, tanShiScore, shiReScore, xueYuScore, qiYuScore, teBingScore, pingHeScore);
    }

    @Basic
    @Column(name = "QI_XU", nullable = false, precision = 0)
    public Long getQiXu() {
        return qiXu;
    }

    public void setQiXu(Long qiXu) {
        this.qiXu = qiXu;
    }

    @Basic
    @Column(name = "YANG_XU", nullable = false, precision = 0)
    public Long getYangXu() {
        return yangXu;
    }

    public void setYangXu(Long yangXu) {
        this.yangXu = yangXu;
    }

    @Basic
    @Column(name = "YIN_XU", nullable = false, precision = 0)
    public Long getYinXu() {
        return yinXu;
    }

    public void setYinXu(Long yinXu) {
        this.yinXu = yinXu;
    }

    @Basic
    @Column(name = "TAN_SHI", nullable = false, precision = 0)
    public Long getTanShi() {
        return tanShi;
    }

    public void setTanShi(Long tanShi) {
        this.tanShi = tanShi;
    }

    @Basic
    @Column(name = "SHI_RE", nullable = false, precision = 0)
    public Long getShiRe() {
        return shiRe;
    }

    public void setShiRe(Long shiRe) {
        this.shiRe = shiRe;
    }

    @Basic
    @Column(name = "XUE_YU", nullable = false, precision = 0)
    public Long getXueYu() {
        return xueYu;
    }

    public void setXueYu(Long xueYu) {
        this.xueYu = xueYu;
    }

    @Basic
    @Column(name = "QI_YU", nullable = false, precision = 0)
    public Long getQiYu() {
        return qiYu;
    }

    public void setQiYu(Long qiYu) {
        this.qiYu = qiYu;
    }

    @Basic
    @Column(name = "TE_BING", nullable = false, precision = 0)
    public Long getTeBing() {
        return teBing;
    }

    public void setTeBing(Long teBing) {
        this.teBing = teBing;
    }

    @Basic
    @Column(name = "PING_HE", nullable = false, precision = 0)
    public Long getPingHe() {
        return pingHe;
    }

    public void setPingHe(Long pingHe) {
        this.pingHe = pingHe;
    }
}